<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\v2\Member;

class TransferSaldoHistory extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($data)
    {
        $member = Member::where('id', $data->receiver_id)->first();
        return [
            'id' => $member->referal_id,
            'name' => $member->name,
            'avatar' => env("APP_URL") . '/storage/' . $member->avatar
        ];
    }
}
