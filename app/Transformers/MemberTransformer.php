<?php

namespace App\Transformers;

use App\Http\Controllers\API\v2\HelperController;
use App\Models\v2\Member;
use App\Models\v2\UserLevel;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class MemberTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'level'
    ];

    /**
     * A Fractal transformer.
     *
     * @param Member $member
     * @return array
     */
    public function transform(Member $member)
    {
        $saldo = HelperController::member_saldo($member->id);
        $point = HelperController::member_point($member->id);

        return [
            'id' => $member->id,
            'avatar' => env("APP_URL") . '/storage/' . $member->avatar,
            'name' => $member->name,
            'email' => $member->email,
            'phone' => $member->phone,
            'address' => $member->address,
            'referral_id' => $member->referal_id,
            'wallet' => [
                'balance' => $saldo,
                'point' => $point,
            ]
        ];
    }

    public function includeLevel(Member $member) {
        $level = UserLevel::where('id', $member->user_level_id)->first();

        return $this->item($level, new UserLevelTransformer());
    }
}
