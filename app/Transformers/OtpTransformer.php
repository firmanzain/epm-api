<?php

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

use App\Models\v2\Otp;

class OtpTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Otp $otp)
    {
        return [
            'phone' => $otp->phone_number,
            'created_at' => Carbon::parse($otp->created_at)->toDateTimeString(),
            'resend_allowed_at' => Carbon::parse($otp->expired_at)->toDateTimeString()
        ];
    }
}
