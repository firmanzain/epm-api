<?php

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

use App\Models\v2\Announcement;

class AnnouncementTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Announcement $announcement
     * @return array
     */
    public function transform(Announcement $announcement)
    {
        return [
            'id' => $announcement->id,
            'title' => $announcement->title,
            'url' => $announcement->action_url,
            'description' => $announcement->desc,
            'html' => $announcement->html_content,
            'pict' => env("APP_URL") . '/storage/' . $announcement->pict_url,
            'valid_until' => Carbon::parse($announcement->valid_until)->toDateTimeString()
        ];
    }
}
