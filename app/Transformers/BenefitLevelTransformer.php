<?php

namespace App\Transformers;

use App\Models\v2\Benefit;
use League\Fractal\TransformerAbstract;
use App\Models\v2\BenefitUserLevels;

class BenefitLevelTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param BenefitUserLevels $data
     * @return array
     */
    public function transform(BenefitUserLevels $data)
    {
        $benefit = Benefit::where('id', $data->benefit_id)->first();
        $available = [];
        $benefitUserLevels = BenefitUserLevels::where('benefit_id', $data->benefit_id)->get();
        foreach ($benefitUserLevels as $level) {
            array_push($available, $level->user_level_id);
        }

        return [
            'id' => $benefit->id,
            'icon' => $benefit->icon,
            'description' => $benefit->description,
            'available' => $available
        ];
    }
}
