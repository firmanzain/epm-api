<?php

namespace App\Transformers;

use App\Http\Controllers\API\v2\HelperController;
use App\Models\v2\Member;
use App\Models\v2\ProductCategories;
use App\Models\v2\SubProductCategory;
use App\Models\v2\UserLevel;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Models\v2\Product;

class SubProductCategoryTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'sub'
    ];

    /**
     * A Fractal transformer.
     *
     * @param SubProductCategory $productCategory
     * @return array
     */
    public function transform(SubProductCategory $productCategory)
    {
        if ($productCategory->getType() == SubProductCategory::TYPE_PRODUCT) {

            $icon = $productCategory->getIcon();
            if ($icon != null) {
                if (strpos($icon, 'http') !== false) {
                } else {
                    $icon = env("APP_URL") . '/storage/' . $productCategory->getIcon();
                }
            }

            $caption = 'ID Pelanggan';
            if (strlen($productCategory->getCaption()) > 0) {
                $caption = $productCategory->getCaption();
            } else {
                $product = Product::where('id', $productCategory->getId())->first();
                if ($product) {
                    if ($product->caption) {
                        $caption = $product->caption;
                    } else {
                        if ($product->category_id) {
                            $productCategorySearch = ProductCategories::where('id', $product->category_id)->first();
                            if ($productCategorySearch) {
                                if ($productCategorySearch->caption) {
                                    $caption = $productCategorySearch->caption;
                                }
                            }
                        }
                    }
                }
            }

            $subcaption = 'Pilih Produk';
            if (strlen($productCategory->getSubCaption()) > 0) {
                $subcaption = $productCategory->getSubCaption();
            } else {
                $product = Product::where('id', $productCategory->getId())->first();
                if ($product) {
                    if ($product->subcaption) {
                        $subcaption = $product->subcaption;
                    } else {
                        if ($product->category_id) {
                            $productCategorySearch = ProductCategories::where('id', $product->category_id)->first();
                            if ($productCategorySearch) {
                                if ($productCategorySearch->subcaption) {
                                    $subcaption = $productCategorySearch->subcaption;
                                }
                            }
                        }
                    }
                }
            }

            return [
                'type' => 'product',
                'id' => $productCategory->getId(),
                'name' => $productCategory->getName(),
                'icon' => $icon,
                'price' => $productCategory->getPrice(),
                'point' => $productCategory->getPointPremium(),
                'caption' => $caption,
                'sub_caption' => $subcaption,
                'input_type' => $productCategory->getInputType(),
                'description' => $productCategory->getDescription(),
                'isPostpaid' => $productCategory->isPostpaid(),
                'index' => $productCategory->getIndex(),
                'extraButton' => (strlen($productCategory->getExtraButton()) > 0) ? $productCategory->getExtraButton() : NULL,
            ];
        } elseif ($productCategory->getType() == SubProductCategory::TYPE_SUB_CATEGORY) {
            $subCategoriesCount = ProductCategories::where('parent', $productCategory->getId())
                ->where('status', 1)
                ->count();

            $filterUrl = $productCategory->getFilterUrl();
            $filterUrl = $filterUrl == null ? null : env("APP_URL") . $filterUrl;

            $icon = $productCategory->getIcon();
            if ($icon != null) {
                if (strpos($icon, 'http') !== false) {
                } else {
                    $icon = env("APP_URL") . '/storage/' . $productCategory->getIcon();
                }
            }

            return [
                'type' => 'category',
                'id' => $productCategory->getId(),
                'name' => $productCategory->getName(),
                'icon' => $icon,
                'filterUrl' => $filterUrl,
                'caption' => $productCategory->getCaption(),
                'sub_caption' => $productCategory->getSubCaption(),
                'input_type' => $productCategory->getInputType(),
                'description' => $productCategory->getDescription(),
                'isPostpaid' => $productCategory->isPostpaid(),
                'index' => $productCategory->getIndex(),
                'hasSubCategory' => ($subCategoriesCount > 0),
                'extraButton' => (strlen($productCategory->getExtraButton()) > 0) ? $productCategory->getExtraButton() : NULL,
            ];
        }

        return null;
    }
}
