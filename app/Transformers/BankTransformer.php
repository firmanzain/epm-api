<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\v2\Bank;

class BankTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Bank $data)
    {
        return [
            'id' => $data->id,
            'name' => $data->name,
        ];
    }
}
