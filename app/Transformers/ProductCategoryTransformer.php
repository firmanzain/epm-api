<?php

namespace App\Transformers;

use App\Models\v2\ProductCategories;
use App\Models\v2\SubProductCategory;
use App\Models\v2\Product;
use League\Fractal\TransformerAbstract;

class ProductCategoryTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'sub'
    ];

    /**
     * A Fractal transformer.
     *
     * @param ProductCategories $categories
     * @return array
     */
    public function transform(ProductCategories $categories)
    {
        return [
            'id' => $categories->id,
            'name' => $categories->name,
            'caption' => $categories->caption,
            'subcaption' => $categories->subcaption,
            'input_type' => $categories->input_type,
            'description' => $categories->description,
            'index' => $categories->index,
            'extraButton' => $categories->extraButton,
        ];
    }

    /**
     * @param ProductCategories $category
     * @return \League\Fractal\Resource\Collection
     */
    public function includeSub(ProductCategories $category)
    {
        $sub = [];
        $subCategories = ProductCategories::where('parent', $category->id)
            ->where('status', 1)
            ->orderBy('index', 'asc')
            ->get();

        foreach ($subCategories as $subCategory) {
            array_push($sub, SubProductCategory::createCategory($subCategory));
        }

        $products = Product::where('category_id', $category->id)
            ->where('status_active', 1)
            ->orderBy('index', 'asc')
            ->get();

        foreach ($products as $product) {
            array_push($sub, SubProductCategory::createProduct($product, $category->isPremium));
        }

        usort($sub, function($a, $b) {
            return $a->index <=> $b->index;
        });

        return $this->collection($sub, new SubProductCategoryTransformer());
    }
}
