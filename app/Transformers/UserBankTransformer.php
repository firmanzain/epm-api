<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\v2\UserBankAccount, App\Models\v2\Bank;

class UserBankTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(UserBankAccount $data)
    {
        $bank = Bank::where('id', $data->bank_id)->first();

        return [
            'id' => $data->id,
            'account_no' => $data->account_no,
            'account_holder' => $data->account_holder,
            'branch' => $data->branch,
            'bank' => $bank->name,
        ];
    }
}
