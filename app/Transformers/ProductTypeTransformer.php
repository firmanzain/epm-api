<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\v2\ProductType;

class ProductTypeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ProductType $data)
    {
        return [
            'id' => $data->id,
            'name' => $data->name,
        ];
    }
}
