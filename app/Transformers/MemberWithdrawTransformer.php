<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\v2\MemberWithdraw, App\Models\v2\UserBankAccount;
use Carbon\Carbon;

class MemberWithdrawTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'bank'
    ];

    /**
     * A Fractal transformer.
     *
     * @param MemberWithdraw $data
     * @return array
     */
    public function transform(MemberWithdraw $data)
    {

        $detail = '';
        $bank = UserBankAccount::with('bank')->where('id', $data->bank_id)->first();
        if ($bank) {
            $detail .= ' ke '.$bank->account_no.' ('.$bank->bank->name.')';
        }

        $description = 'Withdraw'.$detail;
        if ($data->is_approved == 0) {
            $status = 'Ditolak';
        } else if ($data->is_approved == 1) {
            $status = 'Disetujui';
        } else if ($data->is_approved == 2) {
            $status = 'Pending';
        }

        return [
            'id' => $data->id,
            'transaction_id' => 'WITHDRAW'.$data->id,
            'amount' => intval($data->amount),
            'fee' => intval($data->admin_fee),
            'description' => $description,
            'type' => 'Kredit',
            'status' => $status,
            'created_at' => Carbon::parse($data->created_at)->toDateTimeString(),
            'approve_at' => Carbon::parse($data->approve_at)->toDateTimeString(),
        ];
    }

    /**
     * @param MemberWithdraw $data
     * @return \League\Fractal\Resource\Item|null
     */
    public function includeBank(MemberWithdraw $data) {
        $bank = UserBankAccount::where('id', $data->user_bank_account_id)->first();
        if ($bank) {
            return $this->item($bank, new UserBankTransformer());
        }
        return NULL;
    }
}
