<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\v2\TopupMember, App\Models\v2\TransactionBank;
use Carbon\Carbon;

class TopupMemberTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'bank'
    ];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TopupMember $data)
    {
        $detail = '';
        $bank = TransactionBank::with('bank')->where('id', $data->bank_id)->first();
        if ($bank) {
            $detail .= ' ke '.$bank->account_no.' ('.$bank->bank->name.')';
        }

        $dateNow = Carbon::now();
        $created = Carbon::parse($data->created_at);

        $description = 'Topup Saldo'.$detail;
        if ($data->topup_status == 0) {
            $status = 'Pending';
            if (!$data->attachment) {
                $status = 'Unpaid';
            }
            if ($created->diffInHours($dateNow) > 10) {
                $status = 'Expired';
            }
        } else if ($data->topup_status == 1) {
            $status = 'Sukses';
        } else if ($data->topup_status == 2) {
            $status = 'Gagal';
        }

        return [
            'id' => $data->id,
            'transaction_id' => 'TOPUP'.$data->id,
            'amount' => intval($data->topup_saldo),
            'fee' => 0,
            'description' => $description,
            'type' => 'Debit',
            'status' => $status,
            'created_at' => Carbon::parse($data->created_at)->toDateTimeString(),
        ];
    }

    public function includeBank(TopupMember $data) {
        $bank = TransactionBank::where('id', $data->transfer_to)->first();
        if ($bank) {
            return $this->item($bank, new TransactionBankTransformer());
        }
        return NULL;
    }
}
