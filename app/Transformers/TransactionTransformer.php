<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use Carbon\Carbon;
use App\Models\v2\Product, App\Models\v2\Member, App\Models\v2\TransactionBank,
    App\Models\v2\UserBankAccount, App\Models\v2\MemberBuyProduct, App\Models\v2\MemberBuyPpob,
    App\Models\v2\SubProductCategory;
use App\Transformers\Serializer\EpmArraySerializer;

class TransactionTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'product'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($data)
    {
        $description = '';
        $status = '';
        $transactionId = '';
        $product = '';
        $receipt = null;
        $cust_id = '';
        $sn = '';
        $content = '';
        $detail = null;

        $dateNow = Carbon::now();
        $created = Carbon::parse($data->created_at);

        preg_match("/.*[^\d]/", $data->transaction_id, $matches);
        $transactionId = $data->transaction_id.$data->id;

        if ($data->category == 1) {

            $detail = '';
            $bank = TransactionBank::with('bank')->where('id', $data->bank_id)->first();
            if ($bank) {
                $detail .= ' ke '.$bank->account_no.' ('.$bank->bank->name.')';
            }

            $description = 'Topup Saldo'.$detail;
            if ($data->status == 0) {
                $status = 'Sedang Diproses';
                if ($created->diffInHours($dateNow) > 10) {
                    $status = 'Expired';
                }
            } else if ($data->status == 1) {
                $status = 'Sukses';
            } else if ($data->status == 2) {
                $status = 'Gagal';
            }

        } else if ($data->category == 2) {

            $detail = '';
            $bank = UserBankAccount::with('bank')->where('id', $data->bank_id)->first();
            if ($bank) {
                $detail .= ' ke '.$bank->account_no.' ('.$bank->bank->name.')';
            }

            $description = 'Withdraw'.$detail;
            if ($data->status == 0) {
                $status = 'Ditolak';
            } else if ($data->status == 1) {
                $status = 'Disetujui';
            } else if ($data->status == 2) {
                $status = 'Sedang Diproses';
            }

        } else if ($data->category == 3) {

            $detail = '';
            $member = Member::where('id', $data->origin_id)->first();
            if ($member) {
                $detail .= ' ke '.$member->name.' ('.$member->referal_id.')';
            }

            $description = 'Kirim Saldo'.$detail;
            $status = 'Sukses';

        } else if ($data->category == 4) {

            $detail = '';
            $member = Member::where('id', $data->origin_id)->first();
            if ($member) {
                $detail .= ' dari '.$member->name.' ('.$member->referal_id.')';
            }

            $description = 'Terima Saldo'.$detail;
            $status = 'Sukses';

        } else if ($data->category == 5) {

            $detail = ' dari ';
            $arrId = explode('-', $data->origin_id);
            for ($i = 0; $i < sizeof($arrId); $i++) {
                $member = Member::where('id', $arrId[$i])->first();
                if ($member) {
                    $name = explode(' ', $member->name);
                    $detail .= $name[0].' ';
                    if ($i != (sizeof($arrId) - 1)) {
                        $detail .= '> ';
                    }
                }
            }

            $description = 'Bonus Referal'.$detail;
            $status = 'Sukses';

        } else if ($data->category == 6) {

            $description = 'Deposit Koperasi';
            $status = 'Sukses';

        } else if ($data->category == 7) {

            $description = 'Member Buy Level';
            $status = ($data->status == 1) ? 'Sukses' : 'Gagal';

        } else if ($data->category == 8) {

            $transactionId = $matches[0].$created->timestamp;
            $description = 'Member Buy Product Prepaid';
            $status = 'Gagal';
            if ($data->status == 1) {
                $status = 'Sukses';
            } else if ($data->status == 2) {
                $status = 'Sedang Diproses';
            }

            $productName = Product::where('id', $data->origin_id)->first();
            if ($productName) {
                $product .= "\n\"".$productName->name."\"";
            }

            $transaction = MemberBuyProduct::where('id', $data->id)->first();
            if ($transaction->msisdn) {
                $product .= " ke ".$transaction->msisdn."";
            }

            $receipt = env("APP_URL") . '/v2/transaction/prepaid/' . $data->id . '/print';
            if ($data->receiptUrl) {
                $receipt = $data->receiptUrl;
            }

            $cust_id = $data->cust_id;
            $sn = $data->sn;
            $content = $data->data;
            // if ($data->detail) {
            //     $detail = json_decode($data->detail);
            // }

        } else if ($data->category == 9) {

            $transactionId = $matches[0].$created->timestamp;
            $description = 'Member Buy Product Postpaid';
            $status = 'Gagal';
            if ($data->status == 1) {
                $status = 'Sukses';
            } else if ($data->status == 2) {
                $status = 'Sedang Diproses';
            }

            $productName = Product::where('id', $data->origin_id)->first();
            if ($productName) {
                $product .= "\n\"".$productName->name."\"";
            }

            $transaction = MemberBuyPpob::where('id', $data->id)->first();
            if ($transaction->msisdn) {
                $product .= " ke ".$transaction->msisdn."";
            }

            $receipt = env("APP_URL") . '/v2/transaction/postpaid/' . $data->id . '/print';
            if ($data->receiptUrl) {
                $receipt = $data->receiptUrl;
            }

            $cust_id = $data->cust_id;
            $sn = $data->sn;
            $content = $data->data;
            if ($data->detail) {
                $detail = json_decode($data->detail);
            }

        }

        return [
            'id' => $data->id,
            'transaction_id' => $transactionId,
            'cust_id' => $cust_id,
            'sn' => $sn,
            'data' => $content,
            'detail' => $detail,
            'amount' => intval($data->amount),
            'fee' => intval($data->fee),
            'description' => $description.$product,
            'type' => $data->type,
            'method' => ($data->method == 1) ? 'Balance' : 'Point',
            'receipt' => $receipt,
            'status' => $status,
            'created_at' => Carbon::parse($data->created_at)->toDateTimeString(),
        ];
    }

    public function includeProduct($data)
    {
        if ($data->category == 8) {
            $transaction = MemberBuyProduct::where('id', $data->id)->first();
            if ($transaction) {
                $member = Member::where('id', $transaction->user_id)->first();
                $isPremium = ($member->user_level_id != 1);
                $product = Product::where('id', $transaction->product_id)->first();

                $productDetail = SubProductCategory::createProduct($product, $isPremium);
                return $this->item($productDetail, new SubProductCategoryTransformer());
            }
        } else if ($data->category == 9) {
            $transaction = MemberBuyPpob::where('id', $data->id)->first();
            if ($transaction) {
                $member = Member::where('id', $transaction->user_id)->first();
                $isPremium = ($member->user_level_id != 1);
                $product = Product::where('id', $transaction->product_id)->first();

                $productDetail = SubProductCategory::createProduct($product, $isPremium);
                return $this->item($productDetail, new SubProductCategoryTransformer());
            }
        }

        return NULL;
    }
}
