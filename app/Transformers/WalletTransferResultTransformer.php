<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use Carbon\Carbon;
use App\Models\v2\TransferSaldo, App\Models\v2\Member;

class WalletTransferResultTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'sender', 'recipient'
    ];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TransferSaldo $data)
    {
        return [
            'id' => $data->id,
            'transaction_id' => 'TRANSFER'.$data->id,
            'amount' => intval($data->nominal),
            'fee' => 0,
            'description' => 'Kirim Saldo',
            'type' => 'Debit',
            'status' => 'Sukses',
            'created_at' => Carbon::parse($data->created_at)->toDateTimeString(),
        ];
    }

    public function includeSender(TransferSaldo $data) {
        $sender = Member::where('id', $data->sender_id)->first();
        return $this->item($sender, new MemberTransformer());
    }

    public function includeRecipient(TransferSaldo $data) {
        $recipient = Member::where('id', $data->receiver_id)->first();
        return $this->item($recipient, new WalletTransferRecipientTransformer());
    }
}
