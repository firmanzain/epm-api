<?php

namespace App\Transformers;

use App\Http\Controllers\API\v2\HelperController;
use App\Models\v2\Member;
use App\Models\v2\UserLevel;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class WalletTransferRecipientTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Member $member
     * @return array
     */
    public function transform(Member $member)
    {
        return [
            'id' => $member->referal_id,
            'name' => $member->name,
            'avatar' => env("APP_URL") . '/storage/' . $member->avatar
        ];
    }
}
