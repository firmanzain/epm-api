<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

use App\Models\v2\UserLevel;

class UserLevelTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param UserLevel $level
     * @return array
     */
    public function transform(UserLevel $level)
    {
        $upgradable = true;
        if ($level->levelMember >= $level->id) {
            $upgradable = false;
        }

        $price = $level->price;
        $levelPrice = UserLevel::where('id', $level->levelMember)->first();
        if ($levelPrice) {
            $price -= $levelPrice->price;
            if ($price < 0) {
                $price = 0;
            }
        }

        return [
            'id' => $level->id,
            'name' => $level->name,
            'price' => $price,
            'is_upgradable' => $upgradable,
            'is_active' => is_null($level->deleted_at) ? true : false,
        ];
    }
}
