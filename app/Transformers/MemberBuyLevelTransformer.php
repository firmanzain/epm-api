<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\v2\MemberBuyLevel, App\Models\v2\UserLevel, App\Models\v2\Member;

class MemberBuyLevelTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'level', 'member'
    ];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(MemberBuyLevel $data)
    {
        return [
            'id' => $data->id,
            'level_price' => $data->level_price,
            'reference_id' => $data->reference_by,
        ];
    }

    public function includeLevel(MemberBuyLevel $data) {
        $level = UserLevel::where('id', $data->user_level_id)->first();
        return $this->item($level, new UserLevelTransformer());
    }

    public function includeMember(MemberBuyLevel $data) {
        $member = Member::where('id', $data->user_id)->first();
        return $this->item($member, new MemberTransformer());
    }
}
