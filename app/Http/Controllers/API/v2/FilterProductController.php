<?php

namespace App\Http\Controllers\API\v2;

use App\Models\v2\SubProductCategory;
use App\Transformers\SubProductCategoryTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTFactory, JWTAuth, DB;
use App\Models\v2\Product, App\Models\v2\PrefixNumber;
use App\Transformers\Serializer\EpmArraySerializer;

class FilterProductController extends Controller
{
    public function filter_by_prefix_number(Request $request, $category, $prefix)
    {
        if (!$prefix) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `prefix` is required.'
            ], 400);
        }

        $member = JWTAuth::toUser($request->token);

        $prefixNumber = PrefixNumber::where('status_prefix', 1)
            ->where('no', 'like', '%' . substr($prefix, 0, 4) . '%')->first();

        if (!$prefixNumber) {
            return response()->json([
                'code' => 404,
                'message' => 'Prefix not found.'
            ], 404);
        }

        $isPremium = ($member->user_level_id != 1);

        $products = Product::select(
            '*',
            DB::raw('"' . $prefixNumber->icon . '" as icon'),
            DB::raw('"'. $isPremium . '" as isPremium')
        )->where('category_id', $category)
            ->where('status_active', 1)
            ->whereRaw('LOWER(`name`) like ?', '%' . strtolower($prefixNumber->kartu) . '%')
            // ->orWhere('category_id', $category)
            //     ->where('status_active', 1)
            //     ->whereRaw('LOWER(`name`) like ?', '%' . strtolower($prefixNumber->operator) . '%')
            ->orderBy('index', 'asc')->get();

        if (!$products) {
            return response()->json([
                'code' => 404,
                'message' => 'Product not found.'
            ], 404);
        }

        $subProductCategories = [];
        foreach ($products as $product) {
            array_push($subProductCategories, SubProductCategory::createProduct($product, $isPremium));
        }

        return fractal()
            ->collection($subProductCategories, new SubProductCategoryTransformer())
            ->serializeWith(new EpmArraySerializer())
            ->toArray();
    }
}
