<?php

namespace App\Http\Controllers\API\v2;

use App\Transformers\AnnouncementTransformer;
use App\Transformers\Serializer\EpmArraySerializer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v2\Announcement;
use JWTFactory, JWTAuth, DateTime, DateInterval;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_announcement($id = NULL)
    {
        $now = Carbon::now()->format('Y-m-d');
        if (!$id) {
            $paginator = Announcement::where('valid_until', '>=', $now)
                ->whereNull('deleted_at')
                ->paginate(15);

            $announcements = $paginator->getCollection();

            return fractal()->collection($announcements, new AnnouncementTransformer())
                ->paginateWith(new IlluminatePaginatorAdapter($paginator))
                ->toArray();
        }

        $announcement = Announcement::where('id', $id)
            ->where('valid_until', '>=', $now)
            ->whereNull('deleted_at')
            ->first();

        if (!$announcement) {
            return response()->json([
                'code' => 404,
                'message' => 'Announcement not found or has been expired.'
            ], 404);
        }

        return fractal()->item($announcement, new AnnouncementTransformer())
            ->toArray();
    }

    public function get_announcement_banner()
    {
        $now = Carbon::now()->format('Y-m-d');
        $announcements = Announcement::where('valid_until', '>=', $now)
            ->where('isShow', 1)
            ->whereNull('deleted_at')
            ->limit(5)
            ->get();

        return fractal()->collection($announcements, new AnnouncementTransformer())
            ->serializeWith(new EpmArraySerializer())
            ->toArray();
    }
}
