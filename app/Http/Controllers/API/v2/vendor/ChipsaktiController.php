<?php

namespace App\Http\Controllers\API\v2\vendor;

use App\Http\Controllers\API\v2\HelperController;
use App\Http\Controllers\API\v2\MemberController;
use App\Models\v2\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use
    App\Models\v2\SendapiSupplier, App\Models\v2\ResultSupplier, App\Models\v2\MemberBuyProduct,
    App\Models\v2\Member, App\Models\v2\Product, App\Models\v2\MemberBuyPpob,
    App\Models\v2\MemberBonus, App\Models\v2\SupplierProduct;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ChipsaktiController implements VendorController
{
    private static function generate_sign($type, $transactionId, $requestTime, $reffId = NULL)
    {
        $sign = hash('sha256', '$' . $type . '$' . $transactionId . '$' . env("ID_CHIP") . '$EPM$' . $requestTime . '$' . env("KEY_CHIP") . '$');
        if ($reffId) {
            $sign = hash('sha256', '$' . $type . '$' . $transactionId . '$' . env("ID_CHIP") . '$' . $reffId . '$EPM$' . $requestTime . '$' . env("KEY_CHIP") . '$');
        }

        return $sign;
    }

    private static function inquiry($productCode, $idPelanggan)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();

        $count = SendapiSupplier::where('supplier_id', $supplier->id)->count();
        $trxid = str_pad($count + 1, 6, '0', STR_PAD_LEFT);
        $arrResult['trx_id'] = $trxid;

        $requestTime = date('Y-m-d H:i:s');
        $sign = ChipsaktiController::generate_sign('inquiry', $trxid, $requestTime);

        $client = new Client(['http_errors' => false]);
        $params = [
            'transaction_id' => $trxid,
            'partner_id' => env("ID_CHIP"),
            'product_code' => $productCode,
            'customer_no' => $idPelanggan,
            'merchant_code' => 'EPM',
            'request_time' => $requestTime,
            'signature' => $sign,
        ];
        $options = [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => $params,
        ];

        $response = $client->request('POST', env("URL_CHIP") . 'ppob/inquiry', $options);
        $get_response = (array)json_decode($response->getBody()->getContents());

        $arrResult['req'] = $params;
        $arrResult['res'] = $get_response;
        $arrResult['status'] = ($get_response['rc'] == '00' || $get_response['rc'] == '03') ? 0 : 1;

        return $arrResult;
    }

    public static function inquiry_postpaid($productId, $productCode, $idPelanggan)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();
        $inquiry = self::inquiry($productCode, $idPelanggan);

        $result['trx_id'] = $inquiry['trx_id'];

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_CHIP') . 'ppob/inquiry';
        $sendApi->sendapi_text = json_encode($inquiry['req']);
        $sendApi->sendapi_response_text = json_encode($inquiry['res']);
        $sendApi->sendapi_status = $inquiry['status'];
        $sendApi->product_id = $productId;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        // GET ADMIN PRODUCT
        $suppProducts = SupplierProduct::where('product_id', $productId)
            ->where('supplier_id', $supplier->id)
            ->first();

        $result['vendor'] = "chipsakti";
        $result['status'] = $inquiry['status'];
        $result['sendApiId'] = $sendApi->id;

        if ($inquiry['status'] == 0) {

            $content_array = $inquiry['res'];

            $fee = intval($suppProducts->fee);
            $adminEpm = $suppProducts->biaya_admin;
            $admin = 0;
            $amount = 0;

            $data = array();
            $dataDebug = array();
            foreach ($content_array as $key => $value) {
                if (strpos(strtolower($key), 'nama') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('nama', 'Nama Pelanggan', strtolower($key));
                        $data[$newKey] = $value;
                    }
                } elseif (strpos(strtolower($key), 'nopel') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('nopel', 'ID Pelanggan', strtolower($key));
                        $data[$newKey] = $value;
                    }
                } elseif (strtolower($key) == 'tagihan') {
                    if (strlen($value) > 0) {
                        $dataDebug[$key] = str_replace('tagihan', 'Amount', strtolower($key));
                        $data['Tagihan'] = 'Rp' . number_format($value, 0, ",", ".");
                        $amount += intval(str_replace('Rp.', '', $value));
                    }
                } elseif (strpos(strtolower($key), 'data') !== false) {
                    foreach ($content_array[$key] as $keyData => $valueData) {
                        if (strpos(strtolower($keyData), 'periode') !== false) {
                            if (strlen($valueData) > 0) {
                                $newKey = str_replace('periode', 'Periode', strtolower($keyData));
                                $data[$newKey] = $valueData;
                            }
                        }
                    }
                }
                $dataDebug[$key] = $value;
            }
            $data['Admin'] = 'Rp' . number_format($adminEpm, 0, ",", ".");

            $result['admin'] = $adminEpm;
            $result['amount_vendor'] = $amount + $fee;
            $result['amount'] = $amount + $adminEpm;

            $result['status'] = 0;
            $result['data'] = $data;
            $result['data_debug'] = $dataDebug;
        } else {
            $result['admin'] = 0;
            $result['amount_vendor'] = 0;
            $result['amount'] = 0;
            $result['status'] = 4;
            if ($inquiry['res']['rc'] == '27' || $inquiry['res']['rc'] == '28') {
                $result['status'] = 1;
            }
            if ($inquiry['res']['rc'] == '21') {
                $result['status'] = 3;
            }
            $result['data'] = $inquiry;
        }

        return $result;
    }

    public static function pay_postpaid($transactionId, $productCode, $idPelanggan, $trxid, $amount)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();

        $buyProduct = MemberBuyPpob::where('ppob_id', $transactionId)->first();

        // GET ADMIN PRODUCT
        $suppProducts = SupplierProduct::where('product_id', $buyProduct->product_id)
            ->where('supplier_id', $supplier->id)
            ->first();

        $reff_id = '-';
        $checkSendApi = SendapiSupplier::where('supplier_id', 5)->where('sendapi_text', 'like', '%' . $trxid . '%')->first();
        if ($checkSendApi) {
            $content = (array)json_decode($checkSendApi->sendapi_response_text);
            if (isset($content['reffid'])) {
                $reff_id = $content['reffid'];
            }
        }

        $requestTime = date('Y-m-d H:i:s');
        $sign = ChipsaktiController::generate_sign('payment', $trxid, $requestTime, $reff_id);

        $client = new Client(['http_errors' => false]);
        $params = [
            'transaction_id' => $trxid,
            'partner_id' => env("ID_CHIP"),
            'product_code' => $productCode,
            'customer_no' => $idPelanggan,
            'merchant_code' => 'EPM',
            'reff_id' => $reff_id,
            'amount' => $amount,
            'request_time' => $requestTime,
            'signature' => $sign,
        ];
        $options = [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => $params,
        ];

        $response = $client->request('POST', env("URL_CHIP") . 'ppob/payment', $options);
        $get_response = (array)json_decode($response->getBody()->getContents());

        Log::channel('paypostpaid')->info('Log inquiry payment: user = ' . $buyProduct->user_id);
        Log::channel('paypostpaid')->info(json_encode($options));

        $product = Product::where('id', $buyProduct->product_id)->first();

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_CHIP') . 'ppob/payment';
        $sendApi->sendapi_text = json_encode($options);
        $sendApi->sendapi_response_text = json_encode($get_response);
        $sendApi->sendapi_status = $get_response['rc'];
        $sendApi->product_id = $buyProduct->product_id;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        $fee = intval($suppProducts->fee);
        $adminEpm = $suppProducts->biaya_admin;
        $total = ($amount - $fee) + $adminEpm;

        $buyProduct->charge = $total;
        $buyProduct->tagihan = $amount - $fee;
        $buyProduct->admin = $adminEpm;
        $buyProduct->total = $total;
        $buyProduct->sendapi_supplier_id = $sendApi->id;
        $buyProduct->save();

        $internalStatus = ($get_response['rc'] == '00' || $get_response['rc'] == '03') ? 0 : 1;

        $arrResult['url'] = env("URL_CHIP");
        $arrResult['status'] = $internalStatus;
        $arrResult['denom'] = $amount;
        $arrResult['content'] = json_encode($get_response);
        $arrResult['amount'] = $amount + $adminEpm;

        $content_array = $get_response;

        $nama_pelanggan = '';
        $reff_no = '';
        foreach ($content_array as $key => $value) {
            if (strpos($key, 'nama') !== false) {
                if (strlen($value) > 0) {
                    $nama_pelanggan = $value;
                }
            } elseif (strpos($key, 'reff_no') !== false) {
                if (strlen($value) > 0) {
                    $reff_no = $value;
                }
            } elseif (strpos($key, 'tagihan') !== false) {
                unset($content_array[$key]);
            } elseif (strpos($key, 'admin') !== false) {
                unset($content_array[$key]);
            } elseif (strpos($key, 'total_tagihan') !== false) {
                unset($content_array[$key]);
            }
        }

        $statusName = '';
        if ($internalStatus == 1) {
            $buyProduct->status_buy_ppob = 0;
            $buyProduct->save();

            $statusName = 'Gagal';
            $saldo = HelperController::member_saldo($buyProduct->user_id);
            HelperController::history_transaksi($buyProduct->user_id, "Refund Pembelian Gagal", $buyProduct->total, 0, $saldo);

            return [
                'status' => 4,
                'data' => $arrResult,
                'message' => 'Service not available.'
            ];
        } else {
            $buyProduct->status_buy_ppob = 1;
            $buyProduct->nama_pelanggan = $nama_pelanggan;
            $buyProduct->reff_number = $reff_no;
            $buyProduct->save();

            $statusName = 'Sukses';
            $member = Member::where('id', $buyProduct->user_id)->first();

            if ($buyProduct->payment_type == 1 && $member->user_level_id == 1) {
                if ($product != null && $product->point > 0) {
                    $bonus = new MemberBonus();
                    $bonus->nominal_bonus = $product->point;
                    $bonus->user_id = $buyProduct->user_id;
                    $bonus->payment_type = 2;
                    $bonus->save();
                }
            }

            if ($buyProduct->payment_type == 1) {
                if ($product != null && $product->status_bonus == 1) {
                    Log::channel('bonus')->info('Call function at ' . date('d-m-Y H:i:s'));
                    MemberController::count_transaction_bonus($member->id);
                }
            }

            if ($get_response['rc'] == '00') {
                $member = Member::where('id', $buyProduct->user_id)->first();
                $phone = HelperController::format_phone($member->phone);
                // fcm()
                //     ->toTopic($phone . '_android')// $topic must an string (topic name)
                //     ->notification([
                //         'title' => 'Info Transaksi',
                //         'body' => 'Transaksi pembelian ' . $product->name . ' Sukses',
                //     ])
                //     ->send();
            }

            return [
                'status' => 0,
                'data' => $arrResult,
                'message' => $options
            ];
        }

        return [
            'status' => 4,
            'data' => $arrResult,
            'message' => 'Service not available.'
        ];
    }

    public static function buy_prepaid($transactionId, $productCode, $idPelanggan, $nominal)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();

        $requestTime = date('Y-m-d H:i:s');
        $sign = ChipsaktiController::generate_sign('buy', $transactionId, $requestTime);

        $client = new Client(['http_errors' => false]);
        $params = [
            'transaction_id' => $transactionId,
            'partner_id' => env("ID_CHIP"),
            'product_code' => $productCode,
            'customer_no' => $idPelanggan,
            'merchant_code' => 'EPM',
            'request_time' => $requestTime,
            'signature' => $sign,
        ];
        $options = [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => $params,
        ];

        $response = $client->request('POST', env("URL_CHIP") . 'topup/buy', $options);
        $get_response = (array)json_decode($response->getBody()->getContents());

        $buyProduct = MemberBuyProduct::where('transaction_id', $transactionId)->first();
        $product = Product::where('id', $buyProduct->product_id)->first();

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_CHIP') . 'topup/buy';
        $sendApi->sendapi_text = json_encode($options);
        $sendApi->sendapi_response_text = json_encode($get_response);
        $sendApi->sendapi_status = $get_response['rc'];
        $sendApi->product_id = $buyProduct->product_id;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        $buyProduct->sendapi_supplier_id = $sendApi->id;
        $buyProduct->save();

        $internalStatus = ($get_response['rc'] == '00' || $get_response['rc'] == '03') ? 0 : 1;

        $statusName = '';
        if ($internalStatus == 1) {
            $buyProduct->status_buy_product = 0;
            $buyProduct->save();

            $statusName = 'Gagal';
            $member = Member::where('id', $buyProduct->user_id)->first();
            $phone = HelperController::format_phone($member->phone);

            return [
                'status' => 1,
                'message' => 'Service not available.'
            ];
        } else if ($get_response['rc'] == '00') {

            $buyProduct->status_buy_product = 1;
            $buyProduct->nama_pelanggan = '';
            $buyProduct->vsn = $get_response['sn'];
            $buyProduct->save();

            $statusName = 'Sukses';
            $member = Member::where('id', $buyProduct->user_id)->first();

            if ($buyProduct->payment_type == 1 && $member->user_level_id == 1) {
                if ($product != null && $product->point > 0) {
                    $bonus = new MemberBonus();
                    $bonus->nominal_bonus = $product->point;
                    $bonus->user_id = $buyProduct->user_id;
                    $bonus->payment_type = 2;
                    $bonus->save();
                }
            }

            if ($buyProduct->payment_type == 1) {
                if ($product != null && $product->status_bonus == 1) {
                    Log::channel('bonus')->info('Call function at ' . date('d-m-Y H:i:s'));
                    MemberController::count_transaction_bonus($member->id);
                }
            }

            $member = Member::where('id', $buyProduct->user_id)->first();
            $phone = HelperController::format_phone($member->phone);
            // fcm()
            //     ->toTopic($phone . '_android')// $topic must an string (topic name)
            //     ->notification([
            //         'title' => 'Info Transaksi',
            //         'body' => 'Transaksi pembelian ' . $product->name . ' ' . $statusName,
            //     ])
            //     ->send();

            return [
                'status' => 0,
                'message' => $options
            ];
        } else {
            return [
                'status' => 0,
                'message' => $options
            ];
        }
    }

    public static function callback_status(Request $request)
    {
    }

    public function test(Request $request)
    {

        if (!$request->input('msisdn')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `msisdn` required.'
            ], 400);
        }

        if (!$request->input('product_code')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `product_code` required.'
            ], 400);
        }

        $transaction_id = time();
        $request_time = date('Y-m-d H:i:s');
        $sign = ChipsaktiController::generate_sign('inquiry', $transaction_id, $request_time);

        $client = new Client(['http_errors' => false]);
        $params = [
            'transaction_id' => $transaction_id,
            'partner_id' => env("ID_CHIP"),
            'product_code' => $request->input('product_code'),
            'customer_no' => $request->input('msisdn'),
            'merchant_code' => 'EPM',
            'request_time' => $request_time,
            'signature' => $sign,
        ];
        $options = [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => $params,
        ];

        $response = $client->request('POST', env("URL_CHIP") . 'ppob/inquiry', $options);
        $get_response = (array)json_decode($response->getBody()->getContents());
        dd($get_response);
    }
}
