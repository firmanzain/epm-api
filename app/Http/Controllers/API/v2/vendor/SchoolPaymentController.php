<?php

namespace App\Http\Controllers\API\v2\vendor;

use App\Http\Controllers\API\v2\HelperController;
use App\Http\Controllers\API\v2\MemberController;
use App\Models\v2\Supplier;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use
    App\Models\v2\SendapiSupplier, App\Models\v2\ResultSupplier, App\Models\v2\MemberBuyProduct,
    App\Models\v2\Member, App\Models\v2\Product, App\Models\v2\MemberBuyPpob,
    App\Models\v2\MemberBonus, App\Models\v2\SupplierProduct;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;


class SchoolPaymentController implements VendorController
{
    private static function inquiry($productCode, $idPelanggan)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();

        $count = SendapiSupplier::where('supplier_id', $supplier->id)->count();
        $trxid = str_pad($count + 1, 6, '0', STR_PAD_LEFT);
        $arrResult['trx_id'] = $trxid;

        $client = new Client(['http_errors' => false]);
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ];
        $response = $client->request('GET', env("URL_SCHOOL_PAYMENT").'api/v1/payment/'.$idPelanggan, $options);

        $get_response = json_decode($response->getBody()->getContents());
        $status = $response->getStatusCode();

        $type = gettype($get_response);

        $arrResult['req'] = env("URL_SCHOOL_PAYMENT").'api/v1/payment/'.$idPelanggan;
        $arrResult['res'] = $get_response;
        $arrResult['status'] = 1;
        if ($type == 'array') {
            if ($status == 200 && sizeof($get_response) > 0) {
                $arrResult['status'] = 0;
            } else if ($status == 200 && sizeof($get_response) == 0) {
                $arrResult['status'] = 2;
            }
        }

        return $arrResult;
    }

    public static function inquiry_postpaid($productId, $productCode, $idPelanggan)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();
        $inquiry = self::inquiry($productCode, $idPelanggan);

        $result['trx_id'] = $inquiry['trx_id'];

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_SCHOOL_PAYMENT').'api/v1/payment/'.$idPelanggan;
        $sendApi->sendapi_text = $inquiry['req'];
        $sendApi->sendapi_response_text = json_encode($inquiry['res']);
        $sendApi->sendapi_status = $inquiry['status'];
        $sendApi->product_id = $productId;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        // GET ADMIN PRODUCT
        $suppProducts = SupplierProduct::where('product_id', $productId)
            ->where('supplier_id', $supplier->id)
            ->first();

        $content = (array)$inquiry['res'];
        $dateNow = Carbon::parse(date('Y-m-d'))->firstOfMonth();
        $type = gettype($inquiry['res']);

        $result['status'] = $inquiry['status'];
        $result['sendApiId'] = $sendApi->id;

        if ($type == 'object') {
            $price = $content['student']->price->nominal;
            $paid = 0;
            for ($i = 0; $i < sizeof($content['student']->payment); $i++) {
                $paid += $content['student']->payment[$i]->nominal;
            }
            if ($paid == $price) {
                $result['status'] = 2;
            }
            $result['status'] = 0;
        }

        if ($result['status'] == 0) {

            if ($type == 'object') {
                $price = $content['student']->price->nominal;
                $paid = 0;
                for ($i = 0; $i < sizeof($content['student']->payment); $i++) {
                    $paid += $content['student']->payment[$i]->nominal;
                }
                if ($paid == $price) {
                    $result['status'] = 2;
                }
                $result['status'] = 0;
            }

            $adminEpm = intval($suppProducts->biaya_admin);
            $admin = 0;
            $amount = 0;
            $debt = 0;

            $data = array();
            if ($type == 'array') {
                foreach ($content as $value) {
                    $data['NIK'] = $value->student->nik;
                    $data['Nama'] = $value->student->name;
                    $data['Orang Tua'] = $value->student->parent;
                    $data['Sekolah'] = $value->student->school->name;
                    $amount += intval($value->student->price->nominal);
                    $date = Carbon::parse($value->date)->firstOfMonth();
                    if ($date == $dateNow) {
                        $data['Tagihan'] = 'Rp' . number_format(intval($value->student->price->nominal), 0, ",", ".");
                    }
                    if (strtotime($date) < strtotime($dateNow)) {
                        $debt += intval($value->student->price->nominal);
                    }
                }
                if ($debt != 0) {
                    $data['Tunggakan'] = 'Rp' . number_format($debt, 0, ",", ".");
                }

                $result['admin'] = $admin + $adminEpm;
                $result['amount_vendor'] = $amount + $admin;
                $result['amount'] = $amount + $admin + $adminEpm;
            } else if ($type == 'object') {
                foreach ($content as  $key => $value) {
                    if ($key == 'student') {
                        $data['NIK'] = $value->nik;
                        $data['Nama'] = $value->name;
                        $data['Orang Tua'] = $value->parent;
                        $data['Sekolah'] = $value->school->name;
                        $amount += intval($price) - intval($paid);
                        $data['Tagihan'] = 'Rp' . number_format(intval($amount), 0, ",", ".");
                    }
                }

                $result['admin'] = $admin + $adminEpm;
                $result['amount_vendor'] = 0;
                $result['amount'] = $amount + $admin + $adminEpm;
            }

            $result['data'] = $data;
        }

        $result['vendor'] = "school-payment";

        return $result;
    }

    public static function pay_postpaid($transactionId, $productCode, $idPelanggan, $trxid, $amount)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();
        $buyProduct = MemberBuyPpob::where('ppob_id', $transactionId)->first();

        // GET ADMIN PRODUCT
        $suppProducts = SupplierProduct::where('product_id', $buyProduct->product_id)
        ->where('supplier_id', $supplier->id)
        ->first();

        // GET SALDO MEMBER
        $saldo = HelperController::member_saldo($buyProduct->user_id);
        if ($buyProduct->payment_type == 2) {
            $saldo = HelperController::member_point($buyProduct->user_id);
        }

        $admin = intval($suppProducts->biaya_admin);
        $total = $amount + $admin;

        $buyProduct->charge = $amount;
        $buyProduct->tagihan = $amount;
        $buyProduct->admin = $admin;
        $buyProduct->total = $total;
        $buyProduct->save();

        $nama_pelanggan = '';

        $arrResult['url'] = env("URL_SCHOOL_PAYMENT").'api/v1/payment/'.$idPelanggan;
        $arrResult['status'] = 0;
        $arrResult['amount'] = $amount;

        if ($saldo < $total) {
            $buyProduct->status_buy_ppob = 0;
            $buyProduct->save();

            HelperController::history_transaksi($buyProduct->user_id, "Refund Pembelian Gagal", $buyProduct->total, 0, $saldo);

            return [
                'status' => 3,
                'message' => 'Insufficient Ballance.'
            ];
        } else {
            $client = new Client(['http_errors' => false]);
            $json_payment = [
                'id' => $idPelanggan,
                'date' => date('Y-m-01'),
                'amount' => $amount
            ];
            $options = [
                'headers' => [
                        'Content-Type' => 'application/json',
                ],
                'json' => $json_payment,
            ];
            $response = $client->request('POST', env("URL_SCHOOL_PAYMENT").'api/v1/payment', $options);
            $get_response = json_decode($response->getBody()->getContents());
            $status = $response->getStatusCode();

            $nama_pelanggan = $get_response->name;

            // INSERT SEND API SUPPLIER
            $sendApi = new SendapiSupplier();
            $sendApi->sendapi_url = env('URL_SCHOOL_PAYMENT').'api/v1/payment';
            $sendApi->sendapi_text = json_encode($json_payment);
            $sendApi->sendapi_response_text = json_encode($get_response);
            $sendApi->sendapi_status = intval($status);
            $sendApi->product_id = $buyProduct->product_id;
            $sendApi->supplier_id = $supplier->id;
            $sendApi->save();

            if ($status == 200) {
                // SUCCESS
                $buyProduct->sendapi_supplier_id = $sendApi->id;
                $buyProduct->reff_number = 'SPP'.$idPelanggan.'-'.date('Y-m-d');
                $buyProduct->nama_pelanggan = $nama_pelanggan;
                $buyProduct->status_buy_ppob = 1;
                $buyProduct->save();

                $product = Product::where('id', $buyProduct->product_id)->first();
                $member = Member::where('id', $buyProduct->user_id)->first();

                if ($buyProduct->payment_type == 1 && $member->user_level_id == 1) {
                    if ($product != null && $product->point > 0) {
                        $bonus = new MemberBonus();
                        $bonus->nominal_bonus = $product->point;
                        $bonus->user_id = $buyProduct->user_id;
                        $bonus->payment_type = 2;
                        $bonus->save();
                    }
                }

                if ($buyProduct->payment_type == 1) {
                    if ($product != null && $product->status_bonus == 1) {
                        Log::channel('bonus')->info('Call function at '.date('d-m-Y H:i:s'));
                        MemberController::count_transaction_bonus($member->id);
                    }
                }

                return [
                    'status' => 0,
                    'message' => $json_payment
                ];
            } else {
                // GAGAL
                $buyProduct->sendapi_supplier_id = $sendApi->id;
                $buyProduct->status_buy_ppob = 0;
                $buyProduct->save();

                HelperController::history_transaksi($buyProduct->user_id, "Refund Pembelian Gagal", $buyProduct->total, 0, $saldo);

                return [
                    'status' => 4,
                    'message' => 'Service not available.'
                ];
            }
        }
    }

    public static function buy_prepaid($transactionId, $productCode, $idPelanggan, $nominal)
    {
        return [
            'status' => 1,
            'message' => 'Service not available.'
        ];
    }

    public static function callback_status(Request $request)
    {
        return response()->json([
            'code' => 200,
            'message' => 'Transaction updated successfully'
        ], 200);
    }
}
