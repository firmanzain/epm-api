<?php
/**
 * Created by PhpStorm.
 * User: mhasan
 * Date: 24/02/19
 * Time: 14:45
 */

namespace App\Http\Controllers\API\v2\vendor;

use Illuminate\Http\Request;

interface VendorController
{
    public static function inquiry_postpaid($productId, $productCode, $idPelanggan);
    public static function pay_postpaid($transactionId, $productCode, $idPelanggan, $trxid, $amount);
    public static function buy_prepaid($transactionId, $productCode, $idPelanggan, $nominal);
    public static function callback_status(Request $request);
}
