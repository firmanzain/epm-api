<?php

namespace App\Http\Controllers\API\v2\vendor;

use App\Http\Controllers\API\v2\HelperController;
use App\Http\Controllers\API\v2\MemberController;
use App\Models\v2\Supplier;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use
    App\Models\v2\SendapiSupplier, App\Models\v2\ResultSupplier, App\Models\v2\MemberBuyProduct,
    App\Models\v2\Member, App\Models\v2\Product, App\Models\v2\MemberBuyPpob,
    App\Models\v2\MemberBonus, App\Models\v2\SupplierProduct;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;


class SynetController implements VendorController
{
    private static function inquiry($productCode, $idPelanggan)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();

        $count = SendapiSupplier::where('supplier_id', $supplier->id)->count();
        $trxid = str_pad($count + 1, 6, '0', STR_PAD_LEFT);
        $arrResult['trx_id'] = $trxid;

        $client = new Client(['http_errors' => false]);
        $params = [
            'id' => env("ID_SYNET"),
            'pin' => env("PIN_SYNET"),
            'user' => env("USER_SYNET"),
            'pass' => env("PASS_SYNET"),
            'kodeproduk' => $productCode,
            'tujuan' => $idPelanggan,
            'idtrx' => $trxid,
            'jenis' => 5,
        ];
        $options = [
            'query' => $params
        ];

        try {
          $response = $client->request('GET', env("URL_SYNET"), $options);
        } catch (RequestException $e) {
          $arrResult['req'] = $params;
          $arrResult['res'] = array();
          $arrResult['status'] = 1;

          return $arrResult;
        }

        $response = $client->request('GET', env("URL_SYNET"), $options);

        $get_response = (array)json_decode($response->getBody()->getContents());

        $arrResult['req'] = $params;
        $arrResult['res'] = $get_response;
        $arrResult['status'] = ($get_response['success'] == 1) ? 0 : 1;

        return $arrResult;
    }

    public static function inquiry_postpaid($productId, $productCode, $idPelanggan)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();
        $inquiry = self::inquiry($productCode, $idPelanggan);

        $result['trx_id'] = $inquiry['trx_id'];

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_SYNET');
        $sendApi->sendapi_text = json_encode($inquiry['req']);
        $sendApi->sendapi_response_text = json_encode($inquiry['res']);
        $sendApi->sendapi_status = $inquiry['status'];
        $sendApi->product_id = $productId;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        // GET ADMIN PRODUCT
        $suppProducts = SupplierProduct::where('product_id', $productId)
            ->where('supplier_id', $supplier->id)
            ->first();

        $dateNow = Carbon::parse(date('Y-m-d'))->firstOfMonth();
        $result['vendor'] = "synet";

        $result['status'] = $inquiry['status'];
        $result['sendApiId'] = $sendApi->id;
        if ($inquiry['status'] == 0) {

            $content_array = array();

            if (self::startsWith(strtolower($inquiry['res']['msg']), strtolower('Cek'))) {
                if (strpos($inquiry['res']['msg'], "\r\n") !== false) {
                    $msg = explode("\r\n", $inquiry['res']['msg']);
                    $content = $msg;
                    for($i = 0; $i < count($content); $i++){
                        if ($i != 0) {
                            $key_value = explode(':', $content[$i]);
                            $content_array[ltrim($key_value[0])] = ltrim($key_value[1]);
                        }
                    }
                } else {
                    $msg = explode("SUKSES.", $inquiry['res']['msg']);
                    $msg = explode(",", $msg[1]);
                    $content = $msg;
                    for($i = 0; $i < count($content); $i++){
                        $key_value = explode(':', $content[$i]);
                        $content_array[ltrim($key_value[0])] = ltrim(str_replace(".", "", $key_value[1]));
                    }
                }
            } else if (self::startsWith($inquiry['res']['msg'], 'REFF')) {
                $inquiry['res']['msg'] = str_replace("ID PEL", "IDPEL", $inquiry['res']['msg']);
                $msg = explode("BERHASIL", $inquiry['res']['msg']);
                $msg = explode("/", $msg[1]);
                $msg[0] = ltrim($msg[0]);
                $info = explode(" ", $msg[0]);
                for ($i = 0; $i < sizeof($info); $i++) {
                    array_push($msg, $info[$i]);
                }
                $content = $msg;
                for($i = 0; $i < count($content); $i++){
                    if ($i != 0) {
                        if (strpos($content[$i], ':') !== false) {
                            $key_value = explode(':', $content[$i]);
                            $content_array[ltrim($key_value[0])] = ltrim($key_value[1]);
                        }
                    }
                }
            } else if (self::startsWith($inquiry['res']['msg'], 'INQ')) {
                if (strpos($inquiry['res']['msg'], '@DATA:') !== false) {
                    $inquiry['res']['msg'] = str_replace("DATA:", "", $inquiry['res']['msg']);
                    $msg = explode("@", $inquiry['res']['msg']);
                    $info = explode(",", $msg[1]);
                    $ket = explode(": ", $info[2]);
                    $info[2] = $ket[0];
                    array_push($info, $msg[2]);
                    array_push($info, str_replace(".", "", $msg[3]));
                    $content = $info;
                    for($i = 0; $i < count($content); $i++){
                        if (strpos($content[$i], ':') !== false) {
                            $key_value = explode(':', $content[$i]);
                            $content_array[ltrim($key_value[0])] = ltrim($key_value[1]);
                        }
                    }
                } else {
                    $inquiry['res']['msg'] = str_replace(" PERIODE:", ", PERIODE:", $inquiry['res']['msg']);
                    $info = explode("SUKSES.", $inquiry['res']['msg']);
                    $info = explode(",", $info[1]);
                    $content = $info;
                    for($i = 0; $i < count($content); $i++){
                        if (strpos($content[$i], ':') !== false) {
                            $key_value = explode(':', $content[$i]);
                            $content_array[ltrim($key_value[0])] = ltrim($key_value[1]);
                        }
                    }
                    array_pop($content_array);
                }
            }

            $fee = intval($suppProducts->fee);
            $adminEpm = $suppProducts->biaya_admin;
            $admin = 0;
            $amount = 0;

            $data = array();
            $dataDebug = array();
            foreach ($content_array as $key => $value) {
                if (strpos(strtolower($key), 'sn') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('sn', 'Nama Pelanggan', strtolower($key));
                        $data[$newKey] = $value;
                    }
                } elseif (strpos(strtolower($key), 'atas nama') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('atas nama', 'Nama Pelanggan', strtolower($key));
                        $data[$newKey] = $value;
                    }
                } elseif (strpos(strtolower($key), 'nama') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('nama', 'Nama Pelanggan', strtolower($key));
                        $data[$newKey] = $value;
                    }
                } elseif (strpos(strtolower($key), 'id pel') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('id pel', 'ID Pelanggan', strtolower($key));
                        $data[$newKey] = $value;
                    }
                } elseif (strpos(strtolower($key), 'idpel') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('idpel', 'ID Pelanggan', strtolower($key));
                        $data[$newKey] = $value;
                    }
                } elseif (strpos(strtolower($key), 'jmlbln') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('jmlbln', 'Jumlah Periode', strtolower($key));
                        $data[$newKey] = $value;
                    }
                } elseif (strpos(strtolower($key), 'jml') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('jml', 'Jumlah Periode', strtolower($key));
                        $data[$newKey] = $value;
                    }
                } elseif (strpos(strtolower($key), 'periode') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('periode', 'Periode', strtolower($key));
                        $data[$newKey] = $value;
                    }
                } elseif (strpos(strtolower($key), 'ket') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('ket', 'Keterangan', strtolower($key));
                        $data[$newKey] = $value;
                    }
                } elseif (strpos(strtolower($key), 'rp') !== false) {
                    if (strlen($value) > 0) {
                        $dataDebug[$key] = str_replace('rp', 'Amount', strtolower($key));
                        $data['Tagihan'] = 'Rp'.number_format($value, 0, ",", ".");
                        $amount += intval($value);
                    }
                } elseif (strpos(strtolower($key), 'tagihan') !== false) {
                    if (strlen($value) > 0) {
                        $dataDebug[$key] = str_replace('tagihan', 'Amount', strtolower($key));
                        $data['Tagihan'] = 'Rp'.number_format($value, 0, ",", ".");
                        $amount += intval(str_replace('Rp.', '', $value));
                    }
                } elseif (strpos(strtolower($key), 'totalbayar') !== false) {
                    if (strlen($value) > 0) {
                        $dataDebug[$key] = str_replace('totalbayar', 'Amount', strtolower($key));
                        $data['Tagihan'] = 'Rp'.number_format($value, 0, ",", ".");
                        $amount += intval(str_replace('Rp.', '', $value));
                    }
                } elseif (strpos(strtolower($key), 'denda') !== false) {
                    if (strlen($value) > 0) {
                        $dataDebug[$key] = str_replace('denda', 'Denda', strtolower($key));
                        $data['Tagihan'] = 'Rp'.number_format($value, 0, ",", ".");
                        $amount += intval(str_replace('Rp.', '', $value));
                    }
                } elseif (strpos(strtolower($key), 'adm') !== false) {
                    if (strlen($value) > 0) {
                        $admin += intval(str_replace('Rp.', '', $value));
                    }
                } elseif (strpos(strtolower($key), 'admin') !== false) {
                    if (strlen($value) > 0) {
                        $admin += intval(str_replace('Rp.', '', $value));
                    }
                }
                $dataDebug[$key] = $value;
            }
            // $data['Admin'] = 'Rp'.number_format($admin + $adminEpm, 0, ",", ".");
            $data['Admin'] = 'Rp'.number_format($adminEpm, 0, ",", ".");

            if (isset($data['Tagihan'])) {
                $amount -= $admin;
                $data['Tagihan'] = 'Rp'.number_format($amount, 0, ",", ".");
            }

            // $result['admin'] = $admin + $adminEpm;
            // $result['amount_vendor'] = $amount + $admin + $fee;
            // $result['amount'] = $amount + $admin + $adminEpm;
            $result['admin'] = $adminEpm;
            $result['amount_vendor'] = $amount + $fee;
            $result['amount'] = $amount + $adminEpm;

            $result['status'] = 0;
            $result['data'] = $data;
            $result['data_debug'] = $dataDebug;
        } else {
            $result['admin'] = 0;
            $result['amount_vendor'] = 0;
            $result['amount'] = 0;
            $result['status'] = 3;
            $result['data'] = $inquiry;
        }

        return $result;
    }

    public static function pay_postpaid($transactionId, $productCode, $idPelanggan, $trxid, $amount)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();
        // $inquiry = self::inquiry($productCode, $idPelanggan);

        $buyProduct = MemberBuyPpob::where('ppob_id', $transactionId)->first();

        // GET ADMIN PRODUCT
        $suppProducts = SupplierProduct::where('product_id', $buyProduct->product_id)
            ->where('supplier_id', $supplier->id)
            ->first();

        $client = new Client(['http_errors' => false]);
        $params = [
            'id' => env("ID_SYNET"),
            'pin' => env("PIN_SYNET"),
            'user' => env("USER_SYNET"),
            'pass' => env("PASS_SYNET"),
            'kodeproduk' => $productCode,
            'tujuan' => $idPelanggan,
            'idtrx' => $transactionId,
            'jenis' => 6,
        ];
        $options = [
            'query' => $params
        ];
        $response = $client->request('GET', env("URL_SYNET"), $options);

        $get_response = (array)json_decode($response->getBody()->getContents());

        Log::channel('paypostpaid')->info('Log inquiry payment: user = '.$buyProduct->user_id);
        Log::channel('paypostpaid')->info(json_encode($options));

        $product = Product::where('id', $buyProduct->product_id)->first();

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_SYNET');
        $sendApi->sendapi_text = json_encode($options);
        $sendApi->sendapi_response_text = json_encode($get_response);
        $sendApi->sendapi_status = $get_response['success'];
        $sendApi->product_id = $buyProduct->product_id;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        $fee = intval($suppProducts->fee);
        $adminEpm = $suppProducts->biaya_admin;
        $total = $amount - $fee + $adminEpm;

        $buyProduct->charge = $amount;
        $buyProduct->tagihan = $amount - $fee;
        $buyProduct->admin = $adminEpm;
        $buyProduct->total = $total;
        $buyProduct->sendapi_supplier_id = $sendApi->id;
        $buyProduct->save();

        $internalStatus = ($get_response['success']) ? 0 : 1;

        $arrResult['url'] = env("URL_SYNET");
        $arrResult['status'] = $internalStatus;
        $arrResult['denom'] = $amount;
        $arrResult['content'] = $get_response['msg'];
        $arrResult['amount'] = $amount + $adminEpm;

        $content_array = array();

        if (self::startsWith($get_response['msg'], 'Cek')) {
            $msg = explode("\r\n", $get_response['msg']);
            $content = $msg;
            for($i = 0; $i < count($content); $i++){
                if ($i != 0) {
                    $key_value = explode(':', $content[$i]);
                    $content_array[ltrim($key_value[0])] = ltrim($key_value[1]);
                }
            }
        } else if (self::startsWith($get_response['msg'], 'REFF')) {
            $get_response['msg'] = str_replace("ID PEL", "IDPEL", $get_response['msg']);
            $msg = explode("BERHASIL", $get_response['msg']);
            $msg = explode("/", $msg[1]);
            $msg[0] = ltrim($msg[0]);
            $info = explode(" ", $msg[0]);
            for ($i = 0; $i < sizeof($info); $i++) {
                array_push($msg, $info[$i]);
            }
            $content = $msg;
            for($i = 0; $i < count($content); $i++){
                if ($i != 0) {
                    if (strpos($content[$i], ':') !== false) {
                        $key_value = explode(':', $content[$i]);
                        $content_array[ltrim($key_value[0])] = ltrim($key_value[1]);
                    }
                }
            }
        } else if (self::startsWith($get_response['msg'], 'INQ')) {
            $get_response['msg'] = str_replace("DATA:", "", $get_response['msg']);
            $msg = explode("@", $get_response['msg']);
            $info = explode(",", $msg[1]);
            $ket = explode(": ", $info[2]);
            $info[2] = $ket[0];
            array_push($info, $msg[2]);
            array_push($info, str_replace(".", "", $msg[3]));
            $content = $info;
            for($i = 0; $i < count($content); $i++){
                if (strpos($content[$i], ':') !== false) {
                    $key_value = explode(':', $content[$i]);
                    $content_array[ltrim($key_value[0])] = ltrim($key_value[1]);
                }
            }
        } else if (self::startsWith($get_response['msg'], 'BAYAR')) {
            $get_response['sn'] = 'NPEL:'.$get_response['sn'];
            $msg = explode("/", $get_response['sn']);
            $content = $msg;
            for($i = 0; $i < count($content); $i++){
                if (strpos($content[$i], ':') !== false) {
                    $key_value = explode(':', $content[$i]);
                    $content_array[ltrim($key_value[0])] = ltrim($key_value[1]);
                }
            }
        }

        $nama_pelanggan = '';
        $reff_no = '';
        foreach ($content_array as $key => $value) {
            if (strpos($key, 'SN') !== false) {
                if (strlen($value) > 0) {
                    $nama_pelanggan = $value;
                }
            } elseif (strpos($key, 'REF') !== false) {
                if (strlen($value) > 0) {
                    $reff_no = $value;
                }
            } elseif (strpos($key, 'NPEL') !== false) {
                if (strlen($value) > 0) {
                    $nama_pelanggan = $value;
                }
            } elseif (strpos($key, 'REFF') !== false) {
                if (strlen($value) > 0) {
                    $reff_no = $value;
                }
            } elseif (strpos($key, 'RP') !== false) {
                unset($content_array[$key]);
            } elseif (strpos($key, 'TAGIHAN') !== false) {
                unset($content_array[$key]);
            } elseif (strpos($key, 'ADM') !== false) {
                unset($content_array[$key]);
            } elseif (strpos($key, 'ADMIN') !== false) {
                unset($content_array[$key]);
            }
        }

        $statusName = '';
        if ($internalStatus == 1) {
            $buyProduct->status_buy_ppob = 0;
            $buyProduct->save();

            $statusName = 'Gagal';
            $saldo = HelperController::member_saldo($buyProduct->user_id);
            HelperController::history_transaksi($buyProduct->user_id, "Refund Pembelian Gagal", $buyProduct->total, 0, $saldo);

            return [
                'status' => 4,
                'data' => $arrResult,
                'message' => 'Service not available.'
            ];
        } else {
            // $buyProduct->data = json_encode($content_array);
            $buyProduct->status_buy_ppob = 1;
            $buyProduct->nama_pelanggan = $nama_pelanggan;
            $buyProduct->reff_number = $reff_no;
            $buyProduct->save();

            $statusName = 'Sukses';
            $member = Member::where('id', $buyProduct->user_id)->first();

            if ($buyProduct->payment_type == 1 && $member->user_level_id == 1) {
            if ($product != null && $product->point > 0) {
                    $bonus = new MemberBonus();
                    $bonus->nominal_bonus = $product->point;
                    $bonus->user_id = $buyProduct->user_id;
                    $bonus->payment_type = 2;
                    $bonus->save();
                }
            }

            if ($buyProduct->payment_type == 1) {
                if ($product != null && $product->status_bonus == 1) {
                    Log::channel('bonus')->info('Call function at '.date('d-m-Y H:i:s'));
                    MemberController::count_transaction_bonus($member->id);
                }
            }

            return [
                'status' => 0,
                'data' => $arrResult,
                'message' => $options
            ];
        }

        return [
            'status' => 4,
            'data' => $arrResult,
            'message' => 'Service not available.'
        ];
    }

    public static function buy_prepaid($transactionId, $productCode, $idPelanggan, $nominal)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();

        $client = new Client(['http_errors' => false]);
        $params = [
            'id' => env("ID_SYNET"),
            'pin' => env("PIN_SYNET"),
            'user' => env("USER_SYNET"),
            'pass' => env("PASS_SYNET"),
            'kodeproduk' => $productCode,
            'tujuan' => $idPelanggan,
            'idtrx' => $transactionId,
        ];
        $options = [
            'query' => $params
        ];
        $response = $client->request('GET', env("URL_SYNET"), $options);

        $get_response = (array)json_decode($response->getBody()->getContents());

        $buyProduct = MemberBuyProduct::where('transaction_id', $transactionId)->first();
        $product = Product::where('id', $buyProduct->product_id)->first();

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_SYNET');
        $sendApi->sendapi_text = json_encode($options);
        $sendApi->sendapi_response_text = json_encode($get_response);
        $sendApi->sendapi_status = $get_response['success'];
        $sendApi->product_id = $buyProduct->product_id;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        $buyProduct->sendapi_supplier_id = $sendApi->id;
        $buyProduct->save();

        $internalStatus = ($get_response['success']) ? 0 : 1;

        $statusName = '';
        if ($internalStatus == 1) {
            $buyProduct->status_buy_product = 0;
            $buyProduct->save();

            $statusName = 'Gagal';
            $member = Member::where('id', $buyProduct->user_id)->first();
            $phone = HelperController::format_phone($member->phone);

            return [
                'status' => 1,
                'message' => 'Service not available.'
            ];
        } else {
            $statusSuccess = intval($get_response['rc']);
            if ($statusSuccess != 0) {
                return [
                    'status' => 0,
                    'message' => $options
                ];
            }
        }
    }

    public static function callback_status(Request $request)
    {
        // INSERT RESULT SUPPLIER
        $resultSupplier = new ResultSupplier();
        $resultSupplier->result_text = json_encode($request->all());
        $resultSupplier->result_response_text = json_encode($request->all());
        $resultSupplier->result_status = ($request->get('statuscode') == 1) ? 1 : 0;
        $resultSupplier->supplier_id = 4;
        $resultSupplier->save();
        // END INSERT RESULT SUPPLIER

        $status = ($request->get('statuscode') == 1) ? 1 : 0;

        if (strpos($request->get('clientid'), 'PREPAID') !== false) {
            // PREPAID
            $buyProduct = MemberBuyProduct::where('transaction_id', $request->get('clientid'))->first();
            if (!$buyProduct) {
                return response()->json([
                    'code' => 404,
                    'message' => 'Transaction not found.'
                ], 404);
            }

            $product = Product::where('id', $buyProduct->product_id)->first();

            $content_array = array(
                'sn' => $request->get('sn'),
            );

            $buyProduct->result_supplier_id = $resultSupplier->id;
            $buyProduct->status_buy_product = $status;
            $buyProduct->vsn = $request->get('sn');
            // $buyProduct->data = json_encode($content_array);
            $buyProduct->save();

            $member = Member::where('id', $buyProduct->user_id)->first();
            $phone = HelperController::format_phone($member->phone);

            $statusName = '';
            if ($status == 0) {
                $statusName = 'Sukses';
                $member = Member::where('id', $buyProduct->user_id)->first();

                if ($buyProduct->payment_type == 1 && $member->user_level_id == 1) {
                    if ($product != null && $product->point > 0) {
                        $bonus = new MemberBonus();
                        $bonus->nominal_bonus = $product->point;
                        $bonus->user_id = $buyProduct->user_id;
                        $bonus->payment_type = 2;
                        $bonus->save();
                    }
                }

                if ($buyProduct->payment_type == 1) {
                    if ($product != null && $product->status_bonus == 1) {
                        Log::channel('bonus')->info('Call function at '.date('d-m-Y H:i:s'));
                        MemberController::count_transaction_bonus($member->id);
                    }
                }

                fcm()
                    ->toTopic($phone . '_android')// $topic must an string (topic name)
                    ->notification([
                        'title' => 'Info Transaksi',
                        // 'body' => 'Transaksi pembelian ' . $product->name . ' Sukses',
                        'body' => 'Transaksi pembelian ' . $product->name .' telah selesai, lihat history untuk mengetahui status transaksi',
                    ])
                    ->send();
            } else {
                $statusName = 'Gagal';
                $saldo = HelperController::member_saldo($buyProduct->user_id);
                HelperController::history_transaksi($buyProduct->user_id, "Refund Pembelian Gagal", $buyProduct->buy_product_price, 0, $saldo);

                fcm()
                    ->toTopic($phone . '_android')// $topic must an string (topic name)
                    ->notification([
                        'title' => 'Info Transaksi',
                        // 'body' => 'Transaksi pembelian ' . $product->name . ' Gagal',
                        'body' => 'Transaksi pembelian ' . $product->name .' telah selesai, lihat history untuk mengetahui status transaksi',
                    ])
                    ->send();
            }

        } else {
            // POSTPAID
            // $buyProduct = MemberBuyPpob::where('ppob_id', $request->get('clientid'))->first();
            // if (!$buyProduct) {
            //     return response()->json([
            //         'code' => 404,
            //         'message' => 'Transaction not found.'
            //     ], 404);
            // }
            //
            // $product = Product::where('id', $buyProduct->product_id)->first();
            //
            // $buyProduct->status_buy_ppob = 1;
            // $buyProduct->nama_pelanggan = $nama_pelanggan;
            // $buyProduct->reff_number = $request->get('sn');
            // $buyProduct->save();
        }

        return response()->json([
            'code' => 200,
            'message' => 'Transaction updated successfully'
        ], 200);
    }

    private static function startsWith ($string, $startString)
    {
        $len = strlen($startString);
        return (substr($string, 0, $len) === $startString);
    }
}
