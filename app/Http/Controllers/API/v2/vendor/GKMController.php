<?php

namespace App\Http\Controllers\API\v2\vendor;

use App\Http\Controllers\API\v2\HelperController;
use App\Http\Controllers\API\v2\MemberController;
use App\Http\Controllers\Controller;
use App\Models\v2\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\ArrayToXml\ArrayToXml;
use Midnite81\Xml2Array\Xml2Array;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use
    App\Models\v2\SendapiSupplier, App\Models\v2\ResultSupplier, App\Models\v2\MemberBuyProduct,
    App\Models\v2\Member, App\Models\v2\Product, App\Models\v2\MemberBuyPpob,
    App\Models\v2\MemberBonus, App\Models\v2\SupplierProduct;
use Illuminate\Support\Facades\Log;


class GKMController implements VendorController
{
    private static function query_array($array, $length, $id)
    {
        $value = '';
        for ($x = 0; $x < $length; $x++) {
            if ($array[$x]['@attributes']['id'] == $id) {
                $value = $array[$x]['@attributes']['value'];
            }
        }
        return $value;
    }

    private static function get_content($array, $length, $id)
    {
        $value = '';
        for ($x = 0; $x < $length; $x++) {
            if (array_key_exists("@content", $array[$x])) {
                $value = $array[$x]['@content'];
            }
        }
        return $value;
    }

    /**
     * Fungsi internal GKMController untuk inquiry
     *
     * @param $productCode
     * @param $idPelanggan
     * @return array
     * @throws GuzzleException
     */
    private static function inquiry($productCode, $idPelanggan)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();

        $count = SendapiSupplier::where('supplier_id', $supplier->id)->count();
        $trxid = str_pad($count + 1, 6, '0', STR_PAD_LEFT);
        $time = date('mdHis');
        $kode_griya = env("KODE_GRIYA");
        $kode_41_griya = env("KODE_41_GRIYA");
        $kode_42_griya = env("KODE_42_GRIYA");

        $array_inquiry_ppob = [
            'field' => [
                ["_attributes" => ["id" => "0", "value" => "0200"]],
                ["_attributes" => ["id" => "3", "value" => "380000"]],
                ["_attributes" => ["id" => "4", "value" => "0"]],
                ["_attributes" => ["id" => "7", "value" => $time]],
                ["_attributes" => ["id" => "11", "value" => $trxid]],
                ["_attributes" => ["id" => "18", "value" => "7011"]],
                ["_attributes" => ["id" => "32", "value" => $kode_griya]],
                ["_attributes" => ["id" => "41", "value" => $kode_41_griya]],
                ["_attributes" => ["id" => "42", "value" => $kode_42_griya]],
                ["_attributes" => ["id" => "43", "value" => "EPM"]],
                ["_attributes" => ["id" => "61", "value" => $idPelanggan]],
                ["_attributes" => ["id" => "98", "value" => $productCode]]
            ]
        ];
        $xml_inquiry = ArrayToXml::convert($array_inquiry_ppob, 'isomsg', false);

        $client = new Client();
        $options = [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Content-length' => '<XML Message Body Length>',
            ],
            'body' => $xml_inquiry,
        ];

        $response = $client->request('POST', env("URL_GRIYA"), $options);

        $get_response = $response->getBody()->getContents();

        $xml = Xml2Array::create($get_response);

        $xml_array = $xml->toArray();

        $arrResult['trx_id'] = $trxid;
        $arrResult['status'] = static::query_array($xml_array['field'], count($xml_array['field']), '39');
        $arrResult['amount'] = static::query_array($xml_array['field'], count($xml_array['field']), '4');
        $arrResult['content'] = json_decode(static::get_content($xml_array['field'], count($xml_array['field']), '62'));

        $arrResult['res'] = $get_response;
        $arrResult['req'] = $xml_inquiry;
        $arrResult['sts'] = $arrResult['status'];

        return $arrResult;
    }

    /**
     * Fungsi dengan response standar internal project
     * yang akan digunakan pada ProductController
     * menggunakan teknik Polymorphism
     *
     * @param $productCode
     * @param $idPelanggan
     * @return array
     * @throws GuzzleException
     */
    public static function inquiry_postpaid($productId, $productCode, $idPelanggan)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();
        $inquiry = self::inquiry($productCode, $idPelanggan);

        $result['trx_id'] = $inquiry['trx_id'];

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_GRIYA');
        $sendApi->sendapi_text = $inquiry['req'];
        $sendApi->sendapi_response_text = $inquiry['res'];
        $sendApi->sendapi_status = $inquiry['sts'];
        $sendApi->product_id = $productId;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        // GET ADMIN PRODUCT
        $suppProducts = SupplierProduct::where('product_id', $productId)
            ->where('supplier_id', $supplier->id)
            ->first();

        $content = (array)$inquiry['content'];
        $result['vendor'] = "gkm";

        if ($inquiry['status'] == '00') {

            $fee = intval($suppProducts->fee);
            $adminEpm = $suppProducts->biaya_admin;
            $admin = 0;
            $amount = 0;

            $data = array();
            $dataDebug = array();
            foreach ($content as $key => $value) {
                if (strpos($key, 'CustName') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('CustName', 'Nama Pelanggan', $key);
                        $data[$newKey] = ltrim(rtrim($value));
                    }
                } elseif (strpos($key, 'NoOfMember') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('NoOfMember', 'Jumlah Anggota', $key);
                        $data[$newKey] = $value;
                    }
                } elseif (strpos($key, 'Periods') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('Periods', 'Jumlah Periode', $key);
                        $data[$newKey] = $value;
                    }
                } elseif (strpos($key, 'Period') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('Period', 'Periode', $key);
                        // $data[$newKey] = Carbon::createFromFormat('Ym', $value)->format('M Y');
                        $data[$newKey] = $value;
                    }
                } elseif (strpos($key, 'Premi') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('Premi', 'Iuran', $key);
                        $data[$newKey] = 'Rp' . number_format($value, 0, ",", ".");
                    }
                } elseif (strpos($key, 'Penalty') !== false) {
                    if (strlen($value) > 0 && $value != 0) {
                        $newKey = str_replace('Penalty', 'Denda', $key);
                        $data[$newKey] = 'Rp' . number_format($value, 0, ",", ".");
                        $amount += intval($value);
                    }
                } elseif (strpos($key, 'Amount') !== false) {
                    if (strlen($value) > 0 && $value != 0) {
                        $newKey = str_replace('Amount', ' Tagihan', $key);
                        $data[$newKey] = 'Rp' . number_format($value, 0, ",", ".");
                        $amount += intval($value);
                    }
                } elseif (strpos($key, 'Penalty1') !== false) {
                    if (strlen($value) > 0 && $value != 0) {
                        $amount += intval($value);
                    }
                } elseif (strpos($key, 'Penalty2') !== false) {
                    if (strlen($value) > 0 && $value != 0) {
                        $amount += intval($value);
                    }
                } elseif (strpos($key, 'AdminFee') !== false) {
                    if (strlen($value) > 0 && $value != 0) {
                        $admin += intval($value);
                    }
                }
                $dataDebug[$key] = $value;
            }
            // $data['Admin'] = 'Rp'.number_format($admin + $adminEpm, 0, ",", ".");
            $data['Admin'] = 'Rp' . number_format($adminEpm, 0, ",", ".");

            if (isset($data['Tagihan'])) {
                $amount -= $admin;
                $data['Tagihan'] = 'Rp' . number_format($amount, 0, ",", ".");
            }

            if (isset($data['Total Tagihan'])) {
                $amount -= $admin;
                $data['Total Tagihan'] = 'Rp' . number_format($amount, 0, ",", ".");
            }

            // $result['admin'] = $admin + $adminEpm;
            // $result['amount_vendor'] = $amount + $admin + $fee;
            // $result['amount'] = $amount + $admin + $adminEpm;
            $result['admin'] = $adminEpm;
            $result['amount_vendor'] = $amount + $fee;
            $result['amount'] = $amount + $adminEpm;

            $result['data'] = $data;
            $result['status'] = 0;
            $result['data_debug'] = $dataDebug;
        } else if ($inquiry['status'] == '14') {
            // INVALID CUSTOMER
            $result['admin'] = 0;
            $result['amount_vendor'] = 0;
            $result['amount'] = 0;
            $result['data'] = $inquiry;
            $result['status'] = 1;
            $result['sendApiId'] = $sendApi->id;
        } else if ($inquiry['status'] == '88') {
            // ALREADY PAID
            $result['admin'] = 0;
            $result['amount_vendor'] = 0;
            $result['amount'] = 0;
            $result['data'] = $inquiry;
            $result['status'] = 2;
            $result['sendApiId'] = $sendApi->id;
        } else {
            $result['admin'] = 0;
            $result['amount_vendor'] = 0;
            $result['amount'] = 0;
            $result['data'] = $inquiry;
            $result['status'] = 99;
            $result['sendApiId'] = $sendApi->id;
        }

        return $result;
    }

    public static function pay_postpaid($transactionId, $productCode, $idPelanggan, $trxid, $amount)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();

        $buyProduct = MemberBuyPpob::where('ppob_id', $transactionId)->first();

        // GET ADMIN PRODUCT
        $suppProducts = SupplierProduct::where('product_id', $buyProduct->product_id)
            ->where('supplier_id', $supplier->id)
            ->first();

        $trxid = (int)substr($transactionId, 11);
        $time = date('mdHis');
        $kode_griya = env("KODE_GRIYA");
        $kode_41_griya = env("KODE_41_GRIYA");
        $kode_42_griya = env("KODE_42_GRIYA");

        $array_inquiry = [
            'field' => [
                ["_attributes" => ["id" => "0", "value" => "0200"]],
                ["_attributes" => ["id" => "3", "value" => "580000"]],
                ["_attributes" => ["id" => "4", "value" => $amount]],
                ["_attributes" => ["id" => "7", "value" => $time]],
                ["_attributes" => ["id" => "11", "value" => $trxid]],
                ["_attributes" => ["id" => "18", "value" => "7011"]],
                ["_attributes" => ["id" => "32", "value" => $kode_griya]],
                ["_attributes" => ["id" => "41", "value" => $kode_41_griya]],
                ["_attributes" => ["id" => "42", "value" => $kode_42_griya]],
                ["_attributes" => ["id" => "43", "value" => "EPM"]],
                ["_attributes" => ["id" => "61", "value" => $idPelanggan]],
                ["_attributes" => ["id" => "98", "value" => $productCode]]
            ]
        ];
        $xml_inquiry = ArrayToXml::convert($array_inquiry, 'isomsg', false);

        Log::channel('paypostpaid')->info('Log inquiry payment: user = ' . $buyProduct->user_id);
        Log::channel('paypostpaid')->info($xml_inquiry);

        $client = new Client();
        $options = [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Content-length' => '<XML Message Body Length>',
            ],
            'body' => $xml_inquiry,
        ];
        $response = $client->request('POST', env("URL_GRIYA"), $options);

        $get_response = $response->getBody()->getContents();

        $xml = Xml2Array::create($get_response);

        $xml_array = $xml->toArray();

        $arrResult['url'] = env("URL_GRIYA");

        $responseStatus = static::query_array($xml_array['field'], count($xml_array['field']), '39');
        $arrResult['status'] = ($responseStatus != "00" && $responseStatus != "68") ? 1 : 0;

        $arrResult['denom'] = static::query_array($xml_array['field'], count($xml_array['field']), '4');
        $arrResult['content'] = json_decode(static::get_content($xml_array['field'], count($xml_array['field']), '62'));

        $content = json_decode(static::get_content($xml_array['field'], count($xml_array['field']), '62'));

        $nama_pelanggan = '';
        $reff_no = static::query_array($xml_array['field'], count($xml_array['field']), '37');

        $fee = intval($suppProducts->fee);
        $adminEpm = $suppProducts->biaya_admin;
        $admin = 0;
        $amount = $amount - $fee;
        // $amount = 0;

        if ($content) {
            foreach ($content as $key => $value) {
                if (strpos($key, 'CustName') !== false) {
                    $nama_pelanggan = $value;
                } else if (strpos($key, 'CustName1') !== false) {
                    $nama_pelanggan = $value;
                } else if (strpos($key, 'RefNo') !== false) {
                    $reff_no = $value;
                }

                // if (strpos($key, 'Penalty') !== false) {
                //     if (strlen($value) > 0 && $value != 0) {
                //         $amount += intval($value);
                //     }
                // } elseif (strpos($key, 'Amount') !== false) {
                //     if (strlen($value) >  0 && $value != 0) {
                //         $amount += intval($value);
                //     }
                // } elseif (strpos($key, 'Penalty1') !== false) {
                //     if (strlen($value) > 0 && $value != 0) {
                //         $amount += intval($value);
                //     }
                // } elseif (strpos($key, 'Penalty2') !== false) {
                //     if (strlen($value) > 0 && $value != 0) {
                //         $amount += intval($value);
                //     }
                // }
            }
        }

        // $admin = $suppProducts->biaya_admin;
        // $total = $amount + $admin;
        //
        // $buyProduct->charge = $total + $suppProducts->fee;
        // $buyProduct->tagihan = $amount;
        // $buyProduct->admin = $admin;
        // $buyProduct->total = $total;
        // $buyProduct->save();

        $arrResult['denom'] = $amount;
        $arrResult['amount'] = $amount + $adminEpm;

        $buyProduct->charge = $amount + $fee;
        $buyProduct->tagihan = $amount;
        $buyProduct->admin = $adminEpm;
        $buyProduct->total = $amount + $adminEpm;
        $buyProduct->save();

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_GRIYA');
        $sendApi->sendapi_text = $xml_inquiry;
        $sendApi->sendapi_response_text = $get_response;
        $sendApi->sendapi_status = $responseStatus;
        $sendApi->product_id = $buyProduct->product_id;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        if (!$arrResult['denom'] || $arrResult['denom'] == 0) $arrResult['denom'] = $amount;

        // UPDATE MEMBER BUY PRODUCT
        if ($responseStatus != "68") {

            if (!$buyProduct) {
                return [
                    'status' => 1,
                    'data' => $arrResult,
                    'message' => 'Service not available.'
                ];
            }

            $product = Product::where('id', $buyProduct->product_id)->first();

            $buyProduct->sendapi_supplier_id = $sendApi->id;
            $buyProduct->save();

            $statusName = '';
            if ($responseStatus == "00") {

                $content_array = (array)json_decode(static::get_content($xml_array['field'], count($xml_array['field']), '62'));

                foreach ($content_array as $key => $value) {
                    if (gettype($value) == 'string') {
                        $content_array[$key] = ltrim(rtrim($value));

                        if ($key == 'BillStatus') {
                            unset($content_array['BillStatus']);
                        } else if ($key == 'Balance') {
                            unset($content_array['Balance']);
                        } else if (strpos($key, 'Amount') !== false) {
                            unset($content_array[$key]);
                        } else if (strpos($key, 'Admin') !== false) {
                            unset($content_array[$key]);
                        } else if (strpos($key, 'TotalAmount') !== false) {
                            unset($content_array[$key]);
                        } else if (strpos($key, 'Incentive') !== false) {
                            unset($content_array[$key]);
                        } else if (strpos($key, 'Meter') !== false) {
                            unset($content_array[$key]);
                        } else if (strpos($key, 'VAT') !== false) {
                            unset($content_array[$key]);
                        } else if (strpos($key, 'UPPhone') !== false) {
                            unset($content_array[$key]);
                        } else if (strpos($key, 'UPCode') !== false) {
                            unset($content_array[$key]);
                        } else if (strpos($key, 'RefNo') !== false) {
                            unset($content_array[$key]);
                        } else if (strpos($key, 'CustName') !== false) {
                            if (strlen($value) > 0) {
                                $newKey = str_replace('CustName', 'Nama Pelanggan', $key);
                                $content_array[$newKey] = $value;
                            }
                            unset($content_array[$key]);
                        } else if (strpos($key, 'NoOfMember') !== false) {
                            if (strlen($value) > 0) {
                                $newKey = str_replace('NoOfMember', 'Jumlah Anggota', $key);
                                $content_array[$newKey] = $value;
                            }
                            unset($content_array[$key]);
                        } else if (strpos($key, 'Periods') !== false) {
                            if (strlen($value) > 0) {
                                $newKey = str_replace('Periods', 'Jumlah Periode', $key);
                                $content_array[$newKey] = $value;
                            }
                            unset($content_array[$key]);
                        } else if (strpos($key, 'Period') !== false) {
                            if (strlen($value) > 0) {
                                $newKey = str_replace('Period', 'Periode', $key);
                                // $content_array[$newKey] = Carbon::createFromFormat('Ym', $value)->format('M Y');
                                $content_array[$newKey] = $value;
                            }
                            unset($content_array[$key]);
                        } else if (strpos($key, 'Premi') !== false) {
                            if (strlen($value) > 0) {
                                $newKey = str_replace('Premi', 'Iuran', $key);
                                $content_array[$newKey] = 'Rp' . number_format($value, 0, ",", ".");
                            }
                        } else if (strpos($key, 'Penalty') !== false) {
                            if (strlen($value) > 0) {
                                $newKey = str_replace('Penalty', 'Denda', $key);
                                $content_array[$newKey] = 'Rp' . number_format($value, 0, ",", ".");
                            }
                            unset($content_array[$key]);
                        } else if (strpos($key, 'BillCount') !== false) {
                            if (strlen($value) > 0) {
                                $newKey = str_replace('BillCount', 'Jumlah Tagihan', $key);
                                $content_array[$newKey] = number_format($value, 0, ",", ".");
                            }
                            unset($content_array[$key]);
                        } else if (strpos($key, 'DueDate') !== false) {
                            if (strlen($value) > 0) {
                                $newKey = str_replace('DueDate', 'Batas Waktu', $key);
                                $content_array[$newKey] = $value;
                            }
                            unset($content_array[$key]);
                        }

                        if (strlen($value) == 0) {
                            unset($content_array[$key]);
                        }

                        if ($value == 0) {
                            unset($content_array[$key]);
                        }
                    }
                }

                $buyProduct->data = json_encode($content_array);
                $buyProduct->status_buy_ppob = 1;
                $buyProduct->nama_pelanggan = $nama_pelanggan;
                $buyProduct->reff_number = $reff_no;
                $buyProduct->save();

                $statusName = 'Sukses';
                $member = Member::where('id', $buyProduct->user_id)->first();

                if ($buyProduct->payment_type == 1 && $member->user_level_id == 1) {
                    if ($product != null && $product->point > 0) {
                        $bonus = new MemberBonus();
                        $bonus->nominal_bonus = $product->point;
                        $bonus->user_id = $buyProduct->user_id;
                        $bonus->payment_type = 2;
                        $bonus->save();
                    }
                }

                if ($buyProduct->payment_type == 1) {
                    if ($product != null && $product->status_bonus == 1) {
                        Log::channel('bonus')->info('Call function at ' . date('d-m-Y H:i:s'));
                        MemberController::count_transaction_bonus($member->id);
                    }
                }

            } else if ($responseStatus != "00") {

                $buyProduct->status_buy_ppob = 0;
                $buyProduct->save();

                $statusName = 'Gagal';
                $saldo = HelperController::member_saldo($buyProduct->user_id);
                HelperController::history_transaksi($buyProduct->user_id, "Refund Pembelian Gagal", $buyProduct->total, 0, $saldo);
            }

        }
        // END UPDATE MEMBER BUY PRODUCT

        if ($arrResult['status'] == 1) {
            return [
                'status' => 4,
                'data' => $arrResult,
                'message' => 'Service not available.'
            ];
        } else {
            return [
                'status' => 0,
                'data' => $arrResult,
                'message' => $xml_inquiry
            ];
        }
    }

    public static function buy_prepaid($transactionId, $productCode, $idPelanggan, $nominal)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();

        $trxid = (int)substr($transactionId, 11);
        $time = date('mdHis');
        $kode_griya = env("KODE_GRIYA");
        $kode_41_griya = env("KODE_41_GRIYA");
        $kode_42_griya = env("KODE_42_GRIYA");

        $array_inquiry = [
            'field' => [
                ["_attributes" => ["id" => "0", "value" => "0200"]],
                ["_attributes" => ["id" => "3", "value" => "500000"]],
                ["_attributes" => ["id" => "4", "value" => $nominal]],
                ["_attributes" => ["id" => "7", "value" => $time]],
                ["_attributes" => ["id" => "11", "value" => $trxid]],
                ["_attributes" => ["id" => "18", "value" => "7011"]],
                ["_attributes" => ["id" => "32", "value" => $kode_griya]],
                ["_attributes" => ["id" => "41", "value" => $kode_41_griya]],
                ["_attributes" => ["id" => "42", "value" => $kode_42_griya]],
                ["_attributes" => ["id" => "43", "value" => "EPM"]],
                ["_attributes" => ["id" => "61", "value" => $idPelanggan]],
                ["_attributes" => ["id" => "98", "value" => $productCode]]
            ]
        ];
        $xml_inquiry = ArrayToXml::convert($array_inquiry, 'isomsg', false);

        $client = new Client();
        $options = [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Content-length' => '<XML Message Body Length>',
            ],
            'body' => $xml_inquiry,
        ];
        $response = $client->request('POST', env("URL_GRIYA"), $options);

        $get_response = $response->getBody()->getContents();

        $xml = Xml2Array::create($get_response);

        $xml_array = $xml->toArray();

        $responseStatus = static::query_array($xml_array['field'], count($xml_array['field']), '39');
        $internalStatus = ($responseStatus != "00" && $responseStatus != "68") ? 1 : 0;

        $content = json_decode(static::get_content($xml_array['field'], count($xml_array['field']), '62'));

        $nama_pelanggan = '';
        $reff_no = static::query_array($xml_array['field'], count($xml_array['field']), '37');

        if ($content) {
            foreach ($content as $key => $value) {
                if (strpos($key, 'CustName') !== false) {
                    $nama_pelanggan = $value;
                } else if (strpos($key, 'CustName1') !== false) {
                    $nama_pelanggan = $value;
                } else if (strpos($key, 'RefNo') !== false) {
                    $reff_no = $value;
                }
            }
        }

        $buyProduct = MemberBuyProduct::where('transaction_id', $transactionId)->first();

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_GRIYA');
        $sendApi->sendapi_text = $xml_inquiry;
        $sendApi->sendapi_response_text = $get_response;
        $sendApi->sendapi_status = $responseStatus;
        $sendApi->product_id = $buyProduct->product_id;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        $buyProduct->sendapi_supplier_id = $sendApi->id;
        $buyProduct->save();

        $product = Product::where('id', $buyProduct->product_id)->first();

        $statusName = '';
        if ($internalStatus == 1) {
            $buyProduct->status_buy_product = 0;
            $buyProduct->save();

            $statusName = 'Gagal';
        } elseif ($responseStatus == "00") {

            $content_array = (array)json_decode(static::get_content($xml_array['field'], count($xml_array['field']), '62'));

            foreach ($content_array as $key => $value) {
                if (gettype($value) == 'string') {
                    $content_array[$key] = ltrim(rtrim($value));

                    if ($key == 'BillStatus') {
                        unset($content_array['BillStatus']);
                    } else if ($key == 'Balance') {
                        unset($content_array['Balance']);
                    } else if (strpos($key, 'Amount') !== false) {
                        unset($content_array[$key]);
                    } else if (strpos($key, 'Admin') !== false) {
                        unset($content_array[$key]);
                    } else if (strpos($key, 'TotalAmount') !== false) {
                        unset($content_array[$key]);
                    }

                    if (strlen($value) == 0) {
                        unset($content_array[$key]);
                    }

                    if ($value == 0) {
                        unset($content_array[$key]);
                    }
                }
            }

            // $buyProduct->data = json_encode($content_array);
            $buyProduct->status_buy_product = 1;
            $buyProduct->nama_pelanggan = $nama_pelanggan;
            $buyProduct->vsn = $reff_no;
            $buyProduct->save();

            $statusName = 'Sukses';
            $member = Member::where('id', $buyProduct->user_id)->first();

            if ($buyProduct->payment_type == 1 && $member->user_level_id == 1) {
                if ($product != null && $product->point > 0) {
                    $bonus = new MemberBonus();
                    $bonus->nominal_bonus = $product->point;
                    $bonus->user_id = $buyProduct->user_id;
                    $bonus->payment_type = 2;
                    $bonus->save();
                }
            }

            if ($buyProduct->payment_type == 1) {
                if ($product != null && $product->status_bonus == 1) {
                    Log::channel('bonus')->info('Call function at ' . date('d-m-Y H:i:s'));
                    MemberController::count_transaction_bonus($member->id);
                }
            }
        }

        if ($internalStatus == 1) {
            return [
                'status' => 1,
                'message' => 'Service not available.'
            ];
        } else {

            // NOT PENDING, PUSH NOTIFICATION
            if ($responseStatus != "68") {
                $member = Member::where('id', $buyProduct->user_id)->first();
                $phone = HelperController::format_phone($member->phone);
                // fcm()
                //     ->toTopic($phone . '_android')// $topic must an string (topic name)
                //     ->notification([
                //         'title' => 'Info Transaksi',
                //         'body' => 'Transaksi pembelian ' . $product->name . ' ' . $statusName,
                //     ])
                //     ->send();
            }

            return [
                'status' => 0,
                'message' => $xml_inquiry
            ];
        }
    }

    public static function callback_status(Request $request)
    {
        if (!$request->get('0')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `0` is required.'
            ], 400);
        }

        if (!$request->get('3')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `3` is required.'
            ], 400);
        }

        if (!$request->get('4')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `4` is required.'
            ], 400);
        }

        if (!$request->get('7')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `7` is required.'
            ], 400);
        }

        if (!$request->get('11')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `11` is required.'
            ], 400);
        }

        if (!$request->get('15')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `15` is required.'
            ], 400);
        }

        if (!$request->get('18')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `18` is required.'
            ], 400);
        }

        if (!$request->get('32')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `32` is required.'
            ], 400);
        }

        if (!$request->get('37')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `37` is required.'
            ], 400);
        }

        if (!$request->get('39')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `39` is required.'
            ], 400);
        }

        if (!$request->get('41')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `41` is required.'
            ], 400);
        }

        if (!$request->get('42')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `42` is required.'
            ], 400);
        }

        if (!$request->get('43')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `43` is required.'
            ], 400);
        }

        if (!$request->get('61')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `61` is required.'
            ], 400);
        }

        if (!$request->get('98')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `98` is required.'
            ], 400);
        }

        $array_inquiry = [
            'field' => [
                ["_attributes" => ["id" => "0", "value" => $request->get('0')]],
                ["_attributes" => ["id" => "3", "value" => $request->get('3')]],
                ["_attributes" => ["id" => "4", "value" => $request->get('4')]],
                ["_attributes" => ["id" => "7", "value" => $request->get('7')]],
                ["_attributes" => ["id" => "11", "value" => $request->get('11')]],
                ["_attributes" => ["id" => "18", "value" => $request->get('18')]],
                ["_attributes" => ["id" => "32", "value" => $request->get('32')]],
                ["_attributes" => ["id" => "37", "value" => $request->get('37')]],
                ["_attributes" => ["id" => "39", "value" => $request->get('39')]],
                ["_attributes" => ["id" => "41", "value" => $request->get('41')]],
                ["_attributes" => ["id" => "42", "value" => $request->get('42')]],
                ["_attributes" => ["id" => "43", "value" => $request->get('43')]],
                ["_attributes" => ["id" => "61", "value" => $request->get('61')]],
                ["_attributes" => ["id" => "62", "value" => $request->get('62')]],
                ["_attributes" => ["id" => "98", "value" => $request->get('98')]]
            ]
        ];
        $arrResult = ArrayToXml::convert($array_inquiry, 'isomsg', false);

        $xml = Xml2Array::create($arrResult);

        $xml_array = $xml->toArray();

        $content = json_decode(static::$request->get('62'), count($xml_array['field']), '62');
        $content = (array)$content;

        $content = json_decode(static::get_content($xml_array['field'], count($xml_array['field']), '62'));

        $nama_pelanggan = '';
        $reff_no = '';

        foreach ($content as $key => $value) {
            if (strpos($key, 'CustName') !== false) {
                $nama_pelanggan = $value;
            } else if (strpos($key, 'CustName1') !== false) {
                $nama_pelanggan = $value;
            } else if (strpos($key, 'RefNo') !== false) {
                $reff_no = $value;
            }
        }

        $content_array = (array)json_decode(static::get_content($xml_array['field'], count($xml_array['field']), '62'));

        foreach ($content_array as $key => $value) {
            if (gettype($value) == 'string') {
                $content_array[$key] = ltrim(rtrim($value));

                if ($key == 'BillStatus') {
                    unset($content_array['BillStatus']);
                } else if ($key == 'Balance') {
                    unset($content_array['Balance']);
                } else if (strpos($key, 'Amount') !== false) {
                    unset($content_array[$key]);
                } else if (strpos($key, 'Admin') !== false) {
                    unset($content_array[$key]);
                } else if (strpos($key, 'TotalAmount') !== false) {
                    unset($content_array[$key]);
                } else if (strpos($key, 'Incentive') !== false) {
                    unset($content_array[$key]);
                } else if (strpos($key, 'Meter') !== false) {
                    unset($content_array[$key]);
                } else if (strpos($key, 'VAT') !== false) {
                    unset($content_array[$key]);
                } else if (strpos($key, 'UPPhone') !== false) {
                    unset($content_array[$key]);
                } else if (strpos($key, 'UPCode') !== false) {
                    unset($content_array[$key]);
                } else if (strpos($key, 'RefNo') !== false) {
                    unset($content_array[$key]);
                } else if (strpos($key, 'CustName') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('CustName', 'Nama Pelanggan', $key);
                        $content_array[$newKey] = $value;
                    }
                    unset($content_array[$key]);
                } else if (strpos($key, 'NoOfMember') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('NoOfMember', 'Jumlah Anggota', $key);
                        $content_array[$newKey] = $value;
                    }
                    unset($content_array[$key]);
                } else if (strpos($key, 'Periods') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('Periods', 'Jumlah Periode', $key);
                        $content_array[$newKey] = $value;
                    }
                    unset($content_array[$key]);
                } else if (strpos($key, 'Period') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('Period', 'Periode', $key);
                        // $content_array[$newKey] = Carbon::createFromFormat('Ym', $value)->format('M Y');
                        $content_array[$newKey] = $value;
                    }
                    unset($content_array[$key]);
                } else if (strpos($key, 'Premi') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('Premi', 'Iuran', $key);
                        $content_array[$newKey] = 'Rp' . number_format($value, 0, ",", ".");
                    }
                } else if (strpos($key, 'Penalty') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('Penalty', 'Denda', $key);
                        $content_array[$newKey] = 'Rp' . number_format($value, 0, ",", ".");
                    }
                    unset($content_array[$key]);
                } else if (strpos($key, 'BillCount') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('BillCount', 'Jumlah Tagihan', $key);
                        $content_array[$newKey] = number_format($value, 0, ",", ".");
                    }
                    unset($content_array[$key]);
                } else if (strpos($key, 'DueDate') !== false) {
                    if (strlen($value) > 0) {
                        $newKey = str_replace('DueDate', 'Batas Waktu', $key);
                        $content_array[$newKey] = $value;
                    }
                    unset($content_array[$key]);
                }

                if (strlen($value) == 0) {
                    unset($content_array[$key]);
                }

                if ($value == 0) {
                    unset($content_array[$key]);
                }
            }
        }

        // INSERT RESULT SUPPLIER
        $resultSupplier = new ResultSupplier();
        $resultSupplier->result_text = json_encode($arrResult);
        $resultSupplier->result_response_text = json_encode($arrResult);
        $resultSupplier->result_status = $request->get('39');
        $resultSupplier->save();
        // END INSERT RESULT SUPPLIER

        // EPM Status Code: 0: Gagal, 1: Sukses, 2: Pending

        $status = 1;
        if ($request->get('39') == '00') {
            $status = 1;
        } else {
            $status = 0;
        }

        if ($request->get('4') == 500000) {
            $buyProduct = MemberBuyProduct::where('transaction_id', 'like', '%' . $request->get('11'))->first();
            if (!$buyProduct) {
                return response()->json([
                    'code' => 404,
                    'message' => 'Transaction not found..'
                ], 404);
            }

            $product = Product::where('id', $buyProduct->product_id)->first();

            // $buyProduct->data = json_encode($content_array);
            $buyProduct->result_supplier_id = $resultSupplier->id;
            $buyProduct->status_buy_product = $status;
            $buyProduct->vsn = $reff_no;
            $buyProduct->save();

            $statusName = '';
            if ($request->get('39') == '00') {
                $statusName = 'Sukses';
                $member = Member::where('id', $buyProduct->user_id)->first();

                if ($buyProduct->payment_type == 1 && $member->user_level_id == 1) {
                    if ($product != null && $product->point > 0) {
                        $bonus = new MemberBonus();
                        $bonus->nominal_bonus = $product->point;
                        $bonus->user_id = $buyProduct->user_id;
                        $bonus->payment_type = 2;
                        $bonus->save();
                    }
                }

                if ($buyProduct->payment_type == 1) {
                    if ($product != null && $product->status_bonus == 1) {
                        Log::channel('bonus')->info('Call function at ' . date('d-m-Y H:i:s'));
                        MemberController::count_transaction_bonus($member->id);
                    }
                }
            }

            if ($status == 0) {
                $statusName = 'Gagal';
                $saldo = HelperController::member_saldo($buyProduct->user_id);
                HelperController::history_transaksi($buyProduct->user_id, "Refund Pembelian Gagal", $buyProduct->buy_product_price, 0, $saldo);
            }

            $member = Member::where('id', $buyProduct->user_id)->first();
            $phone = HelperController::format_phone($member->phone);

            fcm()
                ->toTopic($phone . '_android')// $topic must an string (topic name)
                ->notification([
                    'title' => 'Info Transaksi',
                    // 'body' => 'Transaksi pembelian ' . $product->name . ' ' . $statusName,
                    'body' => 'Transaksi pembelian ' . $product->name . ' telah selesai, lihat history untuk mengetahui status transaksi',
                ])
                ->send();
        } else if ($request->get('4') == 580000) {
            $buyProduct = MemberBuyPpob::where('ppob_id', 'like', '%' . $request->get('11'))->first();
            if ($buyProduct) {
                return response()->json([
                    'code' => 404,
                    'message' => 'Transaction not found..'
                ], 404);
            }

            $product = Product::where('id', $buyProduct->product_id)->first();

            // $buyProduct->data = json_encode($content_array);
            $buyProduct->result_supplier_id = $resultSupplier->id;
            $buyProduct->status_buy_ppob = $status;
            $buyProduct->nama_pelanggan = $nama_pelanggan;
            $buyProduct->reff_number = $reff_no;
            $buyProduct->save();

            $statusName = '';
            if ($request->get('39') == '00') {
                $statusName = 'Sukses';
                $member = Member::where('id', $buyProduct->user_id)->first();

                if ($buyProduct->payment_type == 1 && $member->user_level_id == 1) {
                    if ($product != null && $product->point > 0) {
                        $bonus = new MemberBonus();
                        $bonus->nominal_bonus = $product->point;
                        $bonus->user_id = $buyProduct->user_id;
                        $bonus->payment_type = 2;
                        $bonus->save();
                    }
                }

                if ($buyProduct->payment_type == 1) {
                    if ($product != null && $product->status_bonus == 1) {
                        Log::channel('bonus')->info('Call function at ' . date('d-m-Y H:i:s'));
                        MemberController::count_transaction_bonus($member->id);
                    }
                }

            }

            if ($status == 0) {
                $statusName = 'Gagal';
                $saldo = HelperController::member_saldo($buyProduct->user_id);
                HelperController::history_transaksi($buyProduct->user_id, "Refund Pembelian Gagal", $buyProduct->total, 0, $saldo);
            }

            $member = Member::where('id', $buyProduct->user_id)->first();
            $phone = HelperController::format_phone($member->phone);

            fcm()
                ->toTopic($phone . '_android')// $topic must an string (topic name)
                ->notification([
                    'title' => 'Info Transaksi',
                    // 'body' => 'Transaksi pembelian ' . $product->name . ' ' . $statusName,
                    'body' => 'Transaksi pembelian ' . $product->name . ' telah selesai, lihat history untuk mengetahui status transaksi',
                ])
                ->send();
        }

        return response()->json([
            'code' => 200,
            'message' => 'Transaction updated successfully'
        ], 200);
    }
}
