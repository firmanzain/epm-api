<?php

namespace App\Http\Controllers\API\v2\vendor;

use App\Http\Controllers\API\v2\HelperController;
use App\Http\Controllers\API\v2\MemberController;
use App\Http\Controllers\Controller;
use App\Models\v2\Supplier;
use Illuminate\Http\Request;
use Spatie\ArrayToXml\ArrayToXml;
use GuzzleHttp\Client;
use
    App\Models\v2\SendapiSupplier, App\Models\v2\ResultSupplier, App\Models\v2\MemberBuyProduct,
    App\Models\v2\Member, App\Models\v2\Product, App\Models\v2\MemberBuyPpob,
    App\Models\v2\MemberBonus, App\Models\v2\SupplierProduct;
use Illuminate\Support\Facades\Log;


class DutaPulsaController implements VendorController
{
    private static function generate_sign($msisdn, $time, $pass)
    {
        $msisdn4 = substr($msisdn, -4);
        $a = $time . $msisdn4;
        $b = substr($msisdn4, -1) . substr($msisdn4, -2, 1) . substr($msisdn4, -3, 1) . substr($msisdn4, -4, 1) . $pass;

        return base64_encode($a ^ $b);
    }

    private static function array_ppob($array)
    {
        $results = array();
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i] != "") {
                if (strpos($array[$i], '=>') !== false) {
                    $data = explode('=>', $array[$i]);
                } else {
                    $data = explode(':', $array[$i]);
                }
                if (isset($data[1])) {
                  $results[$data[0]] = $data[1];
                }
            }

        }

        return $results;
    }

    private static function inquiry($productId, $productCode, $idPelanggan)
    {
        $userh2h = env("USER_H2H_DUTA_PULSA");
        $passh2h = env("PASS_H2H_DUTA_PULSA");
        $trxid = 'PPOB' . date('Y') . date('m') . date('d') . date("H") . date("i") . date("s");
        $time = date('Y-m-d H:m:s');
        $sign = md5($userh2h . $passh2h . $time);

        $array_inquiry_ppob = [
            'methodName' => 'PPOB.Inquiry',
            'params' => ['param' => ['value' => ['struct' => [
                'member' => [
                    ['name' => 'user', 'value' => ['string' => $userh2h]],
                    ['name' => 'waktu', 'value' => ['string' => $time]],
                    ['name' => 'produk', 'value' => ['string' => $productCode]],
                    ['name' => 'idpel', 'value' => ['string' => $idPelanggan]],
                    ['name' => 'reffid', 'value' => ['string' => $trxid]],
                    ['name' => 'signature', 'value' => ['string' => $sign]]
                ]
            ]]]]
        ];

        $xml_inquiry = ArrayToXml::convert($array_inquiry_ppob, 'methodCall', false);

        $client = new Client();
        $options = [
            'headers' => [
                'Content-Type' => 'application/xml',
            ],
            'body' => $xml_inquiry,
        ];
        $response = $client->request('POST', env("URL_DUTA_PULSA_PPOB"), $options);

        $get_response = $response->getBody()->getContents();

        $xml_string = simplexml_load_string($get_response);

        $data_response = $xml_string->params->param->value->struct->member;

        $status = null;
        $array_data = null;
        $arrResult['trx_id'] = $trxid;
        foreach ($data_response as $dr) {
            if ($dr->name == 'code') $arrResult['status'] = $dr->value->string;
            if ($dr->name == 'message') $arrResult['content'] = $dr->value->string;
        }

        $arrResult['req'] = $xml_inquiry;
        $arrResult['res'] = $get_response;
        $arrResult['sts'] = $arrResult['status'];

        $arrResult['amount'] = 0;

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_DUTA_PULSA_PPOB');
        $sendApi->sendapi_text = $xml_inquiry;
        $sendApi->sendapi_response_text = $get_response;
        $sendApi->sendapi_status = $arrResult['status'];
        $sendApi->product_id = $productId;
        $sendApi->supplier_id = 1;
        $sendApi->save();

        if ($arrResult['status'] == '00') {

            if (strpos($arrResult['content'], ';') !== false) {
                $array_data = explode('|', $arrResult['content']);
                $array_data = explode(';', $array_data[0]);
            } else {
                $array_data = explode('|', $arrResult['content']);
            }
            $arrResult['content'] = DutaPulsaController::array_ppob($array_data);

            // $array_data = explode('|', $arrResult['content']);
            // $arrResult['content'] = DutaPulsaController::array_ppob($array_data);
            $arrResult['amount'] = $arrResult['content']['charge'];
        } else if ($arrResult['status'] == '99') {
            $arrResult['status'] = '999';
            $array_error = explode(' ', $arrResult['content']);
            $error_code = $array_error[0];

            if (strpos($error_code, '14')) {
                $arrResult['status'] = 14;
            } elseif (strpos($error_code, '30') || strpos($error_code, '92')) {
                $arrResult['status'] = 30;
            } elseif (strpos($error_code, '99')) {
                $arrResult['status'] = 99;
            } elseif (strpos($error_code, '68')) {
                $arrResult['status'] = 667;
            }
        }

        return $arrResult;
    }

    public static function inquiry_postpaid($productId, $productCode, $idPelanggan)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();
        $inquiry = self::inquiry($productId, $productCode, $idPelanggan);

        $result['trx_id'] = $inquiry['trx_id'];

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_DUTA_PULSA_PPOB');
        $sendApi->sendapi_text = $inquiry['req'];
        $sendApi->sendapi_response_text = $inquiry['res'];
        $sendApi->sendapi_status = $inquiry['sts'];
        $sendApi->product_id = $productId;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        // GET ADMIN PRODUCT
        $suppProducts = SupplierProduct::where('product_id', $productId)
                                       ->where('supplier_id', $supplier->id)
                                       ->first();

        $content = $inquiry['content'];
        $result['vendor'] = "duta";

        if ($inquiry['status'] == '00') {

            $fee = intval($suppProducts->fee);
            $adminEpm = $suppProducts->biaya_admin;
            $admin = 0;
            $amount = 0;

            $data = array();
            $dataDebug = array();
            foreach ($content as $key => $value) {
                if (strpos($key, 'nmpel') !== false) {
                    $newKey = str_replace('nmpel', 'Nama Pelanggan', $key);
                    $data[$newKey] = $value;
                } elseif (strpos($key, 'periode') !== false) {
                    $newKey = str_replace('periode', 'Periode', $key);
                    $data[$newKey] = $value;
                } elseif (strpos($key, 'tarifdaya') !== false) {
                    $newKey = str_replace('tarifdaya', 'Tarif/Daya', $key);
                    $data[$newKey] = $value;
                } elseif (strpos($key, 'charge') !== false) {
                } elseif (strpos($key, 'tagihan') !== false) {
                    $newKey = str_replace('tagihan', 'Tagihan', $key);
                    $data[$newKey] = 'Rp'.number_format($value, 0, ",", ".");
                    $amount += intval(str_replace(".", "", $value));
                } elseif (strpos($key, 'minpayment') !== false) {
                    $newKey = str_replace('minpayment', 'Tagihan', $key);
                    $data[$newKey] = 'Rp'.number_format($value, 0, ",", ".");
                    $amount += intval(str_replace(".", "", $value));
                } elseif (strpos($key, 'adm') !== false) {
                    $admin += $value;
                }
                $dataDebug[$key] = $value;
            }
            // $data['Admin'] = 'Rp'.number_format($admin + $adminEpm, 0, ",", ".");
            $data['Admin'] = 'Rp'.number_format($adminEpm, 0, ",", ".");

            if (isset($data['Tagihan'])) {
                $amount -= $admin;
                $data['Tagihan'] = 'Rp'.number_format($amount, 0, ",", ".");
            }

            // $result['admin'] = $admin + $adminEpm;
            // $result['amount_vendor'] = $amount + $admin + $fee;
            // $result['amount'] = $amount + $admin + $adminEpm;
            $result['admin'] = $adminEpm;
            $result['amount_vendor'] = $amount + $fee;
            $result['amount'] = $amount + $adminEpm;

            $result['data'] = $data;
            $result['status'] = 0;
            $result['data_debug'] = $dataDebug;
        } else if ($inquiry['status'] == 14) {
            $result['admin'] = 0;
            $result['amount_vendor'] = 0;
            $result['amount'] = 0;
            $result['data'] = $inquiry;
            $result['status'] = 1;
            $result['sendApiId'] = $sendApi->id;
        } else if ($inquiry['status'] == 99) {
            $result['admin'] = 0;
            $result['amount_vendor'] = 0;
            $result['amount'] = 0;
            $result['data'] = $inquiry;
            $result['status'] = 2;
            $result['sendApiId'] = $sendApi->id;
        } else {
            $result['admin'] = 0;
            $result['amount_vendor'] = 0;
            $result['amount'] = 0;
            $result['data'] = NULL;
            $result['status'] = 99;
            $result['sendApiId'] = $sendApi->id;
        }

        return $result;
    }

    public static function pay_postpaid($transactionId, $productCode, $idPelanggan, $trxid, $amount)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();
        // $inquiry = self::inquiry($productCode, $idPelanggan);

        // $content = $inquiry['content'];

        $buyProduct = MemberBuyPpob::where('ppob_id', $transactionId)->first();

        // GET ADMIN PRODUCT
        $suppProducts = SupplierProduct::where('product_id', $buyProduct->product_id)
                                       ->where('supplier_id', $supplier->id)
                                       ->first();

        $userh2h = env("USER_H2H_DUTA_PULSA");
        $passh2h = env("PASS_H2H_DUTA_PULSA");
        // $trxid = $transactionId;
        $time = date('Y-m-d H:m:s');
        $sign = md5($userh2h . $passh2h . $time);

        $array_inquiry_ppob = [
            'methodName' => 'PPOB.Payment',
            'params' => ['param' => ['value' => ['struct' => [
                'member' => [
                    ['name' => 'user', 'value' => ['string' => $userh2h]],
                    ['name' => 'waktu', 'value' => ['string' => $time]],
                    ['name' => 'produk', 'value' => ['string' => $productCode]],
                    ['name' => 'idpel', 'value' => ['string' => $idPelanggan]],
                    ['name' => 'bayar', 'value' => ['string' => $amount]],
                    ['name' => 'reffid', 'value' => ['string' => $trxid]],
                    ['name' => 'signature', 'value' => ['string' => $sign]]
                ]
            ]]]]
        ];

        $xml_inquiry = ArrayToXml::convert($array_inquiry_ppob, 'methodCall', false);

        $client = new Client();
        $options = [
            'headers' => [
                'Content-Type' => 'application/xml',
            ],
            'body' => $xml_inquiry,
        ];
        $response = $client->request('POST', env("URL_DUTA_PULSA_PPOB"), $options);

        $get_response = $response->getBody()->getContents();

        $xml_string = simplexml_load_string($get_response);

        $data_response = $xml_string->params->param->value->struct->member;

        $status = null;
        $array_data = null;
        $arrResult['trx_id'] = $trxid;
        foreach ($data_response as $dr) {
            if ($dr->name == 'code') $arrResult['status'] = $dr->value->string;
            if ($dr->name == 'message') $arrResult['content'] = $dr->value->string;
        }

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env('URL_DUTA_PULSA_PPOB');
        $sendApi->sendapi_text = $xml_inquiry;
        $sendApi->sendapi_response_text = $get_response;
        $sendApi->sendapi_status = $arrResult['status'];
        $sendApi->product_id = $buyProduct->product_id;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        $arrResult['amount'] = 0;

        $fee = intval($suppProducts->fee);
        $adminEpm = $suppProducts->biaya_admin;
        $admin = 0;
        $amount = 0;

        if ($arrResult['status'] == '00') {

            if (strpos($arrResult['content'], ';') !== false) {
                $array_data = explode('|', $arrResult['content']);
                $array_data = explode(';', $array_data[0]);
            } else {
                $array_data = explode('|', $arrResult['content']);
            }
            $arrResult['content'] = DutaPulsaController::array_ppob($array_data);

            foreach ($arrResult['content'] as $key => $value) {
                if (strpos($key, 'charge') !== false) {
                } elseif (strpos($key, 'tagihan') !== false) {
                    $amount += intval(str_replace(".", "", $value));
                } elseif (strpos($key, 'adm') !== false) {
                    $admin += $value;
                }
            }

            if (isset($data['Tagihan'])) {
                $amount -= $admin;
            }

        } else if ($arrResult['status'] == '99') {
            $arrResult['status'] = '999';
            $array_error = explode(' ', $arrResult['content']);
            $error_code = $array_error[0];

            if (strpos($error_code, '14')) {
                $arrResult['status'] = 14;
            } elseif (strpos($error_code, '30') || strpos($error_code, '92')) {
                $arrResult['status'] = 30;
            } elseif (strpos($error_code, '99')) {
                $arrResult['status'] = 99;
            } elseif (strpos($error_code, '68')) {
                $arrResult['status'] = 667;
            }
        }

        // $arrResult['denom'] = $amount + $admin + $fee;
        // $arrResult['amount'] = $amount + $admin + $adminEpm;
        //
        // $buyProduct->charge = $amount + $admin + $fee;
        // $buyProduct->tagihan = $amount;
        // $buyProduct->admin = $admin + $adminEpm;
        // $buyProduct->total = $amount + $admin + $adminEpm;
        // $buyProduct->save();

        $arrResult['denom'] = $amount;
        $arrResult['amount'] = $amount + $adminEpm;

        $buyProduct->charge = $amount + $admin;
        $buyProduct->tagihan = $amount - $fee;
        $buyProduct->admin = $adminEpm;
        $buyProduct->total = ($amount - $fee) + $adminEpm;
        $buyProduct->save();

        if ($arrResult['status'] == '00') {
            // SUCCESS
            $content_string = $xml_string->params->param->value->struct->member[1]->value->string;

            $content_data = explode('|', $content_string);
            $content_array = DutaPulsaController::array_ppob($content_data);

            foreach ($content_array as $key => $value) {
                $content_array[$key] = ltrim(rtrim($value));

                if ($key == 'tagihan') {
                    unset($content_array['tagihan']);
                } else if ($key == 'admin') {
                    unset($content_array['admin']);
                } else if ($key == 'admbank') {
                    unset($content_array['admbank']);
                } else if ($key == 'charge') {
                    unset($content_array['charge']);
                } else if ($key == 'saldo') {
                    unset($content_array['saldo']);
                } else if (strpos($key, 'nmpel') !== false) {
                    $newKey = str_replace('nmpel', 'Nama Pelanggan', $key);
                    $content_array[$newKey] = $value;
                } else if (strpos($key, 'periode') !== false) {
                    $newKey = str_replace('periode', 'Periode', $key);
                    $content_array[$newKey] = $value;
                } else if (strpos($key, 'tarifdaya') !== false) {
                    $newKey = str_replace('tarifdaya', 'Tarif/Daya', $key);
                    $content_array[$newKey] = $value;
                }
            }

            // $buyProduct->data = json_encode($content_array);
            $buyProduct->sendapi_supplier_id = $sendApi->id;
            $buyProduct->reff_number = (isset($arrResult['content']['reffno'])) ? $arrResult['content']['reffno'] : '';
            $buyProduct->nama_pelanggan = ($arrResult['content']['nmpel']) ? $arrResult['content']['nmpel'] : '';
            $buyProduct->status_buy_ppob = 1;
            $buyProduct->save();

            $product = Product::where('id', $buyProduct->product_id)->first();
            $member = Member::where('id', $buyProduct->user_id)->first();

            if ($buyProduct->payment_type == 1 && $member->user_level_id == 1) {
                if ($product != null && $product->point > 0) {
                    $bonus = new MemberBonus();
                    $bonus->nominal_bonus = $product->point;
                    $bonus->user_id = $buyProduct->user_id;
                    $bonus->payment_type = 2;
                    $bonus->save();
                }
            }

            if ($buyProduct->payment_type == 1) {
                if ($product != null && $product->status_bonus == 1) {
                    Log::channel('bonus')->info('Call function at '.date('d-m-Y H:i:s'));
                    MemberController::count_transaction_bonus($member->id);
                }
            }

            return [
                'status' => 0,
                'data' => $arrResult,
                'message' => $xml_inquiry
            ];
        } else {
            // GAGAL
            $buyProduct->sendapi_supplier_id = $sendApi->id;
            $buyProduct->status_buy_ppob = 0;
            $buyProduct->save();

            $product = Product::where('id', $buyProduct->product_id)->first();

            $saldo = HelperController::member_saldo($buyProduct->user_id);
            HelperController::history_transaksi($buyProduct->user_id, "Refund Pembelian Gagal", $buyProduct->total, 0, $saldo);

            return [
                'status' => 4,
                'data' => $arrResult,
                'message' => 'Service not available.'
            ];
        }
    }

    public static function buy_prepaid($transactionId, $productCode, $idPelanggan, $nominal)
    {
        $className = get_called_class();
        $supplier = Supplier::where('controller_name', $className)->first();

        $userh2h = env("USER_H2H_DUTA_PULSA");
        $passh2h = env("PASS_H2H_DUTA_PULSA");
        $time = date('His');
        $sign = DutaPulsaController::generate_sign($idPelanggan, $time, $passh2h);

        $array_inquiry = [
            'command' => 'TOPUP',
            'vtype' => $productCode,
            'userid' => $userh2h,
            'time' => $time,
            'msisdn' => $idPelanggan,
            'trxid' => $transactionId,
            'sign' => $sign,
        ];

        $xml_inquiry = ArrayToXml::convert($array_inquiry, 'pulsamatic', false);

        $client = new Client();
        $options = [
            'headers' => [
                'Content-Type' => 'application/xml',
            ],
            'body' => $xml_inquiry,
        ];
        $response = $client->request('POST', env("URL_DUTA_PULSA"), $options);

        $get_response = $response->getBody()->getContents();

        // INSERT SEND API SUPPLIER
        $sendApi = new SendapiSupplier();
        $sendApi->sendapi_url = env("URL_DUTA_PULSA");
        $sendApi->sendapi_text = $xml_inquiry;
        $sendApi->sendapi_response_text = $get_response;
        $sendApi->supplier_id = $supplier->id;
        $sendApi->save();

        $xml_string = simplexml_load_string($get_response);

        if ($xml_string === false) {
        }

        $buyProduct = MemberBuyProduct::where('transaction_id', $transactionId)->first();

        // INSERT SEND API SUPPLIER
        $sendApi->sendapi_status = ($xml_string != false && $xml_string->result == 'failed') ? 1 : 0;
        $sendApi->product_id = $buyProduct->product_id;
        $sendApi->save();

        $buyProduct->sendapi_supplier_id = $sendApi->id;
        $buyProduct->save();

        if ($xml_string != false && $xml_string->result == 'failed') {

            $buyProduct->status_buy_product = 0;
            $buyProduct->save();

            $saldo = HelperController::member_saldo($buyProduct->user_id);
            HelperController::history_transaksi($buyProduct->user_id, "Refund Pembelian Gagal", $buyProduct->buy_product_price, 0, $saldo);

            $member = Member::where('id', $buyProduct->user_id)->first();
            $phone = HelperController::format_phone($member->phone);

            $product = Product::where('id', $buyProduct->product_id)->first();

            return [
                'status' => 1,
                'message' => 'Service not available.'
            ];
        } else {
            return [
                'status' => 0,
                'message' => $xml_inquiry
            ];
        }
    }

    public static function callback_status(Request $request)
    {
        Log::channel('callbackduta')->info('Callback '.date('Y-m-d H:i:s').' '.$request->fullUrl());

        $arrResult = [
            'pid' => $request->get('pid'),
            'code' => $request->get('code'),
            'produk' => $request->get('produk'),
            'sn' => $request->get('sn'),
            'msisdn' => $request->get('msisdn'),
            'msg' => $request->get('msg'),
            'data' => $request->get('data'),
        ];

        // INSERT RESULT SUPPLIER
        $resultSupplier = new ResultSupplier();
        $resultSupplier->result_text = json_encode($arrResult);
        $resultSupplier->result_response_text = $request->fullUrl();
        $resultSupplier->result_status = ($request->get('code')) ? $request->get('code') : -1;
        $resultSupplier->supplier_id = 1;
        $resultSupplier->save();
        // END INSERT RESULT SUPPLIER

        // Duta Status Code: 2: Gagal, 3: Refund, 4: Berhasil
        // EPM Status Code: 0: Gagal, 1: Sukses, 2: Pending

        if (!$request->get('code')) {
            return response()->json([
                'code' => 400,
                'message' => 'Code cannot be null.'
            ], 400);
        }

        $status = 1;
        $statusName = '';
        if ($request->get('code') == 4) {
            $statusName = 'Sukses';
            $status = 1;
        } else if ($request->get('code') == 2) {
            $statusName = 'Gagal';
            $status = 0;
        }

        $buyProduct = MemberBuyProduct::where('transaction_id', $request->get('pid'))->first();
        if (!$buyProduct) {
            return response()->json([
                'code' => 404,
                'message' => 'Transaction not found.'
            ], 200);
        }

        $product = Product::where('id', $buyProduct->product_id)->first();
        if (!$product) {

            $buyProduct->result_supplier_id = $resultSupplier->id;
            $buyProduct->status_buy_product = 0;
            $buyProduct->save();

            return response()->json([
                'code' => 404,
                'message' => 'Product not found.'
            ], 200);
        }

        $content_array = array(
            'sn' => $request->get('sn'),
        );

        $buyProduct->result_supplier_id = $resultSupplier->id;
        $buyProduct->status_buy_product = $status;
        $buyProduct->vsn = $request->get('sn');
        // $buyProduct->data = json_encode($content_array);
        $buyProduct->save();

        $member = Member::where('id', $buyProduct->user_id)->first();
        if (!$member) {

            $buyProduct->result_supplier_id = $resultSupplier->id;
            $buyProduct->status_buy_product = 0;
            $buyProduct->save();

            return response()->json([
                'code' => 404,
                'message' => 'Member not found.'
            ], 200);
        }

        if ($buyProduct->payment_type == 1 && $member->user_level_id == 1) {
            if ($product != null && $product->point > 0) {
                $bonus = new MemberBonus();
                $bonus->nominal_bonus = $product->point;
                $bonus->user_id = $buyProduct->user_id;
                $bonus->payment_type = 2;
                $bonus->save();
            }
        }

        if ($status == 0) {
            $saldo = HelperController::member_saldo($buyProduct->user_id);
            HelperController::history_transaksi($buyProduct->user_id, "Refund Pembelian Gagal", $buyProduct->buy_product_price, 0, $saldo);
        } else {
            if ($buyProduct->payment_type == 1) {
                if ($product != null && $product->status_bonus == 1) {
                    Log::channel('bonus')->info('Call function at '.date('d-m-Y H:i:s'));
                    MemberController::count_transaction_bonus($member->id);
                }
            }
        }

        $phone = HelperController::format_phone($member->phone);

        fcm()
            ->toTopic($phone.'_android') // $topic must an string (topic name)
            ->notification([
                'title' => 'Info Transaksi',
                // 'body' => 'Transaksi pembelian '.$product->name.' '.$statusName,
                'body' => 'Transaksi pembelian '.$product->name.' telah selesai, lihat history untuk mengetahui status transaksi',
            ])
            ->send();

        return response()->json([
            'code' => 200,
            'message' => 'Transaction updated successfully'
        ], 200);
    }
}
