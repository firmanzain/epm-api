<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTFactory, JWTAuth, DB, Carbon\Carbon, PDF;
use
    App\Models\v2\TopupMember, App\Models\v2\MemberWithdraw, App\Models\v2\TransferSaldo,
    App\Models\v2\MemberBonus, App\Models\v2\Deposit, App\Models\v2\TransactionBank,
    App\Models\v2\UserBankAccount, App\Models\v2\MemberBuyLevel, App\Models\v2\MemberBuyProduct,
    App\Models\v2\MemberBuyPpob, App\Models\v2\Product, App\Models\v2\ProductCategories,
    App\Models\v2\Member;
use
    App\Transformers\Serializer\EpmArraySerializer, App\Transformers\TopupMemberTransformer,
    App\Transformers\TransactionTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_transaction_history(Request $request)
    {
        $member = JWTAuth::toUser($request->token);
        if (!$member) {
            return response()->json([
                'code' => 404,
                'message' => 'User not found.'
            ], 404);
        }

        $buyProduct = MemberBuyProduct::select(
            'id',
            'transaction_id',
            'msisdn as cust_id',
            'vsn as sn',
            'nama_pelanggan as data',
            'buy_product_price as amount',
            DB::raw('0 as fee'),
            DB::raw('8 as category'),
            DB::raw('"Kredit" as type'),
            'status_buy_product as status',
            DB::raw('0 as bank_id'),
            'msisdn as reference_id',
            'product_id as origin_id',
            'payment_type as method',
            'receiptUrl as receiptUrl',
            'created_at',
            'data as detail'
        )
        ->where('user_id', $member->id);

        $paginator = MemberBuyPpob::select(
            'id',
            'ppob_id as transaction_id',
            'msisdn as cust_id',
            'reff_number as sn',
            'nama_pelanggan as data',
            'tagihan as amount',
            'admin as fee',
            DB::raw('9 as category'),
            DB::raw('"Kredit" as type'),
            'status_buy_ppob as status',
            DB::raw('0 as bank_id'),
            'msisdn as reference_id',
            'product_id as origin_id',
            'payment_type as method',
            'receiptUrl as receiptUrl',
            'created_at',
            'data as detail'
        )
        ->where('user_id', $member->id)
            ->unionAll($buyProduct)
            ->orderBy('created_at', 'desc')
            ->paginate(15);

        $buyPpob = $paginator->getCollection();

        return fractal()
            ->collection($buyPpob, new TransactionTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->parseIncludes('product')
            ->toArray();
    }

    public function get_transaction_prepaid_print(Request $request, $id)
    {
        $member = JWTAuth::toUser($request->token);
        // $member = Member::where('id', 2636)->first();

        $data = MemberBuyProduct::with('product')->where('id', $id)->first();
        if (!$data) {
            return response()->json([
                'code' => 404,
                'message' => 'Transaction not found.'
            ], 404);
        }

        if ($data->user_id != $member->id) {
            return response()->json([
                'code' => 404,
                'message' => 'Transaction not found.'
            ], 404);
        }

        $caption = 'ID Pelanggan';
        $product = Product::where('id', $data->product->id)->first();
        if ($product) {
            if ($product->caption) {
                $caption = $product->caption;
            } else {
                if ($product->category_id) {
                    $productCategory = ProductCategories::where('id', $product->category_id)->first();
                    if ($productCategory) {
                        if ($productCategory->caption) {
                            $caption = $productCategory->caption;
                        }
                    }
                }
            }
        }
        $data['caption'] = $caption;

        $price = $data['buy_product_price'];
        $fee = 0;

        $mPrice = $price;
        if ($request->get('price')) {
            $mPrice = $request->get('price');
        }

        $mFee = $fee;
        $mDisc = 0;
        if ($request->get('discount')) {
            $mDisc = $request->get('discount');
        }

        if (($price + $fee) > $mPrice) {
            $mDisc += ($price + $fee) - $mPrice;
        } else {
            $mFee += $mPrice - ($price + $fee);
        }

        $mFee = 0;
        $fTotal = $mPrice + $mFee - $mDisc;

        $data['real_price'] = $mPrice;
        $data['real_fee'] = $mFee;
        $data['real_disc'] = $mDisc;
        $data['real_total'] = $fTotal;
        $data['logo'] = env("APP_URL") . '/storage/logo.png';
        // $data['logo'] = '';

        $pdf = PDF::loadView('pdf.transaction_prepaid', $data);
        $pdf->setPaper(array(0,0,204,400));

        return $pdf->stream($data->transaction_id.'.pdf');
    }

    public function get_transaction_postpaid_print(Request $request, $id)
    {
        // $member = JWTAuth::toUser($request->token);
        $member = Member::where('id', 2543)->first();

        $data = MemberBuyPpob::with('product')->where('id', $id)->first();
        if (!$data) {
            return response()->json([
                'code' => 404,
                'message' => 'Transaction not found.'
            ], 404);
        }

        $data['details'] = array();
        if ($data['data']) {
          $data['details'] = json_decode($data['data']);
          foreach ($data['details'] as $key => $value) {
              if (gettype($value) == 'object') {
                  $formatted = '';
                  foreach ($value as $keyVal => $valueVal) {
                      if (sizeof($valueVal) > 0) {
                          if ($valueVal == 0 || $valueVal == "Rp0" || strpos($keyVal, 'Amount') !== false || strpos($keyVal, 'Admin') !== false) {
                          } else {
                              $formatted .= $keyVal.' ';
                              for ($i = 0; $i < sizeof($valueVal); $i++) {
                                  $formatted .= $valueVal[$i];
                                  if (($i + 1) < sizeof($valueVal)) {
                                      $formatted .= ', ';
                                  }
                              }
                              $formatted .= '; ';
                          }
                      }
                  }
                  $data['details']->$key = $formatted;
              } else {
                  if (strlen($value) == 0 || $value == 0 || $value == "Rp0" || strpos($key, 'Amount') !== false || strpos($key, 'Admin') !== false) {
                      unset($data['details']->$key);
                  }
              }
          }
        }

        if ($data->user_id != $member->id) {
            return response()->json([
                'code' => 404,
                'message' => 'Transaction not found.'
            ], 404);
        }

        $caption = 'ID Pelanggan';
        $product = Product::where('id', $data->product->id)->first();
        if ($product) {
            if ($product->caption) {
                $caption = $product->caption;
            } else {
                if ($product->category_id) {
                    $productCategory = ProductCategories::where('id', $product->category_id)->first();
                    if ($productCategory) {
                        if ($productCategory->caption) {
                            $caption = $productCategory->caption;
                        }
                    }
                }
            }
        }
        $data['caption'] = $caption;

        $price = $data['tagihan'];
        $fee = $data['admin'];

        $mPrice = $price;
        if ($request->get('price')) {
            $mPrice = $request->get('price');
        }

        $mFee = $fee;
        $mDisc = 0;
        if ($request->get('discount')) {
            $mDisc = $request->get('discount');
        }

        if (($price + $fee) > $mPrice) {
            $mDisc += ($price + $fee) - $mPrice;
        } else {
            $mFee += $mPrice - ($price + $fee);
        }

        $mPrice = $price;
        $fTotal = $mPrice + $mFee - $mDisc;

        $data['real_price'] = $mPrice;
        $data['real_fee'] = $mFee;
        $data['real_disc'] = $mDisc;
        $data['real_total'] = $fTotal;
        $data['logo'] = env("APP_URL") . '/storage/logo.png';
        // $data['logo'] = '';

        $pdf = PDF::loadView('pdf.transaction_postpaid', $data);
        $pdf->setPaper(array(0,0,204,650));

        return $pdf->stream($data->transaction_id.'.pdf');
    }

}
