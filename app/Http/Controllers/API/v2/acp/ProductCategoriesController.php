<?php

namespace App\Http\Controllers\API\v2\acp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v2\ProductCategories, App\Models\v2\Admin;
use JWTFactory, JWTAuth;
use Illuminate\Support\Facades\Config, Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ProductCategoriesController extends Controller
{
    function __construct()
    {
        Config::set('jwt.user', Admin::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => Admin::class,
        ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->get('name')) {
            return response()->json([
                'code' => 400,
                'message' => 'Category name required.'
            ], 400);
        }

        if (!$request->hasFile('icon')) {
            return response()->json([
                'code' => 400,
                'message' => 'Icon required.'
            ], 400);
        }

        // $admin = JWTAuth::toUser($request->token);
        //
        // if (!$admin) {
        //     return response()->json([
        //         'code' => 404,
        //         'message' => 'User not found.'
        //     ], 404);
        // }

        $path = '';
        if ($request->hasFile('icon')) {
            $path = $request->file('icon')->storeAs('product-categories-icons', Str::random(10).'.'.$request->file('icon')->getClientOriginalExtension());
        }

        $category = ProductCategories::create([
            'name' => $request->get('name'),
            'icon' => $path,
            'index' => ($request->get('index')) ? $request->get('index') : 0,
            'parent' => ($request->get('parent')) ? $request->get('parent') : 0,
            'filterUrl' => ($request->get('filterUrl')) ? $request->get('filterUrl') : NULL,
            'status' => ($request->get('status')) ? $request->get('status') : 1,
            'isPostPaid' => ($request->get('isPostPaid')) ? $request->get('isPostPaid') : 0,
        ]);

        return response()->json([
            'code' => 200,
            'message' => $category
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = ProductCategories::findOrFail($id);

        $path = '';
        if ($request->hasFile('icon')) {

            if ($data->icon != null) {
                if (strpos($data->icon, 'http') !== false) {
                } else {
                    Storage::delete($data->icon);
                }
            }

            $path = $request->file('icon')->storeAs('product-categories-icons', Str::random(10).'.'.$request->file('icon')->getClientOriginalExtension());
            $data->icon = $path;
        }
        $data->save();
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
