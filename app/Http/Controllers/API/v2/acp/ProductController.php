<?php

namespace App\Http\Controllers\API\v2\acp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v2\Product, App\Models\v2\Admin;
use JWTFactory, JWTAuth;
use Illuminate\Support\Facades\Config, Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Product::findOrFail($id);

        $path = '';
        if ($request->hasFile('icon')) {

            if ($data->icon != null) {
                if (strpos($data->icon, 'http') !== false) {
                } else {
                    Storage::delete($data->icon);
                }
            }

            $path = $request->file('icon')->storeAs('product-categories-icons', Str::random(10).'.'.$request->file('icon')->getClientOriginalExtension());
            $data->icon = $path;
        }
        $data->save();
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
