<?php

namespace App\Http\Controllers\API\v2\acp;

use App\Models\v2\Admin, App\Models\v2\Member, App\Models\v2\Otp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTFactory, JWTAuth, Validator, Twilio, Mail, DateTime, DateInterval;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;

class AuthController extends Controller
{
    function __construct()
    {
        Config::set('jwt.user', Admin::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => Admin::class,
        ]]);
    }

    public function login(Request $request)
    {
        if (!$request->get('username')) {
            return response()->json([
                'code' => 400,
                'message' => 'Username required.'
            ], 400);
        }

        if (!$request->get('password')) {
            return response()->json([
                'code' => 400,
                'message' => 'Password required.'
            ], 400);
        }

        $auth = Admin::where('user_name', $request->get('username'))->first();

        if (!$auth) {
            return response()->json([
                'code' => 404,
                'message' => 'Username not found.'
            ], 404);
        }

        $valid = Hash::check($request->get('password'), $auth->user_password);

        if (!$valid) {
            return response()->json([
                'code' => 401,
                'message' => 'Unauthorized.'
            ], 401);
        }

        $token = JWTAuth::fromUser($auth);
        $response = response()->json([
            'code' => 200,
            'user' => $auth,
            'token' => $token
        ], 200);

        return $response;
    }

    public function check_token(Request $request)
    {
        try {
            if (!$user = JWTAuth::toUser($request->token)) {
                return response()->json([
                    'code' => 404,
                    'message' => 'User not found'
                ], 404);
            } else {
                $user = JWTAuth::toUser($request->token);
                return response()->json([
                    'code' => 200,
                    'data' => ['user' => $user]
                ], 200);
            }
        } catch (Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => 'Something went wrong.'
            ], 500);
        }
    }

}
