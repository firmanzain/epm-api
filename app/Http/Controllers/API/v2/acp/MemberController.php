<?php

namespace App\Http\Controllers\API\v2\acp;

use App\Transformers\MemberTransformer;
use App\Transformers\OtpTransformer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTFactory, JWTAuth, Validator, Twilio, Mail, DateTime, DateInterval;
// Include all required models
use App\Models\v2\TopupMember, App\Models\v2\MemberBuyLevel, App\Models\v2\MemberBuyProduct,
    App\Models\v2\MemberBuyPpob, App\Models\v2\TransferSaldo, App\Models\v2\MemberWithdraw,
    App\Models\v2\MemberBonus, App\Models\v2\HistoryTransaksi, App\Models\v2\Deposit,
    App\Models\v2\Otp, App\Models\v2\Member, App\Models\v2\UserLevel;
// Include Mail
use App\Mail\v2\UserRegistered, App\Mail\v2\UserResetPassword;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class MemberController extends Controller
{

    public function move_avatar(Request $request)
    {
        $members = Member::get();

        foreach ($members as $member) {
            $member = Member::where('id', $member->id)->first();
            if ($member->avatar) {
                $filename = $member->avatar;
                if (file_exists(storage_path('app/'.$filename))) {
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $dateNow = Carbon::now();
                    $newFilename = Str::random(10).''.$dateNow->timestamp.'.'.$ext;

                    rename(storage_path('app/'.$filename), storage_path('app/avatars/'.$newFilename));
                    copy(storage_path('app/avatars/'.$newFilename), storage_path('app/public/avatars/'.$newFilename));
                    // unlink(storage_path('app/avatars/'.$newFilename));

                    $member->avatar = 'avatars/'.$newFilename;
                    $member->save();
                }
            }
        }

        return response()->json([
            'code' => 200,
            'message' => 'Success',
        ], 200);
    }

}
