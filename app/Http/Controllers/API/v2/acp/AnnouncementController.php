<?php

namespace App\Http\Controllers\API\v2\acp;

use App\Models\v2\Announcement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v2\ProductCategories, App\Models\v2\Admin;
use JWTFactory, JWTAuth;
use Illuminate\Support\Facades\Config, Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class AnnouncementController extends Controller
{
    function __construct()
    {
        Config::set('jwt.user', Admin::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => Admin::class,
        ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->get('title')) {
            return response()->json([
                'code' => 400,
                'message' => 'Banner title required.'
            ], 400);
        }

        if (!$request->get('desc')) {
            return response()->json([
                'code' => 400,
                'message' => 'Banner desc required.'
            ], 400);
        }

        if (!$request->hasFile('pict_url')) {
            return response()->json([
                'code' => 400,
                'message' => 'Banner pict_url required.'
            ], 400);
        }

        if (!$request->get('valid_until')) {
            return response()->json([
                'code' => 400,
                'message' => 'Banner valid_until required.'
            ], 400);
        }

        $path = '';
        if ($request->hasFile('pict_url')) {
            $path = $request->file('pict_url')->storeAs('announcements', Str::random(10).'.'.$request->file('pict_url')->getClientOriginalExtension());
        }

        $banner = Announcement::create([
            'title' => $request->get('title'),
            'desc' => $request->get('desc'),
            'pict_url' => $path,
            'action_url' => $request->get('action_url'),
            'valid_until' => $request->get('valid_until'),
            'isShow' => ($request->get('isShow')) ? $request->get('isShow') : 0,
        ]);

        return response()->json([
            'code' => 200,
            'message' => $banner
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\v2\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\v2\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\v2\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\v2\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        //
    }
}
