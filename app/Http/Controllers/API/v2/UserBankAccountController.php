<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTFactory, JWTAuth;
use App\Models\v2\UserBankAccount, App\Models\v2\Bank;
use App\Transformers\Serializer\EpmArraySerializer, App\Transformers\UserBankTransformer;

class UserBankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $member = JWTAuth::toUser($request->token);

        $bank = UserBankAccount::where('user_id', $member->id)->get();
        if (!$bank) {
            return response()->json([
                'code' => 404,
                'message' => 'Bank not found.'
            ], 404);
        }

        return fractal()
            ->collection($bank, new UserBankTransformer())
            ->serializeWith(new EpmArraySerializer())
            ->toArray();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->get('id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` required.'
            ], 400);
        }

        if (!$request->get('account_no')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `account_no` required.'
            ], 400);
        }

        if (!$request->get('account_holder')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `account_holder` required.'
            ], 400);
        }

        if (!$request->get('branch')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `branch` required.'
            ], 400);
        }

        $member = JWTAuth::toUser($request->token);
        if (!$member) {
            return response()->json([
                'code' => 404,
                'message' => 'User not found.'
            ], 404);
        }

        $bank = Bank::where('id', $request->get('id'))->first();
        if (!$bank) {
            return response()->json([
                'code' => 404,
                'message' => 'Bank not found.'
            ], 404);
        }

        $account = UserBankAccount::where('user_id', $member->id)->count();
        if ($account == 5) {
            return response()->json([
                'code' => 403,
                'message' => 'You have reached the maximum limit.'
            ], 403);
        }

        $bank = new UserBankAccount();
        $bank->user_id = $member->id;
        $bank->bank_id = $request->get('id');
        $bank->account_no = $request->get('account_no');
        $bank->account_holder = $request->get('account_holder');
        $bank->branch = $request->get('branch');
        $bank->save();

        return fractal()
            ->item($bank, new UserBankTransformer())
            ->toArray();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$id) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `bank_id` required.'
            ], 400);
        }

        if (!$request->get('id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` required.'
            ], 400);
        }

        $member = JWTAuth::toUser($request->token);
        if (!$member) {
            return response()->json([
                'code' => 404,
                'message' => 'User not found.'
            ], 404);
        }

        $account = UserBankAccount::where('user_id', $member->id)->where('id', $id)->first();
        if (!$account) {
            return response()->json([
                'code' => 404,
                'message' => 'Bank account not found.'
            ], 404);
        }

        $account = UserBankAccount::find($id);
        if ($id) {
            $bank = Bank::where('id', $request->get('id'))->first();
            if (!$bank) {
                return response()->json([
                    'code' => 404,
                    'message' => 'Bank not found.'
                ], 404);
            }
            $account->bank_id = $request->get('id');
        }

        if ($request->get('account_no')) {
            $account->account_no = $request->get('account_no');
        }

        if ($request->get('account_holder')) {
            $account->account_holder = $request->get('account_holder');
        }

        if ($request->get('branch')) {
            $account->branch = $request->get('branch');
        }

        $account->save();

        return fractal()
            ->item($account, new UserBankTransformer())
            ->toArray();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!$id) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` required.'
            ], 400);
        }

        $member = JWTAuth::toUser($request->token);
        if (!$member) {
            return response()->json([
                'code' => 404,
                'message' => 'User not found.'
            ], 404);
        }

        $account = UserBankAccount::where('user_id', $member->id)->where('id', $id)->first();
        if (!$account) {
            return response()->json([
                'code' => 404,
                'message' => 'Bank not found.'
            ], 404);
        }

        $account = UserBankAccount::find($id);
        $account->delete();

        return response()->json([
            'code' => 200,
            'message' => 'Success.'
        ], 200);
    }

}
