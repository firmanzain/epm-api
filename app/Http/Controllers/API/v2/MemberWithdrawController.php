<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTFactory, JWTAuth;
use App\Models\v2\MemberWithdraw;
use App\Transformers\MemberWithdrawTransformer;
use App\Transformers\Serializer\EpmArraySerializer;

class MemberWithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_history(Request $request)
    {
        $member = JWTAuth::toUser($request->token);

        $data = MemberWithdraw::with('member', 'member_bank_account.bank')
                                ->where('user_id', $member->id)
                                ->orderBy('created_at', 'desc')
                                ->get();

        return fractal()
               ->collection($data, new MemberWithdrawTransformer())
               ->serializeWith(new EpmArraySerializer())
               ->toArray();
    }
}
