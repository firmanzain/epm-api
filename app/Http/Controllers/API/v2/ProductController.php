<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTFactory, JWTAuth, DB, Carbon\Carbon;
use App\Http\Controllers\API\v2\vendor\VendorController;
use
    App\Models\v2\Product, App\Models\v2\ProductCategories, App\Models\v2\SendapiSupplier,
    App\Models\v2\MemberBuyProduct, App\Models\v2\MemberBuyPpob, App\Models\v2\UserToken,
    App\Models\v2\SupplierProduct;
use
    App\Transformers\Serializer\EpmArraySerializer, App\Transformers\ProductCategoryTransformer,
    App\Transformers\TransactionTransformer;
use App\Models\v2\Member;
use Illuminate\Support\Facades\Log;
use App\Jobs\ProcessTransactionPrepaid, App\Jobs\ProcessTransactionPostpaid;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_categories(Request $request, $id = NULL)
    {
        $member = JWTAuth::toUser($request->token);

        if ($member->active != 1) {

            $userToken = UserToken::where('user_id', $member->id)
                                  ->where('token', $request->bearerToken())
                                  ->first();
            if ($userToken) {
                $userToken->delete();
            }

            $delete = JWTAuth::invalidate($request->token);

            return response()->json([
                'code' => 403,
                'message' => 'Your account has been disabled by administrator. Please contact customer service to resolve this problem.'
            ], 403);
        }

        $isPremium = ($member->user_level_id != 1);

        if (!$id) {
            $categories = ProductCategories::select(
                '*',
                DB::raw('"'. $isPremium . '" as isPremium')
            )->where('parent', 0)
                ->where('status', 1)
                ->orderBy('index', 'asc')->get();

            return fractal()
                ->collection($categories, new ProductCategoryTransformer())
                ->serializeWith(new EpmArraySerializer())
                ->parseIncludes('sub')
                ->toArray();
        }

        $category = ProductCategories::select(
            '*',
            DB::raw('"'. $isPremium . '" as isPremium')
        )->where('id', $id)->first();
        if (!$category) {
            return response()->json([
                'code' => 404,
                'message' => 'Category not found.'
            ], 404);
        }
        return fractal()
            ->item($category, new ProductCategoryTransformer())
            ->serializeWith(new EpmArraySerializer())
            ->parseIncludes('sub')
            ->toArray();
    }

    public function inquiry_postpaid(Request $request)
    {
        if (!$request->get('id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` is required.'
            ], 400);
        }

        if (!$request->get('customer_id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `customer_id` is required.'
            ], 400);
        }

        $member = JWTAuth::toUser($request->token);

        $product = Product::where('id', $request->get('id'))
            ->where('price', 0)
            ->where('status_active', 1)
            ->first();

        if (!$product) {
            return response()->json([
                'code' => 404,
                'message' => 'Product not found.'
            ], 404);
        }

        $flagZero = 0;
        $string = $request->get('customer_id');
        for ($i = 0; $i < strlen($string); $i++) {
            if ((int)$string[$i] != 0) {
                break;
            }
            $flagZero++;
        }

        $customer_id = $request->get('customer_id');
        if ($product->id == 347) {
            $customer_id = sprintf("%016d", substr($request->get('customer_id'), $flagZero));
        }

        if ($product->price != 0 && $product->price_premium != 0) {
            return response()->json([
                'code' => 403,
                'message' => 'Not postpaid product.'
            ], 403);
        }

        $suppProducts = $product->SupplierProduct()->where('status_active', 1)
                                ->orderBy('biaya_admin', 'asc')->get();
        $result = [];
        foreach ($suppProducts as $suppProduct) {
            $supplier = $suppProduct->suppliers()->first();
            $class = $supplier->controller_name;
            $instance = new $class();

            if (!($instance instanceof VendorController)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Please wait. We will back soon',
                    'developerMessage' => 'Must implement VendorController class.'
                ], 500);
            }

            $result = $instance::inquiry_postpaid($request->get('id'), $suppProduct->voucher_code, $customer_id);
            $resultCode = $result['status'];
            switch ($resultCode) {
                case 0:
                    return response()->json($result, 200);
                case 1:
                    return response()->json([
                        'code' => 404,
                        'message' => 'Customer ID not found'
                    ], 404);
                case 2:
                    return response()->json([
                        'code' => 403,
                        'message' => 'Already Paid'
                    ], 403);
                case 3:
                    return response()->json([
                        'code' => 403,
                        'message' => "Bill isn't available yet"
                    ], 403);
            }
        }

        return response()->json([
            'code' => 500,
            'message' => 'Please wait. We will back soon',
            'developerMessage' => 'All Vendor is Down.'
        ], 500);
    }

    public function payment_postpaid(Request $request)
    {
        if (!$request->get('id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` is required.'
            ], 400);
        }

        if (!$request->get('customer_id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `customer_id` is required.'
            ], 400);
        }

        if (!$request->get('payment_method')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `payment_method` is required.'
            ], 400);
        }

        if (!$request->get('password')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `password` required.'
            ], 400);
        }

        $member = JWTAuth::toUser($request->token);

        $check = HelperController::checkAuth($member->id, $request->get('password'));
        if (!$check) {
            return response()->json([
                'code' => 403,
                'message' => 'Password not match.'
            ], 403);
        }

        $product = Product::where('id', $request->get('id'))
            ->where('price', 0)
            ->where('status_active', 1)
            ->first();

        if (!$product) {
            return response()->json([
                'code' => 404,
                'message' => 'Product not found.'
            ], 404);
        }

        $flagZero = 0;
        $string = $request->get('customer_id');
        for ($i = 0; $i < strlen($string); $i++) {
            if ((int)$string[$i] != 0) {
                break;
            }
            $flagZero++;
        }

        $customer_id = $request->get('customer_id');
        if ($product->id == 347) {
            $customer_id = sprintf("%016d", substr($request->get('customer_id'), $flagZero));
        }

        if ($product->price != 0 && $product->price_premium != 0) {
            return response()->json([
                'code' => 403,
                'message' => 'Not postpaid product.'
            ], 403);
        }

        if ($request->get('payment_method') != 1 && $request->get('payment_method') != 2) {
            return response()->json([
                'code' => 403,
                'message' => 'Payment method not valid.'
            ], 403);
        }

        $category = 'POSTPAID';
        $category = preg_replace("/[\s\^$.|?*+()-]/", "", $category);
        $dateNow = Carbon::now();
        $transactionId = strtoupper($category) . $dateNow->timestamp;
        $buy = new MemberBuyPpob();

        $buy->ppob_id = $transactionId;
        $buy->charge = 0;
        $buy->tagihan = 0;
        $buy->admin = 0;
        $buy->total = 0;
        $buy->status_buy_ppob = 2; // Set status as pending
        $buy->msisdn = $customer_id;
        $buy->reff_number = NULL;
        $buy->nama_pelanggan = NULL;
        $buy->user_id = $member->id;
        $buy->product_id = $request->get('id');
        $buy->sendapi_supplier_id = NULL;
        $buy->result_supplier_id = NULL;
        $buy->payment_type = $request->get('payment_method');
        $buy->save();

        $receiptUrl = env("APP_URL") . '/v2/transaction/postpaid/' . $buy->id . '/print';

        if ($request->get('price')) {
            $data['price_member'] = $request->get('price');
            $receiptUrl .= '?price=' . $request->get('price');
            if ($request->get('discount')) {
                $receiptUrl .= '&discount=' . $request->get('discount');
            }
        }

        $buy->receiptUrl = $receiptUrl;
        $buy->save();

        $statusName = 'Pending';
        $phone = HelperController::format_phone($member->phone);

        // INSERT LOG
        Log::channel('transaction')->info('Create transaction: user = '.$member->id.', trx = '.$transactionId);

        $suppProducts = $product->SupplierProduct()->where('status_active', 1)
                                ->orderBy('biaya_admin', 'asc')->get();
        foreach ($suppProducts as $suppProduct) {
            $supplier = $suppProduct->suppliers()->first();
            $class = $supplier->controller_name;
            $instance = new $class();

            if (!($instance instanceof VendorController)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Please wait. We will back soon',
                    'developerMessage' => 'Must implement VendorController class.'
                ], 500);
            }

            $buy->supplier_id = $supplier->id;
            $buy->save();

            $result = $instance::pay_postpaid($transactionId, $suppProduct->voucher_code, $customer_id);
            // $result = 0: Success/Pending, 1: Error Transaction, 2: Insufficient Ballance
            // END INSERT SEND API SUPPLIER

            $resultCode = $result['status'];

            if ($resultCode != 0) {
                fcm()
                    ->toTopic($phone.'_android') // $topic must an string (topic name)
                    ->notification([
                        'title' => 'Info Transaksi',
                        // 'body' => 'Transaksi pembelian '.$product->name.' '.$statusName,
                        'body' => 'Transaksi pembelian '.$product->name.' telah selesai, lihat history untuk mengetahui status transaksi',
                    ])
                    ->send();
            }

            switch ($resultCode) {
                case 1:
                    return response()->json([
                        'code' => 404,
                        'message' => 'Customer ID not found'
                    ], 404);
                case 2:
                    return response()->json([
                        'code' => 403,
                        'message' => 'Already Paid'
                    ], 403);
                case 3:
                    return response()->json([
                        'code' => 403,
                        'message' => 'Insufficient balance'
                    ], 403);
            }

            if ($resultCode == 0) {
                $statusName = 'Sukses';
                break;
            }
        }

        if ($buy->supplier_id == NULL) {
            $buy->status_buy_ppob = 0;
        }

        fcm()
            ->toTopic($phone.'_android') // $topic must an string (topic name)
            ->notification([
                'title' => 'Info Transaksi',
                // 'body' => 'Transaksi pembelian '.$product->name.' '.$statusName,
                'body' => 'Transaksi pembelian '.$product->name.' telah selesai, lihat history untuk mengetahui status transaksi',
            ])
            ->send();

        $buyPpob = MemberBuyPpob::select(
            'id',
            'ppob_id as transaction_id',
            'msisdn as cust_id',
            'reff_number as sn',
            'nama_pelanggan as data',
            'tagihan as amount',
            'admin as fee',
            DB::raw('9 as category'),
            DB::raw('"Kredit" as type'),
            'status_buy_ppob as status',
            DB::raw('0 as bank_id'),
            'msisdn as reference_id',
            'product_id as origin_id',
            'payment_type as method',
            'receiptUrl',
            'created_at'
        )
        ->where('user_id', $member->id)
        ->where('ppob_id', $transactionId)->first();

        return fractal()
            ->item($buyPpob, new TransactionTransformer())
            ->parseIncludes('product')
            ->toArray();
    }

    public function vendor_by_product(Request $request, $id)
    {
        if (!$id) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` is required.'
            ], 400);
        }

        $product = Product::where('id', $id)
                            ->where('price', 0)
                            ->where('status_active', 1)
                            ->first();

        if (!$product) {
            return response()->json([
                'code' => 404,
                'message' => 'Product not found.'
            ], 404);
        }

        $result = array();
        $suppProducts = $product->SupplierProduct()->where('status_active', 1)
                                ->orderBy('biaya_admin', 'asc')->get();
        foreach ($suppProducts as $key => $value) {
            $tempResult = array(
                'sort' => $key,
                'vendorId' => $value->id,
            );
            array_push($result, $tempResult);
        }

        return $result;
    }

    public function inquiry_postpaid_v3(Request $request, $id, $customer)
    {
        // return response()->json([
        //     'code' => 500,
        //     'message' => 'Please wait. We will back soon',
        //     'developerMessage' => 'All Vendor is Down.'
        // ], 500);

        if (!$id) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` is required.'
            ], 400);
        }

        if (!$customer) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `customer` is required.'
            ], 400);
        }

        if ($id == 'spp') {
          $suppProduct = SupplierProduct::where('voucher_code', 'SPP')->first();
        } else {
          $suppProduct = SupplierProduct::find($id);
        }
        if (!$suppProduct) {
            return response()->json([
                'code' => 404,
                'message' => 'Product not found.'
            ], 404);
        }

        $flagZero = 0;
        $string = $customer;
        for ($i = 0; $i < strlen($string); $i++) {
            if ((int)$string[$i] != 0) {
                break;
            }
            $flagZero++;
        }

        $customer_id = $customer;
        if ($suppProduct->product_id == 347) {
            $customer_id = sprintf("%016d", substr($customer, $flagZero));
        }

        $supplier = $suppProduct->suppliers()->first();
        $class = $supplier->controller_name;
        $instance = new $class();

        if (!($instance instanceof VendorController)) {
            return response()->json([
                'code' => 500,
                'message' => 'Please wait. We will back soon',
                'developerMessage' => 'Must implement VendorController class.'
            ], 500);
        }

        $result = $instance::inquiry_postpaid($suppProduct->product_id, $suppProduct->voucher_code, $customer_id);
        $resultCode = $result['status'];
        switch ($resultCode) {
            case 0:
                return response()->json($result, 200);
            case 1:
                return response()->json([
                    'code' => 404,
                    'message' => 'Customer ID not found'
                ], 404);
            case 2:
                return response()->json([
                    'code' => 403,
                    'message' => 'Already Paid'
                ], 403);
            case 3:
                return response()->json([
                    'code' => 403,
                    'message' => "Bill isn't available yet"
                ], 403);
            case 4:
                return response()->json([
                    'code' => 408,
                    'message' => "Next vendor"
                ], 408);
        }

        return response()->json([
            'code' => 408,
            'message' => 'Please wait. We will back soon',
            'developerMessage' => 'Vendor down',
            'developerResponse' => $result,
        ], 408);
    }

    public function payment_postpaid_v3(Request $request)
    {
        if (!$request->input('id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` is required.'
            ], 400);
        }

        if (!$request->post('customer_id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `customer_id` is required.'
            ], 400);
        }

        if (!$request->input('payment_method')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `payment_method` is required.'
            ], 400);
        }

        if (!$request->input('password')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `password` required.'
            ], 400);
        }

        if (!$request->input('trx_id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `trx_id` required.'
            ], 400);
        }

        if (!$request->input('amount')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `amount` required.'
            ], 400);
        }

        // if (!$request->input('amount_vendor')) {
        //     return response()->json([
        //         'code' => 400,
        //         'message' => 'Field `amount_vendor` required.'
        //     ], 400);
        // }

        if (!$request->input('inquiry')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `inquiry` required.'
            ], 400);
        }

        // $checkJsonInquiry = ProductController::isJSON($request->input('inquiry'));
        // if (!$checkJsonInquiry) {
        //     return response()->json([
        //         'code' => 403,
        //         'message' => 'Field `inquiry` must a json format.'
        //     ], 403);
        // }

        if (!is_array($request->input('inquiry'))) {
            return response()->json([
                'code' => 403,
                'message' => 'Field `inquiry` must a json format.'
            ], 403);
        }

        $member = JWTAuth::toUser($request->token);
        $check = HelperController::checkAuth($member->id, $request->input('password'));
        if (!$check) {
            return response()->json([
                'code' => 403,
                'message' => 'Password not match.'
            ], 403);
        }

        if ($request->input('id') == 'spp') {
            $suppProduct = SupplierProduct::where('voucher_code', 'SPP')->first();
        } else {
            $suppProduct = SupplierProduct::find($request->input('id'));
        }
        if (!$suppProduct) {
            return response()->json([
                'code' => 404,
                'message' => 'Product not found.'
            ], 404);
        }
        $adminEpm = $suppProduct->biaya_admin;
        $feeEpm = $suppProduct->fee;

        if ($request->input('id') == 'spp') {
        $product = Product::where('id', $suppProduct->product_id)
        ->where('price', -1)
        ->where('status_active', 1)
        ->first();
        } else {
        $product = Product::where('id', $suppProduct->product_id)
        ->where('price', 0)
        ->where('status_active', 1)
        ->first();
        }
        if (!$product) {
            return response()->json([
                'code' => 404,
                'message' => 'Product not found.'
            ], 404);
        }

        if ($request->input('payment_method') != 1 && $request->input('payment_method') != 2) {
            return response()->json([
                'code' => 403,
                'message' => 'Payment method not valid.'
            ], 403);
        }

        $saldo = HelperController::member_saldo($member->id);
        if ($request->input('payment_method') == 2) {
            $saldo = HelperController::member_point($member->id);
        }

        // if ($saldo < (intval($request->input('amount')) + intval($adminEpm) - intval($feeEpm))) {
        if ($saldo < (intval($request->input('amount')) + intval($adminEpm))) {
            return response()->json([
                'code' => 403,
                'message' => 'Insufficient balance'
            ], 403);
        }

        // FOR BPJS PRODUCT
        $flagZero = 0;
        $string = $request->input('customer_id');
        for ($i = 0; $i < strlen($string); $i++) {
            if ((int)$string[$i] != 0) {
                break;
            }
            $flagZero++;
        }

        $customer_id = $request->input('customer_id');
        if ($suppProduct->product_id == 347) {
            $customer_id = sprintf("%016d", substr($request->input('customer_id'), $flagZero));
        }

        $category = 'POSTPAID';
        $category = preg_replace("/[\s\^$.|?*+()-]/", "", $category);
        $dateNow = Carbon::now();
        $transactionId = strtoupper($category) . $dateNow->timestamp;
        $buy = new MemberBuyPpob();

        $buy->ppob_id = $transactionId;
        $buy->charge = 0;
        $buy->tagihan = 0;
        $buy->admin = 0;
        $buy->total = 0;
        $buy->status_buy_ppob = 2; // Set status as pending
        $buy->msisdn = $customer_id;
        $buy->reff_number = NULL;
        $buy->nama_pelanggan = NULL;
        $buy->user_id = $member->id;
        $buy->product_id = $suppProduct->product_id;
        $buy->sendapi_supplier_id = NULL;
        $buy->result_supplier_id = NULL;
        $buy->payment_type = $request->input('payment_method');
        $buy->data = json_encode($request->input('inquiry'));
        $buy->save();

        $receiptUrl = env("APP_URL") . '/v2/transaction/postpaid/' . $buy->id . '/print';

        if ($request->input('price')) {
            $data['price_member'] = $request->input('price');
            $receiptUrl .= '?price=' . $request->input('price');
            if ($request->input('discount')) {
                $receiptUrl .= '&discount=' . $request->input('discount');
            }
        }

        $buy->receiptUrl = $receiptUrl;
        $buy->save();

        $statusName = 'Pending';
        $phone = HelperController::format_phone($member->phone);

        // INSERT LOG
        Log::channel('transaction')->info('Create transaction: user = '.$member->id.', trx = '.$transactionId);

        $supplier = $suppProduct->suppliers()->first();
        $class = $supplier->controller_name;
        $instance = new $class();

        if (!($instance instanceof VendorController)) {
            return response()->json([
                'code' => 500,
                'message' => 'Please wait. We will back soon',
                'developerMessage' => 'Must implement VendorController class.'
            ], 500);
        }

        $buy->supplier_id = $supplier->id;
        $buy->save();

        $result = $instance::pay_postpaid($transactionId, $suppProduct->voucher_code, $customer_id, $request->input('trx_id'), $request->input('amount'));
        // $result = 0: Success/Pending, 1: Error Transaction, 2: Insufficient Ballance

        $resultCode = $result['status'];
        if ($resultCode != 0) {
            $check = MemberBuyPpob::where('ppob_id', $transactionId)->first();
            if ($check) {
                if ($check->status_buy_ppob == 0) {
                  $statusName = 'Gagal';
                }
            }

            fcm()
                ->toTopic($phone.'_android') // $topic must an string (topic name)
                ->notification([
                    'title' => 'Info Transaksi',
                    // 'body' => 'Transaksi pembelian '.$product->name.' '.$statusName,
                    'body' => 'Transaksi pembelian '.$product->name.' telah selesai, lihat history untuk mengetahui status transaksi',
                ])
                ->send();
        }

        if ($resultCode != 0) {
            switch ($resultCode) {
                case 1:
                    return response()->json([
                        'code' => 404,
                        'message' => 'Customer ID not found'
                    ], 404);
                case 2:
                    return response()->json([
                        'code' => 403,
                        'message' => 'Already Paid'
                    ], 403);
                case 3:
                    return response()->json([
                        'code' => 403,
                        'message' => 'Insufficient balance'
                    ], 403);
                default:
                    return response()->json([
                        'code' => 500,
                        'message' => 'Please wait. We will back soon',
                        'developerMessage' => 'All Vendor is Down.'
                    ], 500);
            }
        }

        $buyPpob = MemberBuyPpob::select(
            'id',
            'ppob_id as transaction_id',
            'msisdn as cust_id',
            'reff_number as sn',
            'nama_pelanggan as data',
            'tagihan as amount',
            'admin as fee',
            DB::raw('9 as category'),
            DB::raw('"Kredit" as type'),
            'status_buy_ppob as status',
            DB::raw('0 as bank_id'),
            'msisdn as reference_id',
            'product_id as origin_id',
            'payment_type as method',
            'receiptUrl',
            'created_at',
            'data as detail'
        )
        ->where('user_id', $member->id)
        ->where('ppob_id', $transactionId)->first();

        return fractal()
            ->item($buyPpob, new TransactionTransformer())
            ->parseIncludes('product')
            ->toArray();
    }

    public function payment_prepaid(Request $request)
    {
        // return response()->json([
        //     'code' => 500,
        //     'message' => 'Please wait. We will back soon',
        //     'developerMessage' => 'All Vendor is Down.'
        // ], 500);

        if (!$request->get('id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` is required.'
            ], 400);
        }

        if (!$request->get('customer_id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `customer_id` is required.'
            ], 400);
        }

        if (!$request->get('payment_method')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `payment_method` is required.'
            ], 400);
        }

        if (!$request->get('password')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `password` required.'
            ], 400);
        }

        $member = JWTAuth::toUser($request->token);

        $check = HelperController::checkAuth($member->id, $request->get('password'));
        if (!$check) {
            return response()->json([
                'code' => 403,
                'message' => 'Password not match.'
            ], 403);
        }

        $product = Product::where('id', $request->get('id'))
            ->where('status_active', 1)
            ->first();

        if (!$product) {
            return response()->json([
                'code' => 404,
                'message' => 'Product not found.'
            ], 404);
        }

        if ($product->price == 0 && $product->price_premium == 0) {
            return response()->json([
                'code' => 403,
                'message' => 'Not prepaid product.'
            ], 403);
        }

        if ($request->get('payment_method') != 1 && $request->get('payment_method') != 2) {
            return response()->json([
                'code' => 403,
                'message' => 'Payment method not valid.'
            ], 403);
        }

        $price = $product->price;
        if ($member->user_level_id != 1) {
            $price = $product->price_premium;
            if ($product->price_premium == 0) {
                $price = $product->price;
            }
        }

        $saldo = HelperController::member_saldo($member->id);
        if ($request->get('payment_method') == 2) {
            $saldo = HelperController::member_point($member->id);
        }

        if ($saldo < $price) {
            return response()->json([
                'code' => 403,
                'message' => 'Insufficient balance'
            ], 403);
        }

        $category = 'PREPAID';
        $category = preg_replace("/[\s\^$.|?*+()-]/", "", $category);
        $dateNow = Carbon::now();
        $transactionId = strtoupper($category) . $dateNow->timestamp;
        $buy = new MemberBuyProduct();

        // INSERT MEMBER BUY PRODUCT
        $buy->transaction_id = $transactionId;
        $buy->buy_product_price = $price;
        $buy->status_buy_product = 2; // Set status as pending
        $buy->msisdn = $request->get('customer_id');
        $buy->nama_pelanggan = NULL;
        $buy->vsn = NULL;
        $buy->user_id = $member->id;
        $buy->product_id = $request->get('id');
        $buy->sendapi_supplier_id = NULL;
        $buy->result_supplier_id = NULL;
        $buy->payment_type = $request->get('payment_method');
        $buy->save();

        $receiptUrl = env("APP_URL") . '/v2/transaction/prepaid/' . $buy->id . '/print';

        if ($request->get('price')) {
            $data['price_member'] = $request->get('price');
            $receiptUrl .= '?price=' . $request->get('price');
            if ($request->get('discount')) {
                $receiptUrl .= '&discount=' . $request->get('discount');
            }
        }

        $buy->receiptUrl = $receiptUrl;
        $buy->save();
        // END INSERT MEMBER BUY PRODUCT

        // INSERT LOG
        Log::channel('transaction')->info('Create transaction: user = '.$member->id.', trx = '.$transactionId);

        $buyProduct = MemberBuyProduct::select(
            'id',
            'transaction_id',
            'msisdn as cust_id',
            'vsn as sn',
            'nama_pelanggan as data',
            'buy_product_price as amount',
            DB::raw('0 as fee'),
            DB::raw('8 as category'),
            DB::raw('"Kredit" as type'),
            'status_buy_product as status',
            DB::raw('0 as bank_id'),
            'msisdn as reference_id',
            'product_id as origin_id',
            'payment_type as method',
            'receiptUrl',
            'created_at',
            'data as detail'
        )
        ->where('user_id', $member->id)
        ->where('transaction_id', $transactionId)->first();

        ProcessTransactionPrepaid::dispatch($buyProduct);

        return fractal()
            ->item($buyProduct, new TransactionTransformer())
            ->parseIncludes('product')
            ->toArray();
    }

    private static function isJSON($string) {
       return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
}
