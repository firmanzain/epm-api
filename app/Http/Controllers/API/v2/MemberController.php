<?php

namespace App\Http\Controllers\API\v2;

use App\Models\v2\Benefit;
use App\Models\v2\Member;
use
    App\Transformers\Serializer\EpmArraySerializer, App\Transformers\MemberTransformer,
    App\Transformers\OtpTransformer, App\Transformers\MemberBuyLevelTransformer,
    App\Transformers\UserLevelTransformer, App\Transformers\BenefitLevelTransformer,
    App\Transformers\UserLevelReferalTransformer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTFactory, JWTAuth, Validator, Twilio, Mail, DateTime, DateInterval, DB;
// Include all required models
use
    App\Models\v2\TopupMember, App\Models\v2\MemberBuyLevel, App\Models\v2\MemberBuyProduct,
    App\Models\v2\MemberBuyPpob, App\Models\v2\TransferSaldo, App\Models\v2\MemberWithdraw,
    App\Models\v2\MemberBonus, App\Models\v2\HistoryTransaksi, App\Models\v2\Deposit,
    App\Models\v2\Otp, App\Models\v2\UserLevel, App\Models\v2\BenefitUserLevels,
    App\Models\v2\UserToken;
// Include Mail
use App\Mail\v2\UserRegistered, App\Mail\v2\UserResetPassword;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private function format_phone($phone)
    {
        $phone = trim(str_replace(' ', '', $phone));
        if (substr($phone, 0, 1) == '0') {
            $phone = '+62' . substr($phone, 1);
        }
        return $phone;
    }

    private function is_allow_otp_request($phone)
    {
        $now = new DateTime();
        $total_attempts_in_day = Otp::where([
            'phone_number' => $phone
        ])->whereRaw('DATE_FORMAT(created_at, "%Y-%m-%d")', $now->format('Y-m-d'))->count();
        if ($total_attempts_in_day < 5) {
            return true;
        } else {
            return false;
        }
    }

    private function send_otp($phone_number)
    {
        if (!$this->is_allow_otp_request($phone_number)) {
            return response()->json([
                'code' => 403,
                'message' => 'Exceed the maximum daily OTP request.'
            ], 403);
        }

        $now = Carbon::now();
        $otp = new Otp();
        $otp->phone_number = $phone_number;
        $otp->otp = mt_rand(100000, 999999);
        $otp->active = true;
        $otp->created_at = $now;
        $otp->expired_at = $now->addMinute();
        $otp->save();

        $message = str_replace('{{otp}}', $otp->otp, env('TWILIO_OTP_MESSAGE'));
        Twilio::message($phone_number, $message);

        return fractal()
            ->item($otp, new OtpTransformer())
            ->toArray();
    }

    public function otp_request(Request $request)
    {
        if (!$request->get('phone')) {
            return response()->json([
                'code' => 400,
                'message' => 'Phone number required.'
            ], 400);
        }

        $phone_number = $this->format_phone($request->get('phone'));

        try {
            $member = Member::where('phone', $phone_number)->count();
            if ($member) {
                return response()->json([
                    'code' => 400,
                    'message' => 'Phone number already registered.'
                ], 400);
            }

            $activeOtp = Otp::where('phone_number', $phone_number)
                ->where('active', 1)
                ->first();

            if (!$activeOtp) return $this->send_otp($phone_number);
            if (Carbon::parse($activeOtp->created_at)->addHours(2) >= Carbon::now()) {
                return fractal()
                    ->item($activeOtp, new OtpTransformer())
                    ->toArray();
            }

            $activeOtp->active = false;
            $activeOtp->update();

            return $this->send_otp($phone_number);

        } catch (Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => 'Something went wrong.'
            ], 500);
        }
    }

    public function otp_resend(Request $request)
    {
        if (!$request->get('phone')) {
            return response()->json([
                'code' => 400,
                'message' => 'Phone number required.'
            ], 400);
        }

        $phone_number = $this->format_phone($request->get('phone'));

        try {
            $member = Member::where('phone', $phone_number)->count();
            if ($member) {
                return response()->json([
                    'code' => 400,
                    'message' => 'Phone number already registered.'
                ], 400);
            }

            $activeOtp = Otp::where('phone_number', $phone_number)
                ->where('active', 1)
                ->first();

            if (!$activeOtp) return $this->send_otp($phone_number);
            if (new DateTime($activeOtp->expired_at) >= new DateTime()) {
                return fractal()
                    ->item($activeOtp, new OtpTransformer())
                    ->toArray();
            }

            $activeOtp->active = false;
            $activeOtp->update();

            return $this->send_otp($phone_number);

        } catch (Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => 'Something went wrong.'
            ], 500);
        }
    }

    public function otp_verify(Request $request)
    {
        if (!$request->get('phone') || !$request->get('otp')) {
            return response()->json([
                'code' => 400,
                'message' => 'field `phone` and `otp` required.'
            ], 400);
        }

        $phone_number = $this->format_phone($request->get('phone'));

        try {
            $activeOtp = Otp::where('phone_number', $phone_number)
                ->where('active', 1)
                ->first();

            if (!$activeOtp) {
                return response()->json([
                    'code' => 404,
                    'message' => 'Phone number not found or has been verified.'
                ], 404);
            }

            if ($activeOtp->otp != $request->get('otp')) {
                return response()->json([
                    'code' => 403,
                    'message' => 'Wrong OTP code.'
                ], 403);
            }

            $activeOtp->active = false;

            if (Carbon::parse($activeOtp->created_at)->addHours(2) < Carbon::now()) {
                $activeOtp->update();
                return response()->json([
                    'code' => 403,
                    'message' => 'OTP code has been expired.'
                ], 403);
            }

            $activeOtp->active = -1;
            $activeOtp->update();

            return fractal()
                ->item($activeOtp, new OtpTransformer())
                ->toArray();
        } catch (Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => 'Something went wrong.'
            ], 500);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'password' => 'required',
            'phone' => 'required|min:10|max:15|unique:users',
            'otp' => 'required|max:6'
        ]);

        if ($validator->fails()) {
            $error = json_decode(json_encode($validator->errors()), true);

            $message = '';

            if (isset($error['email'])) {
                $message = $error['email'][0];
            } else if (isset($error['name'])) {
                $message = $error['name'][0];
            } else if (isset($error['password'])) {
                $message = $error['password'][0];
            } else if (isset($error['phone'])) {
                $message = $error['phone'][0];
            } else if (isset($error['otp'])) {
                $message = $error['otp'][0];
            }

            return response()->json([
                'code' => 400,
                'message' => $message
            ], 400);
        }

        $phone_number = $this->format_phone($request->get('phone'));

        $activeOtp = Otp::where('phone_number', $phone_number)
            ->where('otp', $request->get('otp'))
            ->where('active', -1)
            ->first();

        if (!$activeOtp) {
            return response()->json([
                'code' => 403,
                'message' => "Please verify Phone Number first"
            ], 403);
        }

        $member = Member::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'phone' => $phone_number,
            'user_level_id' => 1,
            'referal_id' => str_replace('+62', 'EPM', $phone_number),
        ]);

        Mail::to($member)->send(new UserRegistered($member));

        $token = JWTAuth::fromUser($member);

        $userToken = new UserToken();
        $userToken->user_id = $member->id;
        $userToken->token = $token;
        $userToken->save();

        return fractal()
            ->item($member, new MemberTransformer())
            ->addMeta([
                'token' => $token
            ])
            ->toArray();
    }

    public function login(Request $request)
    {
        if (!$request->get('phone') || !$request->get('password')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `phone` and `password` are required.'
            ], 400);
        }

        $phone_number = $this->format_phone($request->get('phone'));

        if (Auth::attempt(['phone' => $phone_number, 'password' => $request->get('password')])) {
            $member = Member::where('phone', $phone_number)->first();

            if ($member->active != 1) {
                return response()->json([
                    'code' => 403,
                    'message' => 'Your account has been disabled by administrator. Please contact customer service to resolve this problem.'
                ], 403);
            }

            $token = JWTAuth::fromUser($member);

            $userToken = new UserToken();
            $userToken->user_id = $member->id;
            $userToken->token = $token;
            $userToken->save();

            return fractal()
                ->item($member, new MemberTransformer())
                ->addMeta([
                    'token' => $token
                ])
                ->toArray();
        } else {
            return response()->json([
                'code' => 401,
                'message' => 'Your mobile number and / or password you entered is not appropriate.'
            ], 401);
        }
    }

    public function reset_link_request(Request $request)
    {
        if (!$request->get('email') && !$request->get('encoded_phone')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `email` or `encoded_phone` is required.'
            ], 400);
        }

        if ($request->get('encoded_phone')) {
            $decoded_phone = $this->decode_phone_number($request->get('encoded_phone'));
            if (!$decoded_phone) {
                return response()->json([
                    'code' => 403,
                    'message' => 'Not valid Phone Number.'
                ], 403);
            }

            $phone_number = $this->format_phone($decoded_phone);

            $member = Member::where('phone', $phone_number)->first();
        } else {
            $member = Member::where('email', $request->get('email'))->first();
        }

        if (!$member) {
            return response()->json([
                'code' => 404,
                'message' => 'User not found.'
            ], 404);
        }

        if ($member->remember_token) {
            $checkExpired = $this->decode_reset_link($member->remember_token);
            $checkExpired = $checkExpired['expired_at'];
            // $checkExpired = Carbon::parse($checkExpired['expired_at'])->timestamp;
            $timeNow = Carbon::now()->timestamp;

            if ($checkExpired >= $timeNow) {
                return response()->json([
                    'code' => 403,
                    'message' => 'Request for password reset is only allowed once in 2 hours.'
                ], 403);
            }
        }

        $member->remember_token = $this->encode_reset_link($member->phone);
        $member->save();

        Mail::to($member)->send(new UserResetPassword($member));

        return response()->json([
            'code' => 200,
            'message' => 'Reset link has been sent to ' . $member->email
        ], 200);
    }

    private function decode_phone_number($phone)
    {
        $decoded = base64_decode($phone);
        $split = explode('.', $decoded, 2);
        if (count($split) < 2) return null;
        $encoded_phone = $split[0];
        $verify = $split[1];
        $decoded_phone = base64_decode($encoded_phone);
        preg_match_all("/\d/", $decoded_phone, $matches);
        $key = array_sum($matches[0]);
        if ($key != $verify) return null;
        return $decoded_phone;
    }

    private function decode_reset_link($link)
    {
        $decoded_all = base64_decode($link);
        $split1 = explode('.', $decoded_all, 2);
        if (count($split1) < 2) return null;
        $encoded_expired = $split1[0];
        $verify = $split1[1];

        $decoded_expired = base64_decode($encoded_expired);
        $split2 = explode('.', $decoded_expired, 2);
        if (count($split2) < 2) return null;
        $encoded_phone = $split2[0];
        $expired = $split2[1];

        preg_match_all("/\d/", $expired, $matches);
        $key = array_sum($matches[0]);
        if ($key != $verify) return null;
        $decoded_phone = base64_decode($encoded_phone);

        return ['phone' => $decoded_phone, 'expired_at' => $expired];
    }

    private function encode_reset_link($phone)
    {
        $encoded_phone = base64_encode($phone);
        $timestamp = Carbon::now()->addHours(2)->timestamp;
        preg_match_all("/\d/", $timestamp, $matches);
        $key = array_sum($matches[0]);
        $encoded_expired = base64_encode($encoded_phone . "." . $timestamp);
        $encoded_all = base64_encode($encoded_expired . "." . $key);

        return $encoded_all;
    }

    public function reset_link_check(Request $request, $token)
    {
        if (!$token) {
            return response()->json([
                'code' => 400,
                'message' => 'Token required.'
            ], 400);
        }

        $tokenDecoded = $this->decode_reset_link($token);

        $member = Member::where('phone', $tokenDecoded['phone'])->first();
        if (!$member) {
            return response()->json([
                'code' => 404,
                'message' => 'User not found.'
            ], 404);
        }

        $checkExpired = Carbon::parse($tokenDecoded['expired_at'])->timestamp;
        $timeNow = Carbon::now()->timestamp;

        if ($checkExpired < $timeNow) {
            return response()->json([
                'code' => 401,
                'message' => 'Token expired.'
            ], 401);
        }

        return response()->json([
            'code' => 200,
            'user' => $member
        ], 200);
    }

    public function reset_link(Request $request, $token)
    {
        if (!$token) {
            return response()->json([
                'code' => 400,
                'message' => 'Token required.'
            ], 400);
        }

        if (!$request->get('password')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `password` required.'
            ], 400);
        }

        $tokenDecoded = $this->decode_reset_link($token);

        $member = Member::where('phone', $tokenDecoded['phone'])->first();
        if (!$member) {
            return response()->json([
                'code' => 404,
                'message' => 'User not found.'
            ], 404);
        }

        if ($member->remember_token == NULL) {
            return response()->json([
                'code' => 403,
                'message' => 'Password already been reset by this link.'
            ], 403);
        }

        $checkExpired = Carbon::parse($tokenDecoded['expired_at'])->timestamp;
        $timeNow = Carbon::now()->timestamp;

        if ($checkExpired < $timeNow) {
            return response()->json([
                'code' => 401,
                'message' => 'Token expired.'
            ], 401);
        }

        $member->remember_token = null;
        $member->password = bcrypt($request->get('password'));
        $member->save();

        return response()->json([
            'code' => 200,
            'user' => $member
        ], 200);
    }

    public function profile(Request $request)
    {
        $member = JWTAuth::toUser($request->token);

        $member = Member::where('id', $member->id)->first();

        return fractal()
            ->item($member, new MemberTransformer())
            ->toArray();
    }

    public function profile_update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar' => 'mimes:jpeg,jpg,png'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $validator->errors()
            ], 400);
        }

        $member = JWTAuth::toUser($request->token);

        $member = Member::where('id', $member->id)->first();

        if ($request->get('new_password') && $request->get('old_password')) {
            $check = HelperController::checkAuth($member->id, $request->get('old_password'));
            if (!$check) {
                return response()->json([
                    'code' => 403,
                    'message' => 'Old password not match.'
                ], 403);
            }

            $member->password = bcrypt($request->get('new_password'));
        }

        if ($request->get('name')) {
            $member->name = $request->get('name');
        }

        if ($request->get('address')) {
            $member->address = $request->get('address');
        }

        // if ($request->get('email')) {
        //     $member->email = $request->get('email');
        // }

        if ($request->hasFile('avatar')) {

            if ($member->avatar) {
                Storage::delete($member->avatar);
            }

            $dateNow = Carbon::now();
            $path = $request->file('avatar')->storeAs('avatars', Str::random(10).''.$dateNow->timestamp.'.'.$request->file('avatar')->getClientOriginalExtension());
            $member->avatar = $path;
        }

        $member->save();

        return fractal()
            ->item($member, new MemberTransformer())
            ->toArray();
    }

    public function logout(Request $request)
    {
        $member = JWTAuth::toUser($request->token);

        $userToken = UserToken::where('user_id', $member->id)
                              ->where('token', $request->bearerToken())
                              ->first();
        if ($userToken) {
            $userToken->delete();
        }
        $delete = JWTAuth::invalidate($request->token);

        return response()->json([
            'code' => 200,
            'message' => $delete
        ], 200);
    }

    public function referal(Request $request)
    {
        $member = JWTAuth::toUser($request->token);

        if ($member->user_level_id == 1) {
            return response()->json([
                'code' => 403,
                'message' => 'Youre not premium member.'
            ], 403);
        }

        $referal = MemberBuyLevel::where('reference_by', $member->referal_id)->get();

        return fractal()
            ->collection($referal, new MemberBuyLevelTransformer())
            ->serializeWith(new EpmArraySerializer())
            ->toArray();
    }

    public function get_level_benefit(Request $request)
    {
        $member = JWTAuth::toUser($request->token);

        $level = UserLevel::select(
            '*',
            DB::raw($member->user_level_id . ' as levelMember')
        )->where('internal', 0)->get();
        $benefitLevel = BenefitUserLevels::orderBy('user_level_id')->groupBy('benefit_id')->get();

        $transform['level'] = fractal()
            ->collection($level, new UserLevelTransformer())
            ->serializeWith(new EpmArraySerializer())
            ->toArray();

        $transform['benefits'] = fractal()
            ->collection($benefitLevel, new BenefitLevelTransformer())
            ->serializeWith(new EpmArraySerializer())
            ->toArray();

        return $transform;
    }

    public function get_upgrade_referal(Request $request, $referral)
    {
        $member = JWTAuth::toUser($request->token);

        $referal = Member::where('referal_id', $referral)
            ->where('referal_id', '!=', $member->referal_id)
            ->first();

        if (!$referal) {
            return response()->json([
                'code' => 404,
                'message' => 'Referal not found.'
            ], 404);
        }

        if ($referal->user_level_id == 1) {
            return response()->json([
                'code' => 403,
                'message' => 'This refferal number is not premium account.'
            ], 403);
        }

        return fractal()
            ->item($referal, new UserLevelReferalTransformer())
            ->toArray();
    }

    public function level_upgrade(Request $request)
    {
        if (!$request->get('level')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `level` required.'
            ], 400);
        }

        if (!$request->get('password')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `password` required.'
            ], 400);
        }

        $member = JWTAuth::toUser($request->token);
        $member = Member::where('id', $member->id)->first();

        $check = HelperController::checkAuth($member->id, $request->get('password'));
        if (!$check) {
            return response()->json([
                'code' => 403,
                'message' => 'Password not match.'
            ], 403);
        }

        if ($request->get('level') != 2 && $request->get('level') != 4) {
            return response()->json([
                'code' => 404,
                'message' => 'Level not found.'
            ], 404);
        }

        // Check referal
        $epm01 = Member::where('email', 'admin@epaymentmobile.com')->first();
        $referalId = $epm01->referal_id;
        if ($request->get('referral')) {
            $referal = Member::where('referal_id', $request->get('referral'))
                ->where('referal_id', '!=', $member->referal_id)
                ->first();

            if (!$referal) {
                return response()->json([
                    'code' => 404,
                    'message' => 'Referral not found.'
                ], 404);
            }

            if ($referal->user_level_id == 1) {
                return response()->json([
                    'code' => 403,
                    'message' => 'This refferal number is not premium account.'
                ], 403);
            }

            $referalId = $referal->referal_id;
        }

        if ($member->user_level_id == 2 && $member->user_level_id == $request->get('level')) {
            return response()->json([
                'code' => 403,
                'message' => "You're already premium member."
            ], 403);
        }

        $level = UserLevel::where('id', $member->user_level_id)->first();
        if ($level->internal == 1) {
            return response()->json([
                'code' => 403,
                'message' => "Can't upgrade level."
            ], 403);
        }

        if ($level->upgradable == 0) {
            return response()->json([
                'code' => 403,
                'message' => 'You already reached maximum level.'
            ], 403);
        }

        $saldo = HelperController::member_saldo($member->id);

        // Check balance
        if ($member->user_level_id == 1) {
            if ($request->get('level') == 2) {
                if ($saldo < 100000) {
                    return response()->json([
                        'code' => 403,
                        'message' => 'Insufficient balance.'
                    ], 403);
                }
            } else if ($request->get('level') == 4) {
                if ($saldo < 200000) {
                    return response()->json([
                        'code' => 403,
                        'message' => 'Insufficient balance.'
                    ], 403);
                }
            }
        } else if ($member->user_level_id == 2) {
            if ($saldo < 100000) {
                return response()->json([
                    'code' => 403,
                    'message' => 'Insufficient balance.'
                ], 403);
            }
        } else {
            return response()->json([
                'code' => 500,
                'message' => 'Something went wrong.'
            ], 500);
        }

        if ($member->user_level_id == 1) {
            $buyLevel = new MemberBuyLevel();
            $buyLevel->level_price = 100000;
            $buyLevel->status_buy_level = true;
            $buyLevel->user_id = $member->id;
            $buyLevel->user_level_id = $request->get('level'); // Hard Code
            $buyLevel->reference_by = $referalId;
            $buyLevel->save();

            // Referal bonus
            MemberController::count_referal_bonus($member->id, $referalId);
        }

        if ($request->get('level') == 4) {
            $deposit = new Deposit();
            $deposit->user_id = $member->id;
            $deposit->deposit_type_id = 1; // Hard Code
            $deposit->nominal = 100000;
            $deposit->save();
        }

        $member->referal_id = str_replace('+62', 'EPM', $member->phone);
        $member->user_level_id = $request->get('level');
        $member->save();

        return fractal()
            ->item($member, new MemberTransformer())
            ->toArray();
    }

    public function count_referal_bonus($reference, $referalId)
    {
        // Level 1: 50.000
        $bonus = 50000;
        $memberReference0 = Member::where('id', $reference)->first();
        $memberReference1 = Member::where('referal_id', $referalId)->first();
        if ($memberReference1 == null) {
            return true;
        }
        $referal_tree = $memberReference0->id;
        $captionHistory = 'Bonus referal dari '.$memberReference0->name;
        HelperController::history_bonus($referal_tree, $bonus, $memberReference1->id, 2);
        $saldo = HelperController::member_saldo($memberReference1->id);
        HelperController::history_transaksi($memberReference1->id, $captionHistory, $bonus, 0, $saldo);
        // ==================================================

        // Level 2: 10.000
        $bonus = 10000;
        $reference2 = MemberBuyLevel::where('user_id', $memberReference1->id)->first();
        if ($reference2 == null || $reference2->reference_by == null) {
            return true;
        }
        $memberReference2 = Member::where('referal_id', $reference2->reference_by)->first();
        $referal_tree .= '-'.$memberReference1->id;
        $captionHistory = 'Bonus referal dari ' . $memberReference1->name . '->' . $memberReference0->name;
        HelperController::history_bonus($referal_tree, $bonus, $memberReference2->id, 2);
        $saldo = HelperController::member_saldo($memberReference2->id);
        HelperController::history_transaksi($memberReference2->id, $captionHistory, $bonus, 0, $saldo);
        // ==================================================

        // Level 3: 5.000
        $bonus = 5000;
        $reference3 = MemberBuyLevel::where('user_id', $memberReference2->id)->first();
        if ($reference3 == null || $reference3->reference_by == null) {
            return true;
        }
        $memberReference3 = Member::where('referal_id', $reference3->reference_by)->first();
        $referal_tree .= '-'.$memberReference2->id;
        $captionHistory = 'Bonus referal dari ' . $memberReference2->name . '->' . $memberReference1->name . '->' . $memberReference0->name;
        HelperController::history_bonus($referal_tree, $bonus, $memberReference3->id, 2);
        $saldo = HelperController::member_saldo($memberReference3->id);
        HelperController::history_transaksi($memberReference3->id, $captionHistory, $bonus, 0, $saldo);
        // ==================================================

        // Level 4: 3.000
        $bonus = 3000;
        $reference4 = MemberBuyLevel::where('user_id', $memberReference3->id)->first();
        if ($reference4 == null || $reference4->reference_by == null) {
            return true;
        }
        $memberReference4 = Member::where('referal_id', $reference4->reference_by)->first();
        $referal_tree .= '-'.$memberReference3->id;
        $captionHistory = 'Bonus referal dari ' . $memberReference3->name . '->' . $memberReference2->name . '->' . $memberReference1->name . '->' . $memberReference0->name;
        HelperController::history_bonus($referal_tree, $bonus, $memberReference4->id, 2);
        $saldo = HelperController::member_saldo($memberReference4->id);
        HelperController::history_transaksi($memberReference4->id, $captionHistory, $bonus, 0, $saldo);
        // ==================================================

        // Level 5: 2.000
        $bonus = 2000;
        $reference5 = MemberBuyLevel::where('user_id', $memberReference4->id)->first();
        if ($reference5 == null || $reference5->reference_by == null) {
            return true;
        }
        $memberReference5 = Member::where('referal_id', $reference5->reference_by)->first();
        $referal_tree .= '-'.$memberReference4->id;
        $captionHistory = 'Bonus referal dari ' . $memberReference4->name . '->' . $memberReference3->name . '->' . $memberReference2->name . '->' . $memberReference1->name . '->' . $memberReference0->name;
        HelperController::history_bonus($referal_tree, $bonus, $memberReference5->id, 2);
        $saldo = HelperController::member_saldo($memberReference5->id);
        HelperController::history_transaksi($memberReference5->id, $captionHistory, $bonus, 0, $saldo);
        // ==================================================

        return true;
    }

    public static function count_transaction_bonus($reference)
    {
        Log::channel('bonus')->info('Generate function at '.date('d-m-Y H:i:s'));

        // Level 1: 25
        $bonus = 25;
        $memberReference0 = Member::where('id', $reference)->first();
        $reference1 = MemberBuyLevel::where('user_id', $reference)->first();
        $memberReference1 = null;
        if ($reference1 != null) {
            if ($reference1->reference_by != null) {
                $memberReference1 = Member::where('referal_id', $reference1->reference_by)->first();
                if ($memberReference1 == null) {
                    return true;
                }
                $referal_tree = $memberReference0->id;
                $captionHistory = 'Bonus referal dari ' . $memberReference0->name;
                HelperController::history_bonus($referal_tree, $bonus, $memberReference1->id, 2);
                $saldo = HelperController::member_saldo($memberReference1->id);
                HelperController::history_transaksi($memberReference1->id, $captionHistory, $bonus, 0, $saldo);
            }
        }

        if (isset($memberReference1)) {
          if ($memberReference1 == null) {
            return true;
          }
        } else {
          return true;
        }

        fcm()
            ->toTopic($memberReference1->phone . '_android')// $topic must an string (topic name)
            ->notification([
                'title' => 'Bonus Transaksi',
                'body' => $captionHistory,
            ])
            ->send();
        // ==================================================

        // Level 2: 15
        $bonus = 15;
        $reference2 = MemberBuyLevel::where('user_id', $memberReference1->id)->first();
        if ($reference2 == null || $reference2->reference_by == null) {
            return true;
        }
        $memberReference2 = Member::where('referal_id', $reference2->reference_by)->first();
        $referal_tree .= '-'.$memberReference1->id;
        $captionHistory = 'Bonus referal dari ' . $memberReference0->name . '->' . $memberReference1->name;
        HelperController::history_bonus($referal_tree, $bonus, $memberReference2->id, 2);
        $saldo = HelperController::member_saldo($memberReference2->id);
        HelperController::history_transaksi($memberReference2->id, $captionHistory, $bonus, 0, $saldo);

        fcm()
            ->toTopic($memberReference2->phone . '_android')// $topic must an string (topic name)
            ->notification([
                'title' => 'Bonus Transaksi',
                'body' => $captionHistory,
            ])
            ->send();
        // ==================================================

        // Level 3: 5
        $bonus = 5;
        $reference3 = MemberBuyLevel::where('user_id', $memberReference2->id)->first();
        if ($reference3 == null || $reference3->reference_by == null) {
            return true;
        }
        $memberReference3 = Member::where('referal_id', $reference3->reference_by)->first();
        $referal_tree .= '-'.$memberReference2->id;
        $captionHistory = 'Bonus referal dari ' . $memberReference0->name . '->' . $memberReference1->name . '->' . $memberReference2->name;
        HelperController::history_bonus($referal_tree, $bonus, $memberReference3->id, 2);
        $saldo = HelperController::member_saldo($memberReference3->id);
        HelperController::history_transaksi($memberReference3->id, $captionHistory, $bonus, 0, $saldo);

        fcm()
            ->toTopic($memberReference3->phone . '_android')// $topic must an string (topic name)
            ->notification([
                'title' => 'Bonus Transaksi',
                'body' => $captionHistory,
            ])
            ->send();
        // ==================================================

        // Level 4: 15
        $bonus = 15;
        $reference4 = MemberBuyLevel::where('user_id', $memberReference3->id)->first();
        if ($reference4 == null || $reference4->reference_by == null) {
            return true;
        }
        $memberReference4 = Member::where('referal_id', $reference4->reference_by)->first();
        $referal_tree .= '-'.$memberReference3->id;
        $captionHistory = 'Bonus referal dari ' . $memberReference0->name . '->' . $memberReference1->name . '->' . $memberReference2->name . '->' . $memberReference3->name;
        HelperController::history_bonus($referal_tree, $bonus, $memberReference4->id, 2);
        $saldo = HelperController::member_saldo($memberReference4->id);
        HelperController::history_transaksi($memberReference4->id, $captionHistory, $bonus, 0, $saldo);

        fcm()
            ->toTopic($memberReference4->phone . '_android')// $topic must an string (topic name)
            ->notification([
                'title' => 'Bonus Transaksi',
                'body' => $captionHistory,
            ])
            ->send();
        // ==================================================

        // Level 5: 25
        $bonus = 25;
        $reference5 = MemberBuyLevel::where('user_id', $memberReference4->id)->first();
        if ($reference5 == null || $reference5->reference_by == null) {
            return true;
        }
        $memberReference5 = Member::where('referal_id', $reference5->reference_by)->first();
        $referal_tree .= '-'.$memberReference4->id;
        $captionHistory = 'Bonus referal dari ' . $memberReference0->name . '->' . $memberReference1->name . '->' . $memberReference2->name . '->' . $memberReference3->name . '->' . $memberReference4->name;
        HelperController::history_bonus($referal_tree, $bonus, $memberReference5->id, 2);
        $saldo = HelperController::member_saldo($memberReference5->id);
        HelperController::history_transaksi($memberReference5->id, $captionHistory, $bonus, 0, $saldo);

        fcm()
            ->toTopic($memberReference5->phone . '_android')// $topic must an string (topic name)
            ->notification([
                'title' => 'Bonus Transaksi',
                'body' => $captionHistory,
            ])
            ->send();
        // ==================================================

        return true;
    }

    public function block_member(Request $request)
    {
        $member = Member::where('id', $request->get('id'))->first();
        if (!$member) {
            return response()->json([
                'code' => 404,
                'message' => 'Member not found'
            ], 404);
        }

        $userToken = UserToken::where('user_id', $member->id)->get();
        if (!$userToken) {
            return response()->json([
                'code' => 404,
                'message' => 'Token not found'
            ], 404);
        }

        foreach ($userToken as $token) {
            $deleteToken = UserToken::where('id', $token->id)->first();
            if ($deleteToken) {
                $deleteToken->delete();
            }

            $delete = JWTAuth::invalidate($token->token);
        }

        return response()->json([
            'code' => 200,
            'message' => 'Success Non Active Member'
        ], 200);
    }

    public function logoutAll(Request $request)
    {
        $userToken = UserToken::get();
        foreach ($userToken as $token) {
          $auth = $token->token;
          JWTAuth::invalidate($auth);
          $token->delete();
        }

        return response()->json([
            'code' => 200,
        ], 200);
    }

    public function downline(Request $request, $id = NULL)
    {
        $member = JWTAuth::toUser($request->token);
        if (!$member) {
            return response()->json([
                'code' => 404,
                'message' => 'User not found'
            ], 404);
        }

        $phone = str_replace('+62', 'EPM', $member->phone);
        $referal = MemberBuyLevel::where('reference_by', $phone)->get();

        if ($id) {

            $memberChild = Member::where('id', $id)->first();
            if (!$memberChild) {
                return response()->json([
                    'code' => 404,
                    'message' => 'Referal not found'
                ], 404);
            }
            $phoneChild = str_replace('+62', 'EPM', $memberChild->phone);

            if ($phone != $phoneChild) {
                if ($id && !HelperController::checkDownline($phone, $id)) {
                    return response()->json([
                        'code' => 404,
                        'message' => 'User not found'
                    ], 404);
                }
            }
            $referal = MemberBuyLevel::where('reference_by', $phoneChild)->get();
        }

        return fractal()
            ->collection($referal, new MemberBuyLevelTransformer())
            ->serializeWith(new EpmArraySerializer())
            ->toArray();
    }
}
