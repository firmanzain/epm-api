<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTFactory, JWTAuth;
use DB;
// Include all required models
use App\Models\v2\TopupMember, App\Models\v2\MemberBuyLevel, App\Models\v2\MemberBuyProduct,
    App\Models\v2\MemberBuyPpob, App\Models\v2\TransferSaldo, App\Models\v2\MemberWithdraw,
    App\Models\v2\MemberBonus, App\Models\v2\HistoryTransaksi, App\Models\v2\Deposit,
    App\Models\v2\SystemConfig, App\Models\v2\Member;
use Illuminate\Support\Facades\Auth;

class HelperController extends Controller
{
    public static function format_phone($phone)
    {
       $phone = trim(str_replace(' ', '', $phone));
       if (substr($phone, 0, 1) == '0') {
           $phone = '62' . substr($phone, 1);
       } else if (substr($phone, 0, 1) == '+') {
           $phone = '' . substr($phone, 1);
       }
       return $phone;
    }

    public static function member_saldo($id)
    {
        $topup = TopupMember::where('user_id', $id)
                            ->where('topup_status', 1)
                            ->sum('topup_saldo');

        $withdraw = MemberWithdraw::where('user_id', $id)
                                  ->where('is_approved', '<>', 0)
                                  ->where('payment_type', 1)
                                  ->select(DB::raw('sum(admin_fee + amount) as total'))
                                  ->first()->total;

        $sendSaldo = TransferSaldo::where('sender_id', $id)
                                  ->sum('nominal');

        $receiveSaldo = TransferSaldo::where('receiver_id', $id)
                                     ->sum('nominal');

        $bonus = MemberBonus::where('user_id', $id)
                            ->where('payment_type', 1)
                            ->sum('nominal_bonus');

        $deposit = Deposit::where('user_id', $id)
                                 ->sum('nominal');

        $buyLevel = MemberBuyLevel::where('user_id', $id)
                                  ->sum('level_price');

        $buyProduct = MemberBuyProduct::where('user_id', $id)
                                      ->where('status_buy_product', '<>', 0)
                                      ->where('payment_type', 1)
                                      ->sum('buy_product_price');

        $buyPpob = MemberBuyPpob::where('user_id', $id)
                                ->where('status_buy_ppob', '<>',  0)
                                ->where('payment_type', 1)
                                ->sum('total');

        $saldo = ($topup + $receiveSaldo + $bonus) -
                 ($buyLevel + $buyProduct + $buyPpob + $sendSaldo + $withdraw + $deposit);

        if ($saldo < 0) {
            $saldo = 0;
        }

        return intval($saldo);
    }

    public static function member_point($id)
    {
        $withdraw = MemberWithdraw::where('user_id', $id)
                                  ->where('is_approved', '<>', 0)
                                  ->where('payment_type', 2)
                                  ->select(DB::raw('sum(admin_fee + amount) as total'))
                                  ->first()->total;

        $bonus = MemberBonus::where('user_id', $id)
                            ->where('payment_type', 2)
                            ->sum('nominal_bonus');

        $buyProduct = MemberBuyProduct::where('user_id', $id)
                                      ->where('status_buy_product', '<>', 0)
                                      ->where('payment_type', 2)
                                      ->sum('buy_product_price');

        $buyPpob = MemberBuyPpob::where('user_id', $id)
                                ->where('status_buy_ppob', '<>',  0)
                                ->where('payment_type', 2)
                                ->sum('total');

        $saldo = $bonus - ($buyProduct + $buyPpob + $withdraw);

        if ($saldo < 0) {
           $saldo = 0;
        }

        return intval($saldo);
    }

    public static function history_transaksi($user_id, $keterangan, $debit, $credit, $saldo)
    {
        $historyTransaksi = new HistoryTransaksi();
        $historyTransaksi->user_id = $user_id;
        $historyTransaksi->keterangan = $keterangan;
        $historyTransaksi->debit = $debit;
        $historyTransaksi->credit = $credit;
        $historyTransaksi->saldo_akhir = $saldo;
        $historyTransaksi->save();
    }

    public static function history_bonus($referal_tree, $nominal, $user_id, $payment = null)
    {
        $memberBonus = new MemberBonus();
        $memberBonus->history_referal_tree = $referal_tree;
        $memberBonus->nominal_bonus = $nominal;
        $memberBonus->user_id = $user_id;
        if ($payment) {
            $memberBonus->payment_type = $payment;
        }
        $memberBonus->save();
    }

    public static function get_system_config($code)
    {
        $conf = SystemConfig::where('code', $code)->first();

        if ($conf) {
            return $conf->value;
        } else {
            return null;
        }
    }

    public static function checkAuth($id, $password)
    {
        $check = Auth::attempt(['id' => $id, 'password' => $password]);
        return $check;
    }

    public static function pushNotif($phone)
    {
        $phone = HelperController::format_phone($phone);

        fcm()
            ->toTopic($phone.'_android') // $topic must an string (topic name)
            ->notification([
                'title' => 'Test FCM',
                'body' => 'This is a test of FCM',
            ])
            ->send();
    }

    public function version_android(Request $request)
    {
        $andoridV = 0;

        $android = SystemConfig::where('code', 'ANDROID_VERSION')->first();
        if ($android) {
            $andoridV = (int) $android->value;
        }

        $version = array(
            'version' => $andoridV,
        );

        return response()->json($version);
    }

    public static function checkDownline($ref, $id)
    {
        $member = MemberBuyLevel::select('user_id', 'reference_by')
                                  ->where('user_id', $id)->first();

        if ($member) {
            if ($member->reference_by == NULL || strtoupper($member->reference_by) == 'EPM001') {
                return false;
            } else if (strtoupper($member->reference_by) == $ref) {
                return true;
            }

            $phone = str_replace('EPM', '+62', strtoupper($member->reference_by));
            $parent = Member::select('id')->where('phone', $phone)->first();
            return HelperController::checkDownline($ref, $parent->id);
        }
        return false;
    }

}
