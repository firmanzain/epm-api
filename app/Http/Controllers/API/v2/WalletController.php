<?php

namespace App\Http\Controllers\API\v2;

use App\Models\v2\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTFactory, JWTAuth, DB, Carbon\Carbon, Validator;
use
    App\Models\v2\TopupMember, App\Models\v2\MemberWithdraw, App\Models\v2\TransferSaldo,
    App\Models\v2\MemberBonus, App\Models\v2\Deposit, App\Models\v2\TransactionBank,
    App\Models\v2\UserBankAccount, App\Models\v2\MemberBuyLevel, App\Models\v2\MemberBuyProduct,
    App\Models\v2\MemberBuyPpob, App\Models\v2\Bank;
use
    App\Transformers\Serializer\EpmArraySerializer, App\Transformers\WalletTransformer, App\Transformers\TransactionBankTransformer, App\Transformers\TopupMemberTransformer,
    App\Transformers\WalletTransferRecipientTransformer, App\Transformers\WalletTransferResultTransformer,
    App\Transformers\MemberWithdrawTransformer, App\Transformers\BankTransformer,
    App\Transformers\TransferSaldoHistory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Illuminate\Support\Facades\Storage;
use App\Models\v2\Test;

class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_transfer_recipient(Request $request, $referral)
    {
        $member = JWTAuth::toUser($request->token);

        $recipient = Member::where('referal_id', $referral)
            ->where('referal_id', '!=', $member->referal_id)
            ->first();

        if (!$recipient) {
            return response()->json([
                'code' => 404,
                'message' => 'Recipient not found.'
            ], 404);
        }

        if ($member->user_level_id == 1) {
            return response()->json([
                'code' => 403,
                'message' => 'Youre not premium member.'
            ], 403);
        }

        return fractal()
            ->item($recipient, new WalletTransferRecipientTransformer())
            ->toArray();
    }

    public function get_balance_history(Request $request)
    {
        $member = JWTAuth::toUser($request->token);

        $topup = TopupMember::select(
            'id',
            DB::raw('"TOPUP" as transaction_id'),
            'topup_saldo as amount',
            DB::raw('0 as fee'),
            DB::raw('1 as category'),
            DB::raw('"Debit" as type'),
            'topup_status as status',
            'transfer_to as bank_id',
            DB::raw('0 as reference_id'),
            DB::raw('0 as origin_id'),
            DB::raw('"-" as receiptUrl'),
            'created_at'
        )->where('user_id', $member->id)
            ->where(function ($query) {
                $query->where('topup_status', '!=', 0)
                    ->orWhere('topup_status', '=', '0')
                    ->where('created_at', '<', Carbon::now()->subHours(10)->toDateTimeString());
            });

        $withdraw = MemberWithdraw::select(
            'id',
            DB::raw('"WITHDRAW" as transaction_id'),
            'amount as amount',
            'admin_fee as fee',
            DB::raw('2 as category'),
            DB::raw('"Kredit" as type'),
            'is_approved as status',
            'user_bank_account_id as bank_id',
            DB::raw('0 as reference_id'),
            DB::raw('0 as origin_id'),
            DB::raw('"-" as receiptUrl'),
            'created_at'
        )->where('user_id', $member->id)
            ->where('is_approved', '!=', 2)
            ->where('payment_type', 1);

        $sendSaldo = TransferSaldo::select(
            'id',
            DB::raw('"TRANSFER" as transaction_id'),
            'nominal as amount',
            DB::raw('0 as fee'),
            DB::raw('3 as category'),
            DB::raw('"Kredit" as type'),
            DB::raw('1 as status'),
            DB::raw('0 as bank_id'),
            DB::raw('0 as reference_id'),
            'receiver_id as origin_id',
            DB::raw('"-" as receiptUrl'),
            'created_at'
        )->where('sender_id', $member->id);

        $receiveSaldo = TransferSaldo::select(
            'id',
            DB::raw('"TRANSFER" as transaction_id'),
            'nominal as amount',
            DB::raw('0 as fee'),
            DB::raw('4 as category'),
            DB::raw('"Debit" as type'),
            DB::raw('1 as status'),
            DB::raw('0 as bank_id'),
            DB::raw('0 as reference_id'),
            'sender_id as origin_id',
            DB::raw('"-" as receiptUrl'),
            'created_at'
        )->where('receiver_id', $member->id);

        $bonus = MemberBonus::select(
            'id',
            DB::raw('"BONUS" as transaction_id'),
            'nominal_bonus as amount',
            DB::raw('0 as fee'),
            DB::raw('5 as category'),
            DB::raw('"Debit" as type'),
            DB::raw('1 as status'),
            DB::raw('0 as bank_id'),
            DB::raw('0 as reference_id'),
            'history_referal_tree as origin_id',
            DB::raw('"-" as receiptUrl'),
            'created_at'
        )->where('user_id', $member->id)
            ->where('payment_type', 1);

        $deposit = Deposit::select(
            'id',
            DB::raw('"DEPOSIT" as transaction_id'),
            'nominal as amount',
            DB::raw('0 as fee'),
            DB::raw('6 as category'),
            DB::raw('"Kredit" as type'),
            DB::raw('1 as status'),
            DB::raw('0 as bank_id'),
            DB::raw('0 as reference_id'),
            DB::raw('0 as origin_id'),
            DB::raw('"-" as receiptUrl'),
            'created_at'
        )->where('user_id', $member->id);

        $buyLevel = MemberBuyLevel::select(
            'id',
            DB::raw('"BUYLEVEL" as transaction_id'),
            'level_price as amount',
            DB::raw('0 as fee'),
            DB::raw('7 as category'),
            DB::raw('"Kredit" as type'),
            'status_buy_level as status',
            DB::raw('0 as bank_id'),
            'reference_by as reference_id',
            DB::raw('0 as origin_id'),
            DB::raw('"-" as receiptUrl'),
            'created_at'
        )
            ->where('user_id', $member->id);

        $buyProduct = MemberBuyProduct::select(
            'id',
            'transaction_id',
            'buy_product_price as amount',
            DB::raw('0 as fee'),
            DB::raw('8 as category'),
            DB::raw('"Kredit" as type'),
            'status_buy_product as status',
            DB::raw('0 as bank_id'),
            'msisdn as reference_id',
            'product_id as origin_id',
            'receiptUrl',
            'created_at'
        )
            ->where('user_id', $member->id)
            ->where('payment_type', 1);

        $paginator = MemberBuyPpob::select(
            'id',
            'ppob_id as transaction_id',
            'tagihan as amount',
            'admin as fee',
            DB::raw('9 as category'),
            DB::raw('"Kredit" as type'),
            'status_buy_ppob as status',
            DB::raw('0 as bank_id'),
            'msisdn as reference_id',
            'product_id as origin_id',
            'receiptUrl',
            'created_at'
        )
            ->where('user_id', $member->id)
            ->where('payment_type', 1)
            ->unionAll($topup)
            ->unionAll($withdraw)
            ->unionAll($sendSaldo)
            ->unionAll($receiveSaldo)
            ->unionAll($bonus)
            ->unionAll($deposit)
            ->unionAll($buyLevel)
            ->unionAll($buyProduct)
            ->orderBy('created_at', 'desc')
            ->paginate(15);

        $buyPpob = $paginator->getCollection();

        return fractal()
            ->collection($buyPpob, new WalletTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();
    }

    public function get_point_history(Request $request)
    {
        $member = JWTAuth::toUser($request->token);

        $withdraw = MemberWithdraw::select(
            'id',
            DB::raw('"WITHDRAW" as transaction_id'),
            'amount as amount',
            'admin_fee as fee',
            DB::raw('2 as category'),
            DB::raw('"Kredit" as type'),
            'is_approved as status',
            'user_bank_account_id as bank_id',
            DB::raw('0 as reference_id'),
            DB::raw('0 as origin_id'),
            DB::raw('"-" as receiptUrl'),
            'created_at'
        )->where('user_id', $member->id)
            ->where('is_approved', '!=', 2)
            ->where('payment_type', 1);

        $bonus = MemberBonus::select(
            'id',
            DB::raw('"BONUS" as transaction_id'),
            'nominal_bonus as amount',
            DB::raw('0 as fee'),
            DB::raw('5 as category'),
            DB::raw('"Debit" as type'),
            DB::raw('1 as status'),
            DB::raw('0 as bank_id'),
            DB::raw('0 as reference_id'),
            'history_referal_tree as origin_id',
            DB::raw('"-" as receiptUrl'),
            'created_at'
        )->where('user_id', $member->id)
            ->where('payment_type', 2);

        $buyProduct = MemberBuyProduct::select(
            'id',
            'transaction_id',
            'buy_product_price as amount',
            DB::raw('0 as fee'),
            DB::raw('8 as category'),
            DB::raw('"Kredit" as type'),
            'status_buy_product as status',
            DB::raw('0 as bank_id'),
            'msisdn as reference_id',
            'product_id as origin_id',
            'receiptUrl',
            'created_at'
        )
            ->where('user_id', $member->id)
            ->where('payment_type', 2);

        $paginator = MemberBuyPpob::select(
            'id',
            'ppob_id as transaction_id',
            'tagihan as amount',
            'admin as fee',
            DB::raw('9 as category'),
            DB::raw('"Kredit" as type'),
            'status_buy_ppob as status',
            DB::raw('0 as bank_id'),
            'msisdn as reference_id',
            'product_id as origin_id',
            'receiptUrl',
            'created_at'
        )
            ->where('user_id', $member->id)
            ->where('payment_type', 2)
            ->unionAll($withdraw)
            ->unionAll($bonus)
            ->unionAll($buyProduct)
            ->orderBy('created_at', 'desc')
            ->paginate(15);

        $buyPpob = $paginator->getCollection();

        return fractal()
            ->collection($buyPpob, new WalletTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();
    }

    public function get_pending_history(Request $request)
    {
        $member = JWTAuth::toUser($request->token);

        $topup = TopupMember::select(
            'id',
            DB::raw('"TOPUP" as transaction_id'),
            'topup_saldo as amount',
            DB::raw('0 as fee'),
            DB::raw('1 as category'),
            DB::raw('"Debit" as type'),
            'topup_status as status',
            'transfer_to as bank_id',
            'attachment as reference_id',
            DB::raw('0 as origin_id'),
            DB::raw('"-" as receiptUrl'),
            'created_at'
        )->where('user_id', $member->id)
            ->where('created_at', '>', Carbon::now()->subHours(10)->toDateTimeString())
            ->where('topup_status', 0);

        $paginator = MemberWithdraw::select(
            'id',
            DB::raw('"WITHDRAW" as transaction_id'),
            'amount as amount',
            'admin_fee as fee',
            DB::raw('2 as category'),
            DB::raw('"Kredit" as type'),
            'is_approved as status',
            'user_bank_account_id as bank_id',
            DB::raw('0 as reference_id'),
            DB::raw('0 as origin_id'),
            DB::raw('"-" as receiptUrl'),
            'created_at'
        )->where('user_id', $member->id)
            ->where('is_approved', 2)
            ->unionAll($topup)
            ->orderBy('created_at', 'desc')
            ->paginate(15);

        $withdraw = $paginator->getCollection();

        return fractal()
            ->collection($withdraw, new WalletTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();
    }

    public function topup_methods(Request $request)
    {
        $data = TransactionBank::where('deleted_at', NULL)
            ->orderBy('created_at', 'asc')
            ->get();

        return fractal()
            ->collection($data, new TransactionBankTransformer())
            ->serializeWith(new EpmArraySerializer())
            ->toArray();
    }

    public function topup(Request $request)
    {
        if (!$request->get('amount')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `amount` required.'
            ], 400);
        }

        if (!$request->get('id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` required.'
            ], 400);
        }

        $amount = abs($request->get('amount'));

        if ($amount < 50000) {
            return response()->json([
                'code' => 403,
                'message' => 'Topup amount at least Rp. 50.000'
            ], 403);
        }

        $member = JWTAuth::toUser($request->token);

        $topUp = new TopupMember();
        $topUp->user_id = $member->id;
        $topUp->topup_saldo = $amount;
        $topUp->topup_status = 0; // Pending
        $topUp->attachment = NULL;
        $topUp->topup_reference_number = 0;
        $topUp->transfer_to = $request->get('id');
        $topUp->save();

        return fractal()
            ->item($topUp, new TopupMemberTransformer())
            ->toArray();
    }

    public function topup_confirmation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'attachment' => 'mimes:jpeg,jpg,png'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $validator->errors()
            ], 400);
        }

        if (!$request->get('id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` required.'
            ], 400);
        }

        if (!$request->hasFile('attachment')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `attachment` required.'
            ], 400);
        }

        $member = JWTAuth::toUser($request->token);

        $topUp = TopupMember::where('id', $request->get('id'))
            ->where('topup_status', 0)
            ->where('user_id', $member->id)
            ->first();
        if (!$topUp) {
            return response()->json([
                'code' => 404,
                'message' => 'Reference topup not found or has been followed up.'
            ], 404);
        }

        $dateNow = Carbon::now();
        $created = Carbon::parse($topUp->created_at);
        if ($created->diffInHours($dateNow) > 10) {
            return response()->json([
                'code' => 404,
                'message' => 'Reference topup already expired.'
            ], 404);
        }

        if ($topUp->attachment) {
            Storage::delete($topUp->attachment);
        }

        $path = '';
        if ($request->hasFile('attachment')) {
            $path = $request->file('attachment')->storeAs('topup-attachment', Str::random(10) . '.' . $request->file('attachment')->getClientOriginalExtension());
        }

        $topUp->attachment = $path;
        $topUp->update();

        return fractal()
            ->item($topUp, new TopupMemberTransformer())
            ->toArray();
    }

    public function transfer(Request $request)
    {
        if (!$request->get('id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` required.'
            ], 400);
        }

        if (!$request->get('amount')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `amount` required.'
            ], 400);
        }

        if (!$request->get('password')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `password` required.'
            ], 400);
        }

        $member = JWTAuth::toUser($request->token);

        $recipient = Member::where('referal_id', $request->get('id'))->first();
        if (!$recipient) {
            return response()->json([
                'code' => 404,
                'message' => 'Recipient not found.'
            ], 404);
        }

        $amount = abs($request->get('amount'));

        if ($member->user_level_id == 1) {
            return response()->json([
                'code' => 403,
                'message' => "You can't transfer, Must be premium level."
            ], 403);
        }

        $check = HelperController::checkAuth($member->id, $request->get('password'));
        if (!$check) {
            return response()->json([
                'code' => 403,
                'message' => 'Password not match.'
            ], 403);
        }

        $saldo = HelperController::member_saldo($member->id);
        if ($saldo < $amount) {
            return response()->json([
                'code' => 403,
                'message' => 'Insufficient balance'
            ], 403);
        }

        $transfer = new TransferSaldo();
        $transfer->sender_id = $member->id;
        $transfer->receiver_id = $recipient->id;
        $transfer->nominal = $amount;
        $transfer->save();

        $phone = HelperController::format_phone($recipient->phone);
        fcm()
            ->toTopic($phone.'_android') // $topic must an string (topic name)
            ->notification([
                'title' => 'Transfer saldo',
                'body' => $member->name.' mengirimkan dana Rp.'.number_format($amount, 0, ",", "."),
            ])
            ->send();

        return fractal()
            ->item($transfer, new WalletTransferResultTransformer())
            ->toArray();
    }

    public function withdraw(Request $request)
    {
        if (!$request->get('id')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `id` required.'
            ], 400);
        }

        if (!$request->get('amount')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `amount` required.'
            ], 400);
        }

        if (!$request->get('password')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `password` required.'
            ], 400);
        }

        if (!$request->get('source')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `source` is required.'
            ], 400);
        }

        $bank = UserBankAccount::where('id', $request->get('id'))->first();
        if (!$bank) {
            return response()->json([
                'code' => 404,
                'message' => 'Bank not found.'
            ], 404);
        }

        $amount = abs($request->get('amount'));

        $member = JWTAuth::toUser($request->token);

        if ($member->user_level_id == 1) {
            return response()->json([
                'code' => 403,
                'message' => "You can't transfer, Must be premium level."
            ], 403);
        }

        $check = HelperController::checkAuth($member->id, $request->get('password'));
        if (!$check) {
            return response()->json([
                'code' => 403,
                'message' => 'Password not match.'
            ], 403);
        }

        if ($request->get('source') != 1 && $request->get('source') != 2) {
            return response()->json([
                'code' => 403,
                'message' => 'Payment method not valid.'
            ], 403);
        }

        $admin_fee = HelperController::get_system_config('WITHDRAW_ADMIN_FEE');
        if ($request->get('source') == 2) {
            $admin_fee = HelperController::get_system_config('WITHDRAW_ADMIN_FEE_POINT');
        }

        $saldo = HelperController::member_saldo($member->id);
        if ($request->get('source') == 2) {
            $saldo = HelperController::member_point($member->id);
        }

        if ($saldo < $amount) {
            return response()->json([
                'code' => 403,
                'message' => 'Insufficient balance'
            ], 403);
        }

        if (($saldo - $amount - $admin_fee) < 50000) {
            return response()->json([
                'code' => 403,
                'message' => 'Minimum remaining balance must be Rp 50.000'
            ], 403);
        }

        if ($amount < 100000) {
            return response()->json([
                'code' => 403,
                'message' => 'Withdrawals must be at least Rp 100.000'
            ], 403);
        }

        $dateNow = (array)Carbon::now();
        $date = date('D', strtotime($dateNow['date']));

        if ($request->get('source') == 2) {
            if ($date != 'Tue' || $date != 'Thu') {
                return response()->json([
                    'code' => 403,
                    'message' => 'Withdrawals can only be made on Tuesday and Thursday'
                ], 403);
            }
        }

        $time = Carbon::now();
        if ($request->get('source') == 1) {
            $start = Carbon::create($time->year, $time->month, $time->day, 8, 0, 0); //set time to 08:00
            $end = Carbon::create($time->year, $time->month, $time->day, 20, 0, 0); //set time to 20:00

            if (!$time->between($start, $end, true)) {
                return response()->json([
                    'code' => 403,
                    'message' => 'Withdrawals can only be made on 08:00 - 20:00'
                ], 403);
            }
        } else if ($request->get('source') == 2) {
            $start = Carbon::create($time->year, $time->month, $time->day, 8, 0, 0); //set time to 08:00
            $end = Carbon::create($time->year, $time->month, $time->day, 16, 0, 0); //set time to 16:00

            if (!$time->between($start, $end, true)) {
                return response()->json([
                    'code' => 403,
                    'message' => 'Withdrawals can only be made on 08:00 - 16:00'
                ], 403);
            }
        }

        $check = MemberWithdraw::whereDay('created_at', date('d'))
            ->whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->where('is_approved', '!=', 0)
            ->first();

        if ($check) {
            return response()->json([
                'code' => 403,
                'message' => 'Withdrawal can only be done once a day'
            ], 403);
        }

        $withdraw = new MemberWithdraw();
        $withdraw->user_id = $member->id;
        $withdraw->user_bank_account_id = $request->get('id');
        $withdraw->amount = $amount;
        $withdraw->admin_fee = $admin_fee;
        $withdraw->is_approved = 2;
        $withdraw->payment_type = $request->get('source');
        $withdraw->save();

        return fractal()
            ->item($withdraw, new MemberWithdrawTransformer())
            ->toArray();
    }

    public function get_bank(Request $request)
    {
        $data = Bank::orderBy('name', 'asc')
            ->get();

        return fractal()
            ->collection($data, new BankTransformer())
            ->serializeWith(new EpmArraySerializer())
            ->toArray();
    }

    public function get_transfer_history(Request $request)
    {
        $member = JWTAuth::toUser($request->token);

        $paginator = TransferSaldo::where('sender_id', $member->id)
            ->where('receiver_id', '!=', $member->id)
            ->groupBy('receiver_id')
            ->orderBy('created_at', 'desc')
            ->paginate(5);

        $history = $paginator->getCollection();

        return fractal()
            ->collection($history, new TransferSaldoHistory())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();
    }

    public function test(Request $request)
    {
        $members = Member::get();
        foreach ($members as $member) {
            $saldo = HelperController::member_saldo($member->id);

            $test = new Test();
            $test->id = $member->id;
            $test->nama = $member->name;
            $test->saldo = $saldo;
            $test->save();
        }
    }

}
