<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use JWTAuth;

use Closure;

class RefreshJwtToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['error' => 'auth_error'], 101);
            }
        } catch(TokenExpiredException $ex) {
            try{
                $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                $user = JWTAuth::setToken($refreshed)->toUser();
                header('Authorization: Bearer ' . $refreshed);
            } catch(JWTException $ex) {
                return response()->json(['error' => 'token_not_refreshable'], 103);
            }
        } catch(JWTException $ex) {
            return response()->json(['error' => 'auth_error'], 101);
        }

        Auth::login($user, false);

        return $next($request);
    }
}
