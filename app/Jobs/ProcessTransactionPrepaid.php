<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\v2\Product, App\Models\v2\MemberBuyProduct;
use App\Http\Controllers\API\v2\vendor\VendorController;
use App\Models\v2\SendapiSupplier, App\Models\v2\Member;
use App\Http\Controllers\API\v2\HelperController;
use Illuminate\Support\Facades\Log;

class ProcessTransactionPrepaid implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $buyProduct;
    public $timeout = 300;
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MemberBuyProduct $buyProduct)
    {
        $this->buyProduct = $buyProduct;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $buyProduct = $this->buyProduct;

        $product = Product::where('id', $buyProduct->product_id)
            ->where('status_active', 1)
            ->first();

        $suppProducts = $product->SupplierProduct()->where('status_active', 1)
            ->orderBy('supplier_price', 'asc')->get();

        $status = 1;
        foreach ($suppProducts as $suppProduct) {
            $supplier = $suppProduct->suppliers()->first();

            // UPDATE MEMBER BUY PRODUCT
            $buyProduct->supplier_price = $suppProduct->supplier_price;
            $buyProduct->supplier_id = $supplier->id;
            $buyProduct->status_buy_product = 2; // Set status as pending
            $buyProduct->save();
            // END UPDATE MEMBER BUY PRODUCT

            $class = $supplier->controller_name;
            $instance = new $class();

            if (!($instance instanceof VendorController)) {
                $buyProduct->status_buy_product = 0;
                $buyProduct->save();
                continue;

//                return response()->json([
//                    'code' => 500,
//                    'message' => 'Please wait. We will back soon',
//                    'developerMessage' => 'Must implement VendorController class.'
//                ], 500);
            }

            $result = $instance::buy_prepaid($buyProduct->transaction_id, $suppProduct->voucher_code, $buyProduct->msisdn, $suppProduct->package_code);
            // $result = 0: Success/Pending, 1: Error Transaction
            // END INSERT SEND API SUPPLIER

            $status = $result['status'];
            if ($result['status'] == 0) {
                break;
            }
        }

        $check = MemberBuyProduct::where('id', $buyProduct->id)->first();
        if ($check) {
            $member = Member::where('id', $check->user_id)->first();
            $phone = HelperController::format_phone($member->phone);

            $statusName = 'Pending';
            if ($check->status_buy_product == 0 || $check->status_buy_product == 1) {
                if ($check->status_buy_product == 0) {
                    $statusName = 'Gagal';
                } else if ($check->status_buy_product == 1) {
                    $statusName = 'Sukses';
                }
                fcm()
                    ->toTopic($phone . '_android')// $topic must an string (topic name)
                    ->notification([
                        'title' => 'Info Transaksi',
                        // 'body' => 'Transaksi pembelian ' . $product->name . ' '.$statusName,
                        'body' => 'Transaksi pembelian ' . $product->name . ' telah selesai, lihat history untuk mengetahui status transaksi',
                    ])
                    ->send();
            }
        }
    }

    /**
     * The job failed to process.
     *
     * @param  Exception $exception
     * @return void
     */
    public function failed($exception)
    {
        $buyProduct = $this->buyProduct;
        $buyProduct->status_buy_product = 0;
        $buyProduct->save();

        Log::channel('exceptionprepaid')->info('Exception '.$buyProduct->id.' '.$exception);

        $product = Product::where('id', $buyProduct->product_id)->first();
        $member = Member::where('id', $buyProduct->user_id)->first();
        $phone = HelperController::format_phone($member->phone);
        // fcm()
        // ->toTopic($phone . '_android')// $topic must an string (topic name)
        // ->notification([
        //     'title' => 'Info Transaksi',
        //     'body' => 'Transaksi pembelian ' . $product->name . ' Gagal ',
        // ])
        // ->send();
    }
}
