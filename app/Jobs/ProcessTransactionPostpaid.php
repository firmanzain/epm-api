<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\v2\Ppob, App\Models\v2\MemberBuyPpob, App\Models\v2\Member;
use App\Http\Controllers\API\v2\vendor\VendorController;
use App\Http\Controllers\API\v2\HelperController;

class ProcessTransactionPostpaid implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $memberBuyPpob;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MemberBuyPpob $memberBuyPpob)
    {
        $this->memberBuyPpob = $memberBuyPpob;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $buyPpob = $this->memberBuyPpob;

        $product = Product::where('id', $buyPpob->product_id)
                            ->where('price', 0)
                            ->where('status_active', 1)
                            ->first();

        $member = Member::where('id', $buyPpob->user_id)->first();
        $phone = HelperController::format_phone($member->phone);

        $suppProducts = $product->SupplierProduct()->where('status_active', 1)
                                ->orderBy('biaya_admin', 'asc')->get();

        $statusName = 'Gagal';

        foreach ($suppProducts as $suppProduct) {
            $supplier = $suppProduct->suppliers()->first();
            $class = $supplier->controller_name;
            $instance = new $class();

            if (!($instance instanceof VendorController)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Please wait. We will back soon',
                    'developerMessage' => 'Must implement VendorController class.'
                ], 500);
            }

            $buyPpob->supplier_id = $supplier->id;
            $buyPpob->save();

            $result = $instance::pay_postpaid($transactionId, $suppProduct->voucher_code, $buyPpob->user_id);
            // $result = 0: Success/Pending, 1: Error Transaction, 2: Insufficient Ballance
            // END INSERT SEND API SUPPLIER

            $resultCode = $result['status'];

            if ($resultCode == 0) {
                $statusName = 'Sukses';
                break;
            }
        }

        if ($buyPpob->supplier_id == NULL) {
            $buyPpob->status_buy_ppob = 0;
            $buyPpob->save();
        }

        fcm()
            ->toTopic($phone.'_android') // $topic must an string (topic name)
            ->notification([
                'title' => 'Info Transaksi',
                'body' => 'Transaksi pembelian '.$product->name.' '.$statusName,
            ])
            ->send();
    }
}
