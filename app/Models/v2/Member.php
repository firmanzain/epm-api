<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
       'name', 'email', 'password', 'phone', 'address', 'avatar', 'user_level_id', 'valid', 'referal_id'
    ];

    protected $table = 'users';

    public function level()
    {
        return $this->belongsTo('App\Models\v2\UserLevel', 'user_level_id');
    }

    public function karyawanLevel()
    {
        return $this->belongsTo('App\Models\v2\KaryawanLevel', 'karyawan_level_id');
    }

    public function totalTopUp()
    {
        return $this->hasOne('App\Models\v2\TopupMember','user_id')->selectRaw('user_id,SUM(topup_members.topup_saldo) as topup_saldo')->where('topup_status',1)->groupBy('user_id');
    }

    public function totalBuyProduct()
    {
        return $this->hasOne('App\Models\v2\MemberBuyProduct','user_id')->selectRaw('user_id,SUM(member_buy_products.buy_product_price) as buy_product_price')->where('status_buy_product','<>',  0)->groupBy('user_id');
    }

    public function totalBuyLevel()
    {
        return $this->hasOne('App\Models\v2\MemberBuyLevel','user_id')->selectRaw('user_id,SUM(member_buy_levels.level_price) as level_price')->groupBy('user_id');
    }

    public function totalBuyppob()
    {
        return $this->hasOne('App\Models\v2\MemberBuyPpob','user_id')->selectRaw('user_id,SUM(member_buy_ppobs.total) as total_buy_ppob')->where('status_buy_ppob', '<>', 0)->groupBy('user_id');
    }

    public function totalBonus()
    {
        return $this->hasOne('App\Models\v2\MemberBonus','user_id')->selectRaw('user_id,SUM(member_bonuses.nominal_bonus) as member_bonus')->groupBy('user_id');
    }

    public function totalWithdraw()
    {
        return $this->hasOne('App\Models\v2\MemberWithdraw','user_id')->selectRaw('user_id,round(SUM(member_withdraws.amount + member_withdraws.admin_fee)) as total_withdraw')->where('is_approved', '<>', 0)->groupBy('user_id');
    }

    public function totalSendSaldo()
    {
        return $this->hasOne('App\Models\v2\TransferSaldo','sender_id')->selectRaw('sender_id,SUM(transfer_saldos.nominal) as total_sender_saldo')->groupBy('sender_id');
    }

    public function totalReceiveSaldo()
    {
        return $this->hasOne('App\Models\v2\TransferSaldo','receiver_id')->selectRaw('receiver_id,SUM(transfer_saldos.nominal) as total_receiver_saldo')->groupBy('receiver_id');
    }

    public function totalDeposit()
    {
        return $this->hasOne('App\Models\v2\Deposit','user_id')->selectRaw('user_id,SUM(deposits.nominal) as deposit_nominal')->where('deposit_type_id',1)->groupBy('user_id');
    }

    public function bankAccounts()
    {
        return $this->hasMany('App\Models\v2\UserBankAccount');
    }
}
