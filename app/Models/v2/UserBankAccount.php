<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBankAccount extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function bank()
    {
        return $this->belongsTo('App\Models\v2\Bank');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\v2\Member');
    }
}
