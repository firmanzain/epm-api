<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $fillable = [
       'title', 'desc', 'pict_url', 'action_url', 'valid_util', 'html_content', 'isShow'
    ];
}
