<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = [
       'id', 'name', 'saldo'
    ];
}
