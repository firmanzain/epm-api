<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    public $timestamps = false;

    protected $fillable = [
       'user_id', 'token'
    ];
}
