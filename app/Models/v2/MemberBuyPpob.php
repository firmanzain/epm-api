<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class MemberBuyPpob extends Model
{
    public function member()
    {
        return $this->belongsTo('App\Models\v2\Member', 'user_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\v2\Product', 'product_id');
    }
}
