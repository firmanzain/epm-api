<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;
// use App\ProductType, App\SupplierProduct;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    
    protected $fillable = [
        'name', 'price', "productype"
    ];

    protected $hidden = [
        'product_type_id'
    ];

    // public function producttype()
    // {
    //     $this->belongsTo('App\ProductType');
    // }
    // public function persen_premium()
    // {
    //     return $this->hasOne('App\UserLevel','id')->selectRaw('id,persen_harga as persen');
    // }

    // public function persen_premium()
    // {
    //     return $this->belongsTo('App\UserLevel', 'id');
    // }

    public function SupplierProduct()
    {
        return $this->hasMany('App\Models\v2\SupplierProduct');
    }

    // public function ProductType()
    // {
    //     return $this->belongsTo('App\ProductType', 'product_type_id');
    // }

    public function ProductCategory()
    {
        return $this->belongsTo('App\Models\v2\ProductCategories', 'category_id');
    }
}
