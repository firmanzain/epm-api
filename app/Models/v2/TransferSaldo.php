<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class TransferSaldo extends Model
{
    public function users_sender()
    {
        return $this->belongsTo('App\Models\v2\User', 'sender_id');
    }

    public function users_receiver()
    {
        return $this->belongsTo('App\Models\v2\User', 'receiver_id');
    }
}
