<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model
{
    protected $fillable = [
       'name', 'icon', 'index', 'parent', 'filterUrl', 'status', 'isPostPaid'
    ];

    public function ProductCategory()
    {
        return $this->hasMany('App\Models\v2\Product', 'category_id');
    }
}
