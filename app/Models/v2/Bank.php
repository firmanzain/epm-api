<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = [
        'name'
    ];

    public function userBankAccount()
    {
        return $this->hasMany('App\Models\v2\UserBankAccount');
    }

    public function transactionBank()
    {
        return $this->hasMany('App\Models\v2\TransactionBank');
    }
}
