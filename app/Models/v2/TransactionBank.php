<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class TransactionBank extends Model
{
    public function bank()
    {
        return $this->belongsTo('App\Models\v2\Bank');
    }

    public function topupMember()
    {
        return $this->hasMany('App\Models\v2\TopupMember');
    }
}
