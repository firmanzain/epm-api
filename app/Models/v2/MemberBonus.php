<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class MemberBonus extends Model
{
    public function member()
    {
        return $this->belongsTo('App\Models\v2\Member', 'user_id');
    }
}
