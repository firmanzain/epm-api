<?php
/**
 * Created by PhpStorm.
 * User: mhasan
 * Date: 24/02/19
 * Time: 1:12
 */

namespace App\Models\v2;

class SubProductCategory
{
    public const TYPE_PRODUCT = 'product';
    public const TYPE_SUB_CATEGORY = 'sub-category';

    private $id = 0;
    private $name;
    private $icon;
    private $price = 0;
    private $point_premium = 0;
    private $filterUrl;
    private $postpaid = false;
    private $caption;
    private $sub_caption;
    private $input_type;
    private $description;
    private $extraButton;
    private $type = '';
    public $index = '';

    /**
     * SubProductCategory constructor.
     * @param int $id
     * @param String $name
     * @param String $icon
     * @param String $caption
     * @param String $sub_caption
     * @param String $input_type
     * @param bool $postpaid
     */
    public function __construct(int $id, String $name, String $icon, String $caption, String $sub_caption, String $input_type, bool $postpaid, String $index, String $description, String $extraButton)
    {
        $this->id = $id;
        $this->name = $name;
        $this->icon = $icon;
        $this->caption = $caption;
        $this->sub_caption = $sub_caption;
        $this->input_type = $input_type;
        $this->description = $description;
        $this->postpaid = $postpaid;
        $this->index = $index;
        $this->extraButton = $extraButton;
    }

    /**
     * @param Product $product
     * @param bool $is_premium
     * @return SubProductCategory
     */
    public static function createProduct(Product $product, bool $is_premium)
    {
        $price = intval($product->price);
        if ($is_premium && $product->price_premium > 0) {
            $price = intval($product->price_premium);
        }

        $instance = new self((int)$product->id, (String)$product->name, (String)$product->icon, (String)$product->caption, (String)$product->subcaption, (String)$product->input_type, (bool)$price == 0, (String)$product->index, (String)$product->description, (String)$product->extraButton);
        $instance->price = $price;
        if ($is_premium) {
            $instance->point_premium = $product->point;
        }
        $instance->type = self::TYPE_PRODUCT;
        return $instance;
    }

    /**
     * @param ProductCategories $category
     * @param bool $postpaid
     * @return SubProductCategory
     */
    public static function createCategory(ProductCategories $category)
    {
        $instance = new self((int)$category->id, (String)$category->name, (String)$category->icon, (String)$category->caption, (String)$category->subcaption, (String)$category->input_type, (bool)$category->isPostPaid, (String)$category->index, (String)$category->description, (String)$category->extraButton);
        $instance->filterUrl = $category->filterUrl;
        $instance->type = self::TYPE_SUB_CATEGORY;
        return $instance;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return String
     */
    public function getName(): String
    {
        return $this->name;
    }

    /**
     * @return String
     */
    public function getIcon(): String
    {
        return $this->icon;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getPointPremium(): int
    {
        return $this->point_premium;
    }

    /**
     * @return mixed
     */
    public function getFilterUrl()
    {
        return $this->filterUrl;
    }

    /**
     * @return bool
     */
    public function isPostpaid(): bool
    {
        return $this->postpaid;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return String
     */
    public function getCaption(): String
    {
        return $this->caption;
    }

    /**
     * @return String
     */
    public function getSubCaption(): String
    {
        return $this->sub_caption;
    }

    /**
     * @return String
     */
    public function getInputType(): String
    {
        return $this->input_type;
    }

    /**
     * @return String
     */
    public function getIndex(): String
    {
        return $this->index;
    }

    /**
     * @return String
     */
    public function getDescription(): String
    {
        return $this->description;
    }

    /**
     * @return String
     */
    public function getExtraButton(): String
    {   
        return $this->extraButton;
    }

}
