<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class SendapiSupplier extends Model
{
    public function products()
    {
        $this->belongsTo('App\Models\v2\Product');
    }

    public function suppliers()
    {
        $this->belongsTo('App\Models\v2\Supplier');
    }
}
