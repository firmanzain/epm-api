<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class SupplierProduct extends Model
{
    public function products()
    {
        return $this->belongsTo('App\Models\v2\Product', 'product_id');
    }

    public function suppliers()
    {
        return $this->hasOne('App\Models\v2\Supplier', 'id', 'supplier_id');
    }
}
