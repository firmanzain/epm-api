<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class MemberWithdraw extends Model
{
    public function member()
    {
        return $this->belongsTo('App\Models\v2\Member', 'user_id');
    }

    public function member_bank_account()
    {
        return $this->belongsTo('App\Models\v2\UserBankAccount');
    }
}
