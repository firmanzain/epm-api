<?php

namespace App\Models\v2;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model
{
    public function users()
    {
       return $this->hasMany('App\Models\v2\User');
    }
}
