<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return response()->json(['code' => 404, 'message' => 'not found'], 404);
});

Route::group(['prefix' => '/v2'], function () {
    // Route::get('/test', 'API\v2\WalletController@test');
    Route::get('/', function () {
        return response()->json(['code' => 404, 'message' => 'not found'], 404);
    });
    Route::get('/version/android', 'API\v2\HelperController@version_android');
    // Route::get('/test', 'API\v2\vendor\DutaPulsaController@test');

    // ROUTING FOR ACP
    Route::group(['prefix' => '/acp'], function () {
        Route::group(['prefix' => '/auth'], function () {
            Route::post('/', 'API\v2\acp\AuthController@login');
            Route::post('/check', 'API\v2\acp\AuthController@check_token')->middleware('jwt.auth');
        });
        Route::post('/categories', 'API\v2\acp\ProductCategoriesController@store');
        Route::post('/categories/{id}', 'API\v2\acp\ProductCategoriesController@update');
        Route::post('/categories/product/{id}', 'API\v2\acp\ProductController@update');
        Route::post('/banner', 'API\v2\acp\AnnouncementController@store');
        // Route::post('/avatar/move', 'API\v2\acp\MemberController@move_avatar');
        // Route::get('/test', 'API\v2\vendor\GKMController@test');
    });

    // ROUTING FOR API
    Route::group(['prefix' => '/otp'], function () {
        Route::post('/request', 'API\v2\MemberController@otp_request');
        Route::post('/resend', 'API\v2\MemberController@otp_resend');
        Route::post('/verify', 'API\v2\MemberController@otp_verify');
    });
    Route::group(['prefix' => '/member'], function () {
        Route::post('/register', 'API\v2\MemberController@register');
        Route::post('/login', 'API\v2\MemberController@login');
        Route::post('/reset/link', 'API\v2\MemberController@reset_link_request');
        Route::get('/reset/{token}', 'API\v2\MemberController@reset_link_check');
        Route::post('/reset/{token}', 'API\v2\MemberController@reset_link');
        Route::delete('/', 'API\v2\MemberController@logout')->middleware('jwt.auth');
        Route::delete('/logout-all', 'API\v2\MemberController@logoutAll');
        Route::delete('/block', 'API\v2\MemberController@block_member');
        Route::group(['prefix' => '/account', 'middleware' => 'jwt.auth'], function () {
            Route::get('/', 'API\v2\MemberController@profile');
            Route::post('/', 'API\v2\MemberController@profile_update');
        });
        Route::group(['prefix' => '/bank', 'middleware' => 'jwt.auth'], function () {
            Route::get('/', 'API\v2\UserBankAccountController@index');
            Route::post('/', 'API\v2\UserBankAccountController@store');
            Route::post('/{id}', 'API\v2\UserBankAccountController@update');
            Route::delete('/{id}', 'API\v2\UserBankAccountController@destroy');
        });
        Route::get('/referal', 'API\v2\MemberController@referal')->middleware('jwt.auth');
        Route::group(['prefix' => '/downline', 'middleware' => 'jwt.auth'], function () {
            Route::get('/', 'API\v2\MemberController@downline');
            Route::get('/{id}', 'API\v2\MemberController@downline');
        });
    });
    // Route::group(['prefix' => '/transaction'], function () {
    Route::group(['prefix' => '/transaction', 'middleware' => 'jwt.auth'], function () {
        Route::get('/history', 'API\v2\TransactionController@get_transaction_history');
        Route::get('/postpaid/{id}/print', 'API\v2\TransactionController@get_transaction_postpaid_print');
        Route::get('/prepaid/{id}/print', 'API\v2\TransactionController@get_transaction_prepaid_print');
    });
    Route::group(['prefix' => '/wallet', 'middleware' => 'jwt.auth'], function () {
        Route::get('/balance/history', 'API\v2\WalletController@get_balance_history');
        Route::get('/point/history', 'API\v2\WalletController@get_point_history');
        Route::get('/pending', 'API\v2\WalletController@get_pending_history');
        Route::post('/withdraw', 'API\v2\WalletController@withdraw');
        Route::group(['prefix' => '/transfer'], function () {
            Route::post('/', 'API\v2\WalletController@transfer');
            Route::get('/history', 'API\v2\WalletController@get_transfer_history');
            Route::get('/{referral}', 'API\v2\WalletController@get_transfer_recipient');
        });
        Route::get('/bank', 'API\v2\WalletController@get_bank');
        Route::group(['prefix' => '/topup'], function () {
            Route::post('/', 'API\v2\WalletController@topup');
            Route::get('/methods', 'API\v2\WalletController@topup_methods');
            Route::post('/confirmation', 'API\v2\WalletController@topup_confirmation');
        });
    });
    Route::group(['prefix' => '/categories', 'middleware' => 'jwt.auth'], function () {
        Route::get('/', 'API\v2\ProductController@get_categories');
        Route::get('/{id}', 'API\v2\ProductController@get_categories');
    });
    Route::group(['prefix' => '/filter', 'middleware' => 'jwt.auth'], function () {
        Route::get('/{category}/{prefix}', 'API\v2\FilterProductController@filter_by_prefix_number');
    });
    Route::group(['prefix' => '/product', 'middleware' => 'jwt.auth'], function () {
        Route::post('/prepaid', 'API\v2\ProductController@payment_prepaid');
        // Route::post('/prepaidv2', 'API\v2\ProductController@payment_prepaid_v2');
        Route::get('/{id}/vendor', 'API\v2\ProductController@vendor_by_product');
        Route::get('/{id}/inquiry/{customer}', 'API\v2\ProductController@inquiry_postpaid_v3');
        Route::post('/postpaid', 'API\v2\ProductController@payment_postpaid_v3');

        Route::post('/inquiry', 'API\v2\ProductController@inquiry_postpaid');
        // Route::post('/postpaid', 'API\v2\ProductController@payment_postpaid');
        // Route::post('/test', 'API\v2\vendor\SynetController@test');
    });
    Route::group(['prefix' => '/announcements', 'middleware' => 'jwt.auth'], function () {
        Route::get('/', 'API\v2\AnnouncementController@get_announcement_banner');
        Route::get('/inbox/{id}', 'API\v2\AnnouncementController@get_announcement');
        Route::get('/banner', 'API\v2\AnnouncementController@get_announcement_banner');
        Route::get('/inbox', 'API\v2\AnnouncementController@get_announcement');
    });
    Route::group(['prefix' => '/vendor/result'], function () {
        Route::get('/dutapulsa', 'API\v2\vendor\DutaPulsaController@callback_status');
        Route::post('/gkm', 'API\v2\vendor\GKMController@callback_status');
        Route::get('/synet', 'API\v2\vendor\SynetController@callback_status');
        Route::post('/chipsakti', 'API\v2\vendor\ChipsaktiController@callback_status');
    });
    Route::group(['prefix' => '/level', 'middleware' => 'jwt.auth'], function () {
        Route::get('/benefit', 'API\v2\MemberController@get_level_benefit');
        Route::get('/upgrade/{referral}', 'API\v2\MemberController@get_upgrade_referal');
        Route::post('/upgrade', 'API\v2\MemberController@level_upgrade');
    });

});
