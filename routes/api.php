<?php

use Illuminate\Http\Request;

Route::group(['prefix' => '/v1'], function() {
    Route::get('/', function() {
        return response()->json(['version' => 'v1']);
    });
    // Route::group(['prefix' => '/otp'], function() {
    //     Route::post('/request', 'APIAuthController@request_otp');
    //     Route::post('/resend', 'APIAuthController@resend_otp');
    //     Route::post('/check', 'APIAuthController@check_otp');
    // });
    Route::post('/user/login', function () {
        return response()->json([
            'code' => 401,
            'status' => false,
            'error' => 'invalid_authorization',
            'message' => 'Please update your application.'
        ], 401);
    });
    // Route::group(['prefix' => '/user'], function() {
    //     Route::get('/', 'APIAuthController@getUsers')->middleware('jwt.auth');
    //     Route::post('/register', 'APIAuthController@register');
    //     Route::post('/login', 'APIAuthController@login');
    //     Route::post('/reset/link', 'APIAuthController@request_link_reset');
    //     Route::get('/reset/{phone}', 'APIAuthController@find_user_by_phone');
    //     Route::post('/reset/{phone}', 'APIAuthController@reset_password');
    // });
    // Route::group(['prefix' => '/admin/transaction', 'middleware' => 'jwt.auth'], function() {
    //     Route::post('/confirm_upgrade_level', 'TransactionController@confirm_upgrade_level');
    //     Route::post('/confirm_top_up', 'TransactionController@confirm_top_up');
    // });
    // Route::get('/avatar/{id}', 'UserController@get_user_avatar_by_id');
    // Route::group(['prefix' => '/me', 'middleware' => 'jwt.auth'], function() {
    //     Route::get('/credits', 'UserController@get_total_credits');
    //     Route::get('/level', 'UserController@get_current_level');
    //     Route::post('/level/upgrade', 'UserController@do_upgrade_level');
    //     Route::get('/statistic', 'UserController@get_user_statistic');
    //     Route::post('/edit', 'UserController@update_profile');
    //     Route::post('/change_password', 'UserController@change_password');
    //     Route::get('/user_bank_accounts', 'UserBankAccountController@index');
    //     Route::post('/user_bank_accounts', 'UserBankAccountController@store');
    //     Route::put('/user_bank_accounts/{id}', 'UserBankAccountController@update');
    //     Route::delete('/user_bank_accounts/{id}', 'UserBankAccountController@destroy');
    //     Route::get('/avatar', 'UserController@get_user_avatar');
    //     Route::group(['prefix' => '/referal'], function() {
    //         Route::get('/generate', 'UserController@generate_my_referal_id');
    //         Route::get('/members', 'UserController@get_user_members');
    //     });
    //     Route::group(['prefix' => '/transaction'], function() {
    //         Route::group(['prefix' => '/history'], function() {
    //             Route::get('/topup', 'TransactionController@get_user_topup_history');
    //             Route::get('/topup/{id}', 'TransactionController@get_user_topup_detail');
    //             Route::post('/topup/change_transfer_to', 'TransactionController@change_transfer_to');
    //             Route::get('/spending', 'TransactionController@get_user_spending_history');
    //             Route::get('/spending/{id}/{type}', 'TransactionController@get_spending_transaction_detail');
    //         });
    //         Route::group(['prefix' => '/transfer_credit'], function() {
    //             Route::post('/send', 'TransferSaldoController@transfer_saldo');
    //         });
    //     });
    //     Route::group([
    //         'prefix' => '/withdraws',
    //         'middleware' => 'jwt.auth'
    //     ], function() {
    //         Route::get('/', 'WithdrawController@index');
    //         Route::post('/', 'WithdrawController@store');
    //         Route::get('/{id}', 'WithdrawController@show');
    //         Route::patch('/{id}', 'WithdrawController@update');
    //         Route::put('/{id}', 'WithdrawController@update');
    //         Route::delete('/{id}', 'WithdrawController@destroy');
    //     });
    // });
    // Route::group(['prefix' => '/common', 'middleware' => 'jwt.auth'], function() {
    //     Route::group(['prefix' => '/topup'], function() {
    //         Route::get('/banks', 'BankController@transaction_banks_list');
    //     });
    //     Route::group(['prefix' => '/referal'], function() {
    //         Route::get('/check/{id}', 'UserController@check_referal_exist');
    //     });
    //
    //     Route::group([
    //         'prefix' => '/configs',
    //         'middleware' => 'jwt.auth'
    //     ], function() {
    //         Route::get('/{code}', 'SystemConfigController@get');
    //     });
    //
    //     Route::group(['prefix' => '/notifications'], function() {
    //         Route::post('/', 'NotificationController@send');
    //         Route::post('/manual', 'NotificationController@manual_send');
    //         Route::put('/{id}', 'NotificationController@update_key');
    //         Route::patch('/{id}', 'NotificationController@update_key');
    //     });
    // });
    // Route::group([
    //     'prefix' => '/common/announcements'
    // ], function() {
    //     Route::get('/', 'AnnouncementController@index');
    //     Route::post('/', 'AnnouncementController@store');
    //     Route::get('/{id}', 'AnnouncementController@show');
    //     Route::patch('/{id}', 'AnnouncementController@update');
    //     Route::put('edit/{id}', 'AnnouncementController@update');
    //     Route::delete('/delete/{id}', 'AnnouncementController@destroy');
    //     Route::post('/send_notif_all', 'NotificationController@all_send');
    // });
    //
    // Route::get('h2h_result/duta', 'DutaPulsaController@duta_result');
    //
    // Route::post('/inquiry_ppob', 'DutaPulsaController@inquiry_ppob')->middleware('jwt.auth');
    // Route::post('/payment_ppob', 'DutaPulsaController@payment_ppob')->middleware('jwt.auth');
    // Route::post('/buy_voucher', 'DutaPulsaController@buy_voucher')->middleware('jwt.auth');
    // Route::post('/token_listrik', 'DutaPulsaController@token_listrik')->middleware('jwt.auth');
    // Route::post('/e_toll', 'DutaPulsaController@e_toll')->middleware('jwt.auth');
    // Route::post('/upgrade_level', 'TransactionController@upgrade_level')->middleware('jwt.auth');
    // Route::post('/top_up', 'TransactionController@top_up')->middleware('jwt.auth');
    //
    // Route::post('/vendor/prepaid', 'VendorController@prepaid')->middleware('jwt.auth');
    // Route::post('/vendor/postpaid', 'VendorController@postpaid')->middleware('jwt.auth');
    //
    // Route::post('/gkm/payment', 'GKMController@payment');
    // Route::post('/gkm/postpaid', 'GKMController@postpaid')->middleware('jwt.auth');
    // Route::post('/gkm/coba', 'GKMController@coba');
    //
    // Route::post('/helper/prefix_number', 'HelperController@get_prefix_number')->middleware('jwt.auth');
    //
    // Route::post('/product/pulsa/prabayar', 'ProductController@get_pulsa_prabayar')->middleware('jwt.auth');
    // Route::post('/product/pulsa/paket_data', 'ProductController@get_paket_data')->middleware('jwt.auth');
    // Route::post('/product/pulsa/pln_data', 'ProductController@get_pln_data')->middleware('jwt.auth');
    // Route::post('/product/pulsa/voucher_game', 'ProductController@get_voucher_game')->middleware('jwt.auth');
    // Route::post('/product/pulsa/voucher_mtix', 'ProductController@get_voucher_mtix')->middleware('jwt.auth');
    // Route::post('/product/pulsa/voucher_ovo', 'ProductController@get_voucher_ovo')->middleware('jwt.auth');
    // Route::post('/product/pulsa/voucher_toll', 'ProductController@get_voucher_toll')->middleware('jwt.auth');
    // Route::post('/product/ppob/pdam_data', 'ProductController@get_pdam_ppob')->middleware('jwt.auth');
    // Route::post('/product/ppob/pln_data', 'ProductController@get_pln_ppob')->middleware('jwt.auth');
    // Route::post('/product/ppob/ppob_data', 'ProductController@get_data_ppob')->middleware('jwt.auth');
    // Route::post('/product/ppob/tv_kabel', 'ProductController@get_data_ppob_tv_kabel')->middleware('jwt.auth');
    // Route::post('/product/ppob/tv_kabel_prepaid', 'ProductController@get_data_ppob_tv_kabel_prepaid')->middleware('jwt.auth');
    // Route::post('/product/ppob/topas_tv', 'ProductController@get_topas_tv')->middleware('jwt.auth');
    // Route::post('/product/ppob/kvision_tv', 'ProductController@get_kvision_tv')->middleware('jwt.auth');
    // Route::post('/product/ppob/matrix_tv', 'ProductController@get_matrix_tv')->middleware('jwt.auth');
    // Route::post('/product/ppob/skynindo_tv', 'ProductController@get_skynindo_tv')->middleware('jwt.auth');
    // Route::post('/product/ppob/internet', 'ProductController@get_data_ppob_internet')->middleware('jwt.auth');
    // Route::post('/product/ppob/internet_prepaid', 'ProductController@get_data_ppob_internet_prepaid')->middleware('jwt.auth');
    // Route::post('/product/ppob/telco_postpaid', 'ProductController@get_data_postpaid_telco')->middleware('jwt.auth');
    // Route::post('/product/ppob/multi_finance', 'ProductController@get_data_multi_finance')->middleware('jwt.auth');
    // Route::post('/product/ppob/kartu_kredit', 'ProductController@get_data_kartu_kredit')->middleware('jwt.auth');
    // Route::post('/product/ppob/bpjs_kesehatan', 'ProductController@get_data_bpjs_kesehatan')->middleware('jwt.auth');
    // Route::post('/product/ppob/telkom_data', 'ProductController@get_telkom_ppob')->middleware('jwt.auth');
    // Route::post('/product/ppob/pdam', 'ProductController@get_data_ppob_pdam')->middleware('jwt.auth');
    //
    // Route::get('/dashboard/get_summary_transaksi', 'HelperController@get_summary_transaksi')->middleware('jwt.auth');
    // Route::get('/dashboard/get_grafik_transaksi', 'HelperController@get_grafik_transaksi')->middleware('jwt.auth');
    // Admin Routing
    // Route::group(['prefix' => '/admin'], function() {
    //     Route::group(['prefix' => '/user_login'], function() {
    //         Route::post('/register', 'UserLoginController@register');
    //         Route::post('/login', 'UserLoginController@login');
    //         Route::put('/change_password_user_login', 'UserLoginController@change_password_user_login');
    //         Route::post('/', 'UserLoginController@index');
    //         Route::post('/search', 'UserLoginController@search');
    //         Route::post('/add', 'UserLoginController@create');
    //         Route::put('/edit/{id}', 'UserLoginController@edit');
    //         Route::delete('/delete/{id}', 'UserLoginController@remove');
    //     });
    //     Route::group(['prefix' => '/otp'], function() {
    //         Route::get('/', 'OtpController@index');
    //         Route::post('/', 'OtpController@get_data_paging');
    //         Route::post('/search', 'OtpController@search');
    //         Route::delete('/delete/{id}', 'OtpController@remove');
    //         Route::delete('/deleteAll/{id}', 'OtpController@removeAll');
    //         Route::delete('/deleteAllRows', 'OtpController@removeAllRows');
    //     });
    //     Route::group(['prefix' => '/user'], function() { //created by ucup
    //         Route::get('/all_user', 'UserController@index');
    //         Route::post('/all', 'UserController@index');
    //         Route::post('/search', 'UserController@search');
    //         Route::post('/history', 'UserController@history');
    //         Route::post('/referal', 'UserController@referal');
    //         Route::post('/add', 'UserController@create');
    //         Route::put('/edit/{id}', 'UserController@edit');
    //         Route::delete('/delete/{id}', 'UserController@remove');
    //         Route::post('/userGrowth', 'UserController@userGrowth');
    //         Route::put('/changeKaryawanLevel/{id}', 'UserController@change_karyawan_level');
    //         Route::post('/getChild', 'UserController@get_child_data');
    //         Route::post('/getManagerList', 'UserController@get_manager_list');
    //         Route::post('/getSPVList', 'UserController@get_spv_list');
    //         Route::post('/getLeaderList', 'UserController@get_leader_list');
    //         Route::post('/getSalesList', 'UserController@get_sales_list');
    //         Route::post('/getReferalList', 'UserController@get_referal_list');
    //         Route::post('/getSalary', 'UserController@get_salary_bydate');
    //         Route::post('/search_referal', 'UserController@search_referal_manager');
    //         Route::post('/search_referal_nonManager', 'UserController@search_referal_nonManager');
    //         Route::post('/test_bonus', 'UserController@test_bonus_member');
    //     });
    //     Route::group(['prefix' => '/userlevel'], function() {
    //         Route::get('/', 'UserLevelController@index');
    //         Route::post('/', 'UserLevelController@get_data_paging');
    //         Route::post('/search', 'UserLevelController@search');
    //         Route::post('/added', 'UserLevelController@created');
    //         Route::put('/edited/{id}', 'UserLevelController@edited');
    //         Route::delete('/deleted/{id}', 'UserLevelController@removed');
    //     });
    //     Route::group(['prefix' => '/product'], function() {
    //         Route::get('/', 'ProductController@index');
    //         Route::post('/', 'ProductController@get_data_paging');
    //         Route::get('/{id}', 'ProductController@get_data_id');
    //         Route::post('/search', 'ProductController@search');
    //         Route::post('/add', 'ProductController@create');
    //         Route::put('/edit/{id}', 'ProductController@edit');
    //         Route::delete('/delete/{id}', 'ProductController@remove');
    //     });
    //     Route::group(['prefix' => '/productype'], function() {
    //         Route::get('/', 'ProductTypeController@index');
    //         Route::post('/', 'ProductTypeController@get_data_paging');
    //         Route::post('/search', 'ProductTypeController@search');
    //         Route::post('/add', 'ProductTypeController@create');
    //         Route::put('/edit/{id}', 'ProductTypeController@edit');
    //         Route::delete('/delete/{id}', 'ProductTypeController@remove');
    //     });
    //     Route::group(['prefix' => '/level'], function() {
    //         Route::get('/all', 'UserLevelController@get_all_trashed');
    //         Route::get('/{id}', 'UserLevelController@get');
    //         Route::post('/add', 'UserLevelController@create');
    //         Route::put('/{id}', 'UserLevelController@edit');
    //         Route::delete('/{id}', 'UserLevelController@remove');
    //         Route::get('/{id}/restore', 'UserLevelController@restore');
    //     });
    //     // Route::get('/top_up_saldo', 'TransactionController@get_data_top_up'); //created by ucup
    //     Route::post('/top_up_saldo', 'TransactionController@get_paging_topup');
    //     Route::post('/top_up_saldo/search', 'TransactionController@search_topup');
    //     Route::post('/top_up_saldo/searchbyDate', 'TransactionController@search_topup_by_date');
    //     Route::post('/top_up_saldo_success', 'TransactionController@get_paging_topup_success');
    //     Route::post('/top_up_saldo/search_success', 'TransactionController@search_topup_success');
    //     Route::post('/top_up_saldo/searchbyDate_success', 'TransactionController@search_topup_by_date_success');
    //     Route::post('/top_up_saldo_reject', 'TransactionController@get_paging_topup_reject');
    //     Route::post('/top_up_saldo/search_reject', 'TransactionController@search_topup_reject');
    //     Route::post('/top_up_saldo/searchbyDate_reject', 'TransactionController@search_topup_by_date_reject');
    //     Route::put('/top_up_saldo/edit/{id}', 'TransactionController@edit');
    //     Route::post('/cetak_top_up_sukses_excell', 'TransactionController@search_topup_sukses_by_date_excell');
    //     Route::post('/cetak_top_up_pending_excell', 'TransactionController@search_topup_pending_by_date_excell');
    //     Route::post('/cetak_top_up_reject_excell', 'TransactionController@search_topup_reject_by_date_excell');
    //     Route::post('/confirm_top_up', 'TransactionController@confirm_top_up'); //created by ucup
    //     Route::post('/reject_top_up', 'TransactionController@reject_top_up'); //created by ucup
    //     Route::post('/total_deposit', 'TransactionController@totalDeposit'); //created by ucup
    //     Route::get('/getBonusbyPass/{request}', 'UserController@member_bonus');
    //     Route::get('/getBonusbyPassUserID/{user_id}/{reference_by}', 'UserController@member_bonus_bypass_id');
    //     Route::group(['prefix' => '/supplier'], function() {
    //         Route::get('/', 'SupplierController@index');
    //         Route::post('/', 'SupplierController@get_data_paging');
    //         Route::post('/search', 'SupplierController@search');
    //         Route::post('/add', 'SupplierController@create');
    //         Route::put('/edit/{id}', 'SupplierController@edit');
    //         Route::delete('/delete/{id}', 'SupplierController@remove');
    //     });
    //     Route::group(['prefix' => '/supplierproduct'], function() {
    //         // Route::get('/', 'SupplierProductController@index');
    //         Route::post('/', 'SupplierProductController@get_data_paging');
    //         Route::post('/search', 'SupplierProductController@search');
    //         Route::post('/add', 'SupplierProductController@create');
    //         Route::put('/edit/{supplier_id}/{product_id}', 'SupplierProductController@edit');
    //         Route::delete('/delete/{product_id}/{supplier_id}', 'SupplierProductController@remove');
    //         Route::get('/get/{product_id}/{supplier_id}', 'SupplierProductController@get_supplier_product');
    //         Route::post('/getBySupplier', 'SupplierProductController@get_product_by_supplier');
    //     });
    //     Route::group(['prefix' => '/prefixnumber'], function() {
    //         // Route::get('/', 'PrefixNumberController@index');
    //         Route::post('/', 'PrefixNumberController@get_data_paging');
    //         Route::post('/search', 'PrefixNumberController@search');
    //         Route::post('/add', 'PrefixNumberController@create');
    //         Route::put('/edit/{id}', 'PrefixNumberController@edit');
    //         Route::delete('/delete/{id}', 'PrefixNumberController@remove');
    //     });
    //     Route::group(['prefix' => '/bank'], function() {
    //         Route::get('/', 'BankController@index');
    //         Route::post('/', 'BankController@get_data_paging');
    //         Route::post('/search', 'BankController@search');
    //         Route::post('/add', 'BankController@add_bank');
    //         Route::put('/edit/{id}', 'BankController@edit');
    //         Route::delete('/delete/{id}', 'BankController@remove');
    //         Route::group(['prefix' => '/transaction'], function() {
    //             Route::post('/add', 'BankController@add_transaction_bank');
    //         });
    //     });
    //     Route::group(['prefix' => '/transactionbank'], function() {
    //         Route::get('/', 'TransactionBankController@index');
    //         Route::post('/', 'TransactionBankController@get_data_paging');
    //         Route::post('/search', 'TransactionBankController@search');
    //         Route::post('/add', 'TransactionBankController@create');
    //         Route::put('/edit/{id}', 'TransactionBankController@edit');
    //         Route::delete('/delete/{id}', 'TransactionBankController@remove');
    //     });
    //     Route::group(['prefix' => '/transaction'], function() {
    //         Route::group(['prefix' => '/history'], function() {
    //             Route::get('/topup', 'TransactionController@get_user_topup_history');
    //             Route::post('/topup/change_transfer_to', 'TransactionController@change_transfer_to');
    //             // Route::get('/buy_level', 'TransactionController@get_user_buy_level_all');
    //             Route::post('/buy_level', 'TransactionController@get_user_buy_level_all');
    //             Route::post('/buy_level/search', 'TransactionController@search_buy_level');
    //             Route::post('/buy_level/searchbyDate', 'TransactionController@search_buy_level_by_date');
    //             // Route::get('/buy_product', 'TransactionController@get_user_buy_product_all');
    //             Route::post('/buy_product', 'TransactionController@get_user_buy_product_all');
    //             Route::post('/buy_product/search', 'TransactionController@search_buy_product');
    //             Route::post('/buy_product/searchbyDate', 'TransactionController@search_buy_product_by_date');
    //             Route::post('/member_bonus', 'TransactionController@get_paging_member_bonus');
    //             Route::post('/member_bonus/view_referal', 'TransactionController@view_referal');
    //             Route::post('/member_bonus/search', 'TransactionController@search_member_bonus');
    //             Route::post('/member_bonus/searchbyDate', 'TransactionController@search_member_bonus_by_date');
    //             Route::post('/transfer_saldo', 'TransferSaldoController@get_data_transfer_saldo');
    //             Route::post('/search_transfer_saldo', 'TransferSaldoController@search_transfer_saldo');
    //             Route::post('/search_transfer_saldo_bydate', 'TransferSaldoController@search_transfer_saldo_bydate');
    //             Route::post('/buy_ppob', 'TransactionController@get_paging_member_buy_ppob');
    //             Route::post('/buy_ppob/search', 'TransactionController@search_member_buy_ppob');
    //             Route::post('/buy_ppob/searchbyDate', 'TransactionController@search_member_buy_ppob_by_date');
    //         });
    //         Route::post('/refund', 'TransactionController@get_refund');
    //     });
    //     Route::group(['prefix' => '/dashboard'], function() {
    //         Route::get('/userlogin', 'HelperController@get_count_userlogin');
    //         Route::get('/member', 'HelperController@get_count_member');
    //         Route::get('/supplierproduct', 'HelperController@get_count_supplierproduct');
    //         Route::get('/product', 'HelperController@get_count_product');
    //         Route::get('/premium_member', 'HelperController@get_count_premium_member');
    //         Route::get('/standart_member', 'HelperController@get_count_standart_member');
    //         Route::get('/top_user', 'HelperController@get_top10_user');
    //         Route::get('/top_product', 'HelperController@get_top10_product');
    //         Route::get('/product_pulsa', 'HelperController@get_count_product_pulsa');
    //         Route::get('/product_data', 'HelperController@get_count_product_data');
    //         Route::get('/product_token', 'HelperController@get_count_product_token');
    //         Route::get('/product_game', 'HelperController@get_count_product_game');
    //     });
    //     Route::group(['prefix' => '/karyawanlevel'], function() {
    //         Route::get('/', 'KaryawanLevelController@index');
    //         Route::post('/', 'KaryawanLevelController@get_data_paging');
    //         Route::post('/search', 'KaryawanLevelController@search');
    //         Route::post('/add', 'KaryawanLevelController@create');
    //         Route::put('/edit/{id}', 'KaryawanLevelController@edit');
    //         Route::delete('/delete/{id}', 'KaryawanLevelController@remove');
    //     });
    //     Route::group(['prefix' => '/withdraw'], function() {
    //         Route::post('/', 'WithdrawController@get_data_paging');
    //         Route::post('/confirm_member_withdraw', 'WithdrawController@confirm_member_withdraw');
    //         Route::post('/reject_member_withdraw', 'WithdrawController@reject_member_withdraw');
    //         Route::post('/search_withdraw', 'WithdrawController@search_withdraw');
    //         Route::post('/save_attachment', 'WithdrawController@save_attachment');
    //     });
    // });
});
