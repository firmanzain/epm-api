<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditMemberBuyLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_buy_levels', function (Blueprint $table) {
            $table->dropColumn('reference_number_member_level');
            $table->boolean('status_buy_level')->default(false)->change();
            $table->unsignedInteger('user_login_id')->nullable()->change();
            $table->unsignedInteger('user_id')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_buy_levels', function (Blueprint $table) {
            $table->string('reference_number_member_level', 25)->nullable();
            $table->unsignedInteger('user_id')->nullable()->change();
        });
    }
}
