<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrefixNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prefix_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no', 6);
            $table->string('kartu', 50);
            $table->string('operator', 50);
            $table->boolean('status_prefix')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prefix_numbers');
    }
}
