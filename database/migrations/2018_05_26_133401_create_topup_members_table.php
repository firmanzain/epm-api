<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopupMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topup_members', function (Blueprint $table) {
            $table->increments('id');
            $table->float('topup_saldo', 9, 2);
            $table->timestamps();
            $table->string('topup_reference_number', 25);
            $table->boolean('topup_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topup_members');
    }
}
