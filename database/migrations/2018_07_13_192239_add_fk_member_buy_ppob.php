<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkMemberBuyPpob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_buy_ppobs', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('product_id')->nullable();
            $table->unsignedInteger('supplier_id')->nullable();
            $table->unsignedInteger('sendapi_supplier_id')->nullable();
            $table->unsignedInteger('result_supplier_id')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('supplier_id')->references('id')->on('suppliers');
            $table->foreign('sendapi_supplier_id')->references('id')->on('sendapi_suppliers');
            $table->foreign('result_supplier_id')->references('id')->on('result_suppliers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_buy_ppobs', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['product_id']);
            $table->dropForeign(['supplier_id']);
            $table->dropForeign(['sendapi_supplier_id']);
            $table->dropForeign(['result_supplier_id']);

            $table->dropColumn('user_id');
            $table->dropColumn('product_id');
            $table->dropColumn('supplier_id');
            $table->dropColumn('sendapi_supplier_id');
            $table->dropColumn('result_supplier_id');
        });
    }
}
