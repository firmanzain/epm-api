<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_transaksis', function (Blueprint $table) {
            $table->increments('id');
            $table->text('keterangan');            
            $table->timestamps();
            $table->float('debit', 9, 2);
            $table->float('credit', 9, 2);
            $table->float('saldo_akhir', 9, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_transaksis');
    }
}
