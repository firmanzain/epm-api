<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKelurahansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelurahans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenis_kabupaten_kota', 20);
            $table->string('kode_pos', 5);
            $table->string('nama_kabupaten_kota', 100);
            $table->string('nama_kecamatan', 100);
            $table->string('nama_kelurahan_desa', 100);
            $table->string('nama_provinsi', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelurahans');
    }
}
