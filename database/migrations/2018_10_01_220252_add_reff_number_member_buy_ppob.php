<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReffNumberMemberBuyPpob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_buy_ppobs', function (Blueprint $table) {
            $table->string('reff_number', 100)->nullable()->after('msisdn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_buy_ppobs', function (Blueprint $table) {
            $table->dropColumn('reff_number');
        });
    }
}
