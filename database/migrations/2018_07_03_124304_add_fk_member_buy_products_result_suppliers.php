<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkMemberBuyProductsResultSuppliers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_buy_products', function (Blueprint $table) {
            $table->unsignedInteger('result_supplier_id')->nullable();

            $table->foreign('result_supplier_id')->references('id')->on('result_suppliers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_buy_products', function (Blueprint $table) {
            $table->dropForeign(['result_supplier_id']);

            $table->dropColumn('result_supplier_id');
        });
    }
}
