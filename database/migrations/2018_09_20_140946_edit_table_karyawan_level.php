<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTableKaryawanLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('karyawan_levels', function (Blueprint $table) {
            $table->renameColumn('bonus_achievement', 'uang_bensin');
            $table->dropColumn(['bonus_bila_lebih_target', 'prorate', 'target']);
            $table->renameColumn('keterangan', 'target');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('karyawan_levels', function (Blueprint $table) {
            $table->renameColumn('uang_bensin', 'bonus_achievement');
		    $table->renameColumn('target', 'keterangan');
        });
    }
}
