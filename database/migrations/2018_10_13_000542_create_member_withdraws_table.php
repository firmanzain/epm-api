<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_withdraws', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('user_bank_account_id');
            $table->decimal('amount', 10, 2);
            $table->string('remark')->nullable();
            $table->unsignedInteger('user_login_id')->nullable();
            $table->boolean('is_approved')->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->string('attachment')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('user_login_id')->references('id')->on('user_logins');
            $table->foreign('user_bank_account_id')->references('id')->on('user_bank_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_withdraws');
    }
}
