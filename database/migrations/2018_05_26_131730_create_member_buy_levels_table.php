<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberBuyLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_buy_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->float('level_price', 9, 2);
            $table->timestamps();
            $table->string('reference_number_member_level', 25);
            $table->boolean('status_buy_level');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_buy_levels');
    }
}
