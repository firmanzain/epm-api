<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberBuyPpobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_buy_ppobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ppob_id', 45);
            $table->float('charge', 12, 2);
            $table->float('tagihan', 12, 2);
            $table->float('admin', 12, 2);
            $table->float('total', 12, 2);
            $table->timestamps();
            $table->boolean('status_buy_ppob');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_buy_ppobs');
    }
}
