<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNamaPelangganMemberBuyProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_buy_products', function (Blueprint $table) {
            $table->text('nama_pelanggan')->nullable()->after('msisdn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_buy_products', function (Blueprint $table) {
            $table->dropColumn('nama_pelanggan');
        });
    }
}
