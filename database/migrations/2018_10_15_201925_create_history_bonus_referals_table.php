<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryBonusReferalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_bonus_referals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string("history_referal_tree", 150)->nullable();
            $table->float("nominal_bonus", 9, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_bonus_referals');
    }
}
