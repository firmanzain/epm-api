<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkResultSupplierToSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('result_suppliers', function (Blueprint $table) {
            $table->unsignedInteger('supplier_id')->nullable();

            $table->foreign('supplier_id')->references('id')->on('suppliers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('result_suppliers', function (Blueprint $table) {
            $table->dropForeign(['supplier_id']);

            $table->dropColumn('supplier_id');
        });
    }
}
