<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMsisdnNapelMemberBuyPpob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_buy_ppobs', function (Blueprint $table) {
            $table->string('msisdn', 25)->nullable()->after('status_buy_ppob');
            $table->string('nama_pelanggan', 125)->nullable()->after('msisdn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_buy_ppobs', function (Blueprint $table) {
            $table->dropColumn('msisdn');
            $table->dropColumn('nama_pelanggan');
        });
    }
}
