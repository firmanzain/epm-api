<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditSendapiSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sendapi_suppliers', function (Blueprint $table) {
            // $table->renameColumn('snedapi_status', 'sendapi_status');
            $table->string('sendapi_response_text')->after('sendapi_text');
            $table->boolean('sendapi_status')->after('sendapi_response_text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sendapi_suppliers', function (Blueprint $table) {
            $table->dropColumn('sendapi_response_text');
            $table->dropColumn('sendapi_status');
        });
    }
}
