<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentTypeMemberWithdrawTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_withdraws', function (Blueprint $table) {
            $table->unsignedInteger('payment_type')->default(1)->comment('1: Balance, 2: Point');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_withdraws', function (Blueprint $table) {
            $table->dropColumn('payment_type');
        });
    }
}
