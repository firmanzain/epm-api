<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendapiSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sendapi_suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sendapi_url', 50);
            $table->text('sendapi_text');
            // $table->boolean('sendapi_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sendapi_suppliers');
    }
}
