<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkMemberBuyLevelUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_buy_levels', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('user_level_id')->nullable();
            $table->unsignedInteger('user_login_id')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('user_level_id')->references('id')->on('user_levels');
            $table->foreign('user_login_id')->references('id')->on('user_logins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_buy_levels', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['user_level_id']);
            $table->dropForeign(['user_login_id']);

            $table->dropColumn('user_id');
            $table->dropColumn('user_level_id');
            $table->dropColumn('user_login_id');
        });
    }
}
