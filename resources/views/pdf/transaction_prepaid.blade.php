<!DOCTYPE html>
<html lang="ar">
<!-- <html lang="ar"> for arabic only -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>
        E-Payment Mobile Receipt - {{ $transaction_id }}
    </title>
    <style>
        @page {
            /* margin: 0 auto;
            sheet-size: 300mm 250mm; */
        }
        html {
            direction: rtl;
        }
        html,body {
            margin:5px;
            padding:0;
            font-size: 14px;
        }
        #printContainer {
            /* width: 250px; */
            margin: auto;
            /*padding: 10px;*/
            /*border: 2px dashed #000;*/
            text-align: justify;
        }

        .text-left {
            text-align: left;
        }
        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        .dashed {
            border: 1px dashed #000000;
            border-style: none none dashed;
            color: #fff;
            background-color: #fff;
        }
        .header {
            /* margin-top: -20px; */
        }
        .header > p {
            /* margin-top: -20px; */
        }
        .header > hr {
            margin-top: -10px;
        }
    </style>
</head>
<body>

    <!-- <hr class="dashed"> -->
    <div class="header text-center">
        <!-- <img src="{{ $logo }}" style="width: 100px;padding:4pt;"><br><br>
        <p>https://epaymentmobile.com</p> -->
        <p>STRUK PEMBELIAN</p>
        <hr class="dashed">
    </div>

    <div class="body">
        <table style="width:100%;">
            <tr>
                <td>
                    Waktu Transaksi
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;
                    {{ Carbon\Carbon::parse($created_at)->format('d/m/Y H:i:s') }}
                </td>
            </tr>
            <!-- <tr>
                <td>
                    Transaksi
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;
                    {{ $transaction_id }}
                </td>
            </tr> -->
            <tr>
                <td>
                    {{ $caption }}
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;
                    {{ $msisdn }}
                </td>
            </tr>
            <tr>
                <td>
                    Pembelian
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;
                    {{ $product['name'] }}
                </td>
            </tr>
            <tr>
                <td>
                    SN
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;
                    {{ $vsn }}
                </td>
            </tr>
        </table>
        <hr class="dashed">
        <!-- <table style="width:100%;">
            <tr>
                <td style="width:75%;">
                    {{ $product['name'] }}
                </td>
                <td class="text-right" style="width:25%;">
                    {{ number_format($real_price) }}
                </td>
            </tr>
        </table>
        <hr class="dashed"> -->
        <table style="width:100%;">
            <tr>
                <td class="text-right" style="width:75%;">
                    Harga :
                </td>
                <td class="text-right" style="width:25%;">
                    {{ number_format($real_price) }}
                </td>
            </tr>
            @if ($real_fee != 0)
                <tr>
                    <td class="text-right" style="width:75%;">
                        Admin :
                    </td>
                    <td class="text-right" style="width:25%;">
                        {{ number_format($real_fee) }}
                    </td>
                </tr>
            @endif
            @if ($real_disc != 0)
                <tr>
                    <td class="text-right" style="width:75%;">
                        Diskon :
                    </td>
                    <td class="text-right" style="width:25%;">
                        {{ number_format($real_disc) }}
                    </td>
                </tr>
            @endif
        </table>
        <hr class="dashed">
        <table style="width:100%;">
            <tr>
                <td class="text-right" style="width:75%;">
                    Total :
                </td>
                <td class="text-right" style="width:25%;">
                    {{ number_format($real_total) }}
                </td>
            </tr>
        </table>
        <!-- <hr class="dashed"> -->
    </div>

    <div class="footer text-center">
        <!-- <p>
            Bila ada keluhan hubungi <br>
            085950089005, 081222221709 <br>
            support@epaymentmobile.com
        </p> -->
        <p>
            --------- Terima Kasih ---------
        </p>
        <p>
            Struk ini merupakan alat bukti<br>
            pembayaran yang sah
        </p>
    </div>

</body>
</html>
