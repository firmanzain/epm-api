<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Reset Password E-Payment Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <?php 
        $secret = base64_encode($user->phone);
        $url = env('CLIENT_APP_URL') . 'reset/' . $secret;
    ?>
    <h1>Atur ulang password E-Payment Mobile Anda</h1>
    <p>Silahkan klik <a href="<?php echo $url ?>" target="_blank">disini</a> untuk mengatur password Anda</p>
    <p>Atau klik link berikut: <br/> <a href="<?php echo $url ?>" target="_blank"><?php echo $url ?></a></p>
</body>
</html>