<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Reset Password E-Payment Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <?php
        $url = env('CLIENT_APP_URL') . 'reset/' . $member->remember_token;
    ?>
    <h1>Atur ulang password E-Payment Mobile Anda</h1>
    <p>
        Silahkan klik <a href="<?php echo $url ?>" target="_blank">disini</a> untuk mengatur ulang password Anda. Atau copy link berikut pada browser anda:
        <br/> <a href="<?php echo $url ?>" target="_blank"><?php echo $url ?></a>
        <br/><br/> Link ini akan kadaluarsa dalam 2 jam. <b>Abaikan jika bukan anda yang melakukan aktivitas ini</b></p>
</body>
</html>
