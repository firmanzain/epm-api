<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Selamat Bergabung di E-Payment Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1>Selamat bergabung di dalam sistem E-Payment Mobile</h1>
    <p>Berikut detil akun Anda:</p>
    <table border="0">
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td><?php echo $user->name ?></td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
            <td><?php echo $user->email ?></td>
        </tr>
        <tr>
            <td>No. Handphone</td>
            <td>:</td>
            <td><?php echo $user->phone ?> <i>(digunakan untuk login)</i></td>
        </tr>
        <tr>
            <td>Referal ID</td>
            <td>:</td>
            <td><?php echo $user->referal_id ?></td>
        </tr>
    </table>
    <p>
        Pastikan akun Anda menjadi Premium untuk mendapatkan banyak keuntungan 
        dari kami serta jangan pernah topup atau registrasi secara manual atau 
        menitipkan uang kepada pihak manapun baik yang mengatasnamakan EPM atau 
        bukan, karena kami tidak bertanggung jawab atas hal tersebut di atas 
        selain perintah dari menu "TOPUP" atau menu "UPGRADE" pada aplikasi kami.
    </p>
    <p>
        Terima kasih,
    </p>
    <p><strong>Management EPM</strong></p>
</body>
</html>